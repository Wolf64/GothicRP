/********************************************
*	Klassen zum "Erweitern" bestehender     *
*   Daedalus Klassen zur Speicherung
*   zusätzlicher Eigenschaften
********************************************/

const int RP_ITEM_MAX_EXT = 16;
var int RP_ITEM_EXT[RP_ITEM_MAX_EXT];
var int RP_Item_Ext_NextIndex;
const int RP_TEST_INDEX = 0;

class C_Item_Ext
{
    var int condition_max;
    var int condition;
};

prototype C_Item_Ext_PROTO@(C_Item_Ext)
{
	condition_max 	= 100;
	condition		= 100;
};

instance C_Item_Ext@(C_Item_Ext_PROTO@);

func C_Item_Ext RP_Item_GetExt(var c_item itm) {
    var int hndl; hndl = MEM_ReadStatArr(RP_ITEM_EXT, RP_TEST_INDEX);
    if (!hndl) { // Da steht noch nichts drin
        hndl = new(C_Item_Ext@); // Dann schreiben wir da ein Objekt zum Speichern der neuen Sachen rein!
        MEM_WriteStatArr(RP_ITEM_EXT, RP_TEST_INDEX, hndl);
    };
    get(hndl);
    return;
};
