Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2024-01-07T15:12:34+01:00

====== Rausschmeißer ======
Angelegt Sonntag 07 Januar 2024

**Rausschmeißer**
Gilden:				[[Gilden:NL:Bandit|Bandit]]
Voraussetzungen:		[[Systeme:Skills:Überzeugen|Überzeugen]] >=25 oder Stärke >= 30
Tätigkeiten/Quests:		Kneipe bewachen

== [TODO] Ablauf ==
- Einstiegsmöglichkeiten:
	* Nachfragen bei den Rausschmeißern
- Arbeitszeiten:
	* Schichtarbeit nach Absprache
	* Frühschicht 8 - 16 Uhr
	* Spätschicht 16 - 0 Uhr
	* Nachtschicht 0 - 8 Uhr
- Ablösen des Rausschmeißers zur angegebenen Zeit
- Posten überwiegend halten
- Pausen in der Kneipe möglich
- Beim Einschreiten darf Posten verlassen werden
- Warten bis Ablösung kommt

== [TODO] Spielmechanik ==
Ziel: 
- Bewahrung der Ruhe in der Kneipe
- Ungebetene Gäste vom Eintritt hindern

Möglichkeiten der Aktivität an sich:
- Außenseiter (scripted events?) aufhalten und oder bestechen lassen
- Zufällige Kämpfe in der Kneipe schlichten
- Zechenpreller rauswerfen

Belohnungen:
- Erz und Verpflegung von Silas

