//****************************
// 	Konstanten f�r 
//  Kalendertage und Monate
//****************************

const int RP_CALENDAR_MON = 0;
const int RP_CALENDAR_TUE = 1;
const int RP_CALENDAR_WED = 2;
const int RP_CALENDAR_THU = 3;
const int RP_CALENDAR_FR = 4;
const int RP_CALENDAR_SAT = 5;
const int RP_CALENDAR_SUN = 6;

const int RP_CALENDAR_JAN = 0;
const int RP_CALENDAR_FEB = 1;
const int RP_CALENDAR_MAR = 2;
const int RP_CALENDAR_APR = 3;
const int RP_CALENDAR_MAY = 4;
const int RP_CALENDAR_JUN = 5;
const int RP_CALENDAR_JUL = 6;
const int RP_CALENDAR_AUG = 7;
const int RP_CALENDAR_SEP = 8;
const int RP_CALENDAR_OCT = 9;
const int RP_CALENDAR_NOV = 10;
const int RP_CALENDAR_DEC = 11;

const int RP_MAX_WEEKDAYS = 7;
const int RP_MAX_MONTHS = 12;
const int RP_MAX_DAYS_OF_MONTH = 28; // der Einfachheit gerundet, also 4*7 Tage, 4 Wochen/Mo

const String RP_WEEKDAYS[RP_MAX_WEEKDAYS] =
{
	"Montag",
 	"Dienstag",
	"Mittwoch",
	"Donnerstag",
	"Freitag",
	"Samstag",
	"Sonntag"
};

const String RP_MONTHS[RP_MAX_MONTHS] =
{
	"Januar",
 	"Februar",
	"M�rz",
	"April",
	"Mai",
	"Juni",
	"Juli",
	"August",
	"September",
	"Oktober",
	"November",
	"Dezember"
};