// Look for the function "DMG_OnDmg" to modify

func int DMG_OnDmg (var int victimPtr, var int attackerPtr, var int dmg) 
{ 
	var c_npc attackerNpc;
    if(Hlp_Is_oCNpc(attackerptr)){
        attackerNpc = _^(attackerptr);
    } else{
        attackerNpc = MEM_NullToInst();
    };

    var c_npc victimNpc;
    if(Hlp_Is_oCNpc(victimptr)){
        victimNpc = _^(attackerptr);
    } else{
        victimNpc = MEM_NullToInst();
    };

	var c_item attackerWpn;
	attackerWpn = Npc_GetReadiedWeapon(attackerNpc);
	var c_item victimWpn;
	victimWpn = Npc_GetReadiedWeapon(victimNpc);
	var c_item attackerArmor;
	attackerArmor = Npc_GetEquippedArmor(attackerNpc);
	var c_item victimArmor;
	victimArmor = Npc_GetEquippedArmor(victimNpc);
	
	
	/*
	var string prnt; prnt = attackerNpc.name;
	prnt = ConcatStrings("attacker: ", prnt);
	Print_Ext(150, 4000, prnt, "font_old_20_white.tga", RGBA(0, 255, 0, 255), 2*1000);
	prnt = attackerWpn.name;
	prnt = ConcatStrings("wpn: ", prnt);
	prnt = ConcatStrings(prnt, " (");
	prnt = ConcatStrings(prnt, IntToString(attackerWpn.hp));
	prnt = ConcatStrings(prnt, ")");
	Print_Ext(150, 4500, prnt, "font_old_20_white.tga", RGBA(0, 255, 0, 255), 2*1000);	
*/

	if (Npc_IsPlayer(victimNpc))
	{
		RP_ModCurrentWeaponDurability(victimNpc, dmg);
	}
	else if (Npc_IsPlayer(attackerNpc))
	{
		RP_ModCurrentWeaponDurability (attackerNpc, dmg);
		
		//Player_SkillManager();
		
		/*
		if (PlayerSkill_Disarm == TRUE)
		{
			AI_DropItem(victimNpc, victimWpn.id);	
		};	
		
		if (PlayerSkill_Stun == TRUE)
		//&& (C_BodyStateContains(self, BS_SNEAK))
		//&& (!Npc_CanSeeNpc(victimNpc, attackerNpc))
		&& (attackerWpn.damagetype == DAM_BLUNT)
		{
			dmg = 1;
			AI_StartState(victimNpc, ZS_Unconscious, 0, "");
			
			//DEBUG
			PrintScreen (victimNpc.name, -1, 50, FONT_Screen, 2);
		};		
		*/
	};
	
	//Skill Progression nur fuer den SC!
	if(RP_Skills_Initialized)
	{
		if(Hlp_GetInstanceID(attackerNpc) == Hlp_GetInstanceID(PC_Hero)) 
		{
			var c_item wpn; wpn = Npc_GetReadiedWeapon(attackerNpc);
			var int skillHndl; skillHndl = RP_Skills_GetWpnSkill(wpn);
			
			if(Hlp_IsValidHandle(skillHndl))
			{
				RP_AddSkillPoints(skillHndl, dmg);
			};
		};
	};
	
	//Apply fatigue (less if PC got hit, more if PC attacked)
	if(RP_Needs_Initialized)
	{
		if(Hlp_GetInstanceID(attackerNpc) == Hlp_GetInstanceID(PC_Hero)) 
		{
			//TODO: Move this to some place which also registers hits in the air, so fatigue
			//is applied, even if the player doesn't hit anybode (which makes sense)
			RP_Needs_Hunger -= RP_Needs_CombatAttack_Hunger;		
			RP_Needs_Thirst -= RP_Needs_CombatAttack_Thirst;		
			RP_Needs_Sleep -= RP_Needs_CombatAttack_Sleep;
		}
		else if (Hlp_GetInstanceID(victimNpc) == Hlp_GetInstanceID(PC_Hero))
		{
			RP_Needs_Hunger -= RP_Needs_CombatHurt_Hunger;		
			RP_Needs_Thirst -= RP_Needs_CombatHurt_Thirst;		
			RP_Needs_Sleep -= RP_Needs_CombatHurt_Sleep;
		};
	};
	
    return dmg;
};
    
func void _DMG_OnDmg () 
{
    EAX = DMG_OnDmg (ECX, MEM_ReadInt (MEM_ReadInt (ESP+548)+4), EAX);
};

func void InitDamage () {
    const int dmg = 0;
    if (dmg) { return; };
    HookEngineF(7567329, 6, _DMG_OnDmg);
    dmg = 1;
};