// ************************************************************************************************
// Heilwirkung und Kosten von Nahrung
// ************************************************************************************************

const int	Value_Bugmeat		=	 2;		const int	HP_Bugmeat			=	4;
const int	Value_Apfel			=	 4;		const int	HP_Apfel			=	8;
const int	Value_Weintrauben	=	 6;		const int	HP_Weintrauben		=	8;

const int	Value_Wasser		=	 3;		const int	HP_Wasser			=	5;
const int	Value_Bier			=	10;		const int	HP_Bier				=	3;
const int	Value_Wein			=	13;		const int	HP_Wein				=	3;
const int	Value_Reisschnaps	=	15;		const int	HP_Reisschnaps		=	3;

const int	Value_Reis			=	 5;		const int	HP_Reis				=	10;
const int	Value_Wurzelsuppe	=	 3;		const int	HP_Wurzelsuppe		=	 7;
const int	Value_Ragout		=	 4;		const int	HP_Ragout			=	 9;
const int	Value_CrawlerSuppe	=	10;		const int	HP_CrawlerSuppe		=	15;

const int	Value_FleischRoh	=	 5;		const int	HP_FleischRoh		=	10;
const int	Value_Brot			=	 8;		const int	HP_Brot				=	12;
const int	Value_K�se			=	10;		const int	HP_K�se				=	15;
const int	Value_Fleisch		=	 8;		const int	HP_Fleisch			=	15; // Rohes Fleisch mit Pfanne benutzt
const int	Value_Schinken		=	12;		const int	HP_Schinken			=	18;

const int	Value_Waldbeeren	=	5;		const int	HP_Waldbeeren		=	10;
const int	Value_Flammendorn	=	6;		const int	HP_Flammendorn		=	12;
const int	Value_Seraphis		=	7;		const int	HP_Seraphis			=	14;
const int	Value_Velayis		=	8;		const int	HP_Velayis			=	16;
const int	Value_Bergmoos		=	9;		const int	HP_Bergmoos			=	18;
const int	Value_Grabmoos		=	10;		const int	HP_Grabmoos			=	20;
const int	Value_Nachtschatten	=	11;		const int	HP_Nachtschatten	=	22;
const int	Value_Mondschatten	=	12;		const int	HP_Mondschatten		=	24;
const int	Value_Orkblatt		=	13;		const int	HP_Orkblatt			=	26;
const int	Value_Eichenblatt	=	14;		const int	HP_Eichenblatt		=	28;

const int	Value_H�llenpilz	=	 3;		const int	HP_H�llenpilz		=	 6;
const int	Value_Sklavenbrot	=	 9;		const int	HP_Sklavenbrot		=	15;

const int	Value_Heilkr�uter1	=	14;		const int	HP_Heilkr�uter1		=	30;
const int	Value_Heilkr�uter2	=	19;		const int	HP_Heilkr�uter2		=	39;
const int	Value_Heilkr�uter3	=	24;		const int	HP_Heilkr�uter3		=	49;

// Fixme: Trollkirsche als Handelsware mit Wert?
const int	Value_Trollkirsche	=	15;		const int	HP_Trollkirsche		=	-20;

// Mana
const int	Value_Blutbuche		=	3;		const int	Mana_Blutbuche		=	5;
const int	Value_Turmeiche		=	8;		const int	Mana_Turmeiche		=	10;
const int	Value_Rabenkraut	=	12;		const int	Mana_Rabenkraut		=	15;
const int	Value_Dunkelkraut	=	17;		const int	Mana_Dunkelkraut	=	20;
const int	Value_Steimwurzel	=	20;		const int	Mana_Steinwurzel	=	25;
const int	Value_Drachenwurzel	=	23;		const int	Mana_Drachenwurzel	=	30;


/*******************************************************************************************
**	Essbares				                                          					**
*******************************************************************************************/

INSTANCE ItAt_Meatbug_01 (C_Item)
{	
	name 				=	"Wanzenfleisch";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;
	
	value 				=	Value_Bugmeat;
	weight				=	RP_CarryWeight_Meatbug;
	nutrition			= 	RP_Nutrition_Meatbug;
	
	visual 				=	"ItAt_Meatbug_01.3DS";
	material 			=	MAT_LEATHER;
	scemeName			=	"FOOD";
	on_state[0]			=	UseBugmeat;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Bugmeat;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Meatbug;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Bugmeat;

};

	FUNC VOID UseBugmeat()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Bugmeat);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoApple(C_Item)
{	
	name 				=	"Apfel";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Apfel;
	weight				=	RP_CarryWeight_Apple;
	nutrition			= 	RP_Nutrition_Apple;

	visual 				=	"ItFo_Apple_01.3ds"; 
	material 			=	MAT_LEATHER;
	scemeName			=	"FOOD";
	on_state[0]			=	UseApple;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Apfel;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Apple;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Apple/2;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Apfel;
};

	FUNC VOID UseApple()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Apfel);
		RP_Needs_ConsumeDrink(self, item.nutrition / 2);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFo_wineberrys_01(C_Item)
{	
	name 				=	"Weintrauben";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_Weintrauben;
	weight				=	RP_CarryWeight_Wineberry;
	nutrition			= 	RP_Nutrition_Wineberry;

	visual 				=	"ItFo_wineberrys_01.3ds";
	on_state[0]         = 	Usewineberrys;  
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Weintrauben;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Wineberry;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Wineberry/2;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Weintrauben;
};

	func void Usewineberrys () 
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Weintrauben);
		RP_Needs_ConsumeDrink(self, item.nutrition / 2);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoLoaf(C_Item)
{	
	name 				=	"Brot";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Brot;
	weight				=	RP_CarryWeight_Bread;
	nutrition			= 	RP_Nutrition_Bread;

	visual 				=	"ItFo_Loaf_01.3ds";
	scemeName			=	"FOODHUGE";
	on_state[0]			=	UseLoaf;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Brot;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Bread;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Brot;

};

	FUNC VOID UseLoaf()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Brot);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoMutton (C_Item)
{	
	name 				=	"gebratenes Fleisch";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_Fleisch;
	weight				=	RP_CarryWeight_Meat;
	nutrition			= 	RP_Nutrition_Meat;

	visual 				=	"ItFo_CookedMutton_01.3ds";
	scemeName			=	"MEAT";
	on_state[0]			=	UseMutton;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Fleisch;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Meat;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Fleisch;
};

	FUNC VOID UseMutton()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Fleisch);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoMuttonRaw (C_Item)
{	
	name 				=	"rohes Fleisch";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_FleischRoh;
	weight				=	RP_CarryWeight_MeatRaw;
	nutrition			= 	RP_Nutrition_MeatRaw;

	visual 				=	"ItFo_RawMutton_01.3ds";
	scemeName			=	"MEAT";
	on_state[0]			=	UseMuttonRaw;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_FleischRoh;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_MeatRaw;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_FleischRoh;
};

	FUNC VOID UseMuttonRaw()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_FleischRoh);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFo_mutton_01 (C_Item)
{	
	name 				=	"Schinken";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_Schinken;
	weight				=	RP_CarryWeight_Ham;
	nutrition			= 	RP_Nutrition_Ham;

	visual 				=	"ItFo_mutton_01.3ds";
	scemeName			=	"FOODHUGE";
	on_state[0]			=	UseMutton1;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Schinken;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Ham;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Schinken;

};

	FUNC VOID UseMutton1()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Schinken);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoCheese(C_Item)
{	
	name 				=	"K�se";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_K�se;
	weight				=	RP_CarryWeight_Cheese;
	nutrition			= 	RP_Nutrition_Cheese;

	visual 				=	"ItFo_Cheese_01.3ds";
	scemeName			=	"FOODHUGE";
	on_state[0]			=	UseCheese;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_K�se;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Cheese;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_K�se;
};

	FUNC VOID UseCheese()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_K�se);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoRice(C_Item)
{	
	name 				=	"Reis";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;	

	value 				=	Value_Reis;
	weight				=	RP_CarryWeight_Rice;
	nutrition			= 	RP_Nutrition_Rice;

	visual 				=	"ItFo_Rice_01.3ds";
	scemeName			=	"RICE";
	on_state[0]			=	UseRice;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Reis;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Rice;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Reis;
};

	FUNC VOID UseRice()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Reis);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoSoup(C_Item)
{	
	name 				=	"Wurzelsuppe";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Wurzelsuppe;
	weight				=	RP_CarryWeight_RootSoup;
	nutrition			= 	RP_Nutrition_RootSoup;

	visual 				=	"ItFo_Soup_01.3ds";
	scemeName			=	"RICE";
	on_state[0]			=	UseSoup;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Wurzelsuppe;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_RootSoup;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_RootSoup;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Wurzelsuppe;
};

	FUNC VOID UseSoup()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Wurzelsuppe);
		RP_Needs_ConsumeDrink(self, item.nutrition);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoMeatbugragout(C_Item)
{	
	name 				=	"Fleischwanzenragout";
	
	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Ragout;
	weight				=	RP_CarryWeight_Ragout;
	nutrition			= 	RP_Nutrition_Ragout;

	visual 				=	"ItFo_Meatbugragout_01.3ds"; 
	material 			=	MAT_LEATHER;
	scemeName			=	"RICE";
	on_state[0]			=	UseMeatbugragout;

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Ragout;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Ragout;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Ragout/4;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Ragout;
};

	FUNC VOID UseMeatbugragout()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Ragout);
		RP_Needs_ConsumeDrink(self, item.nutrition / 4);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/
INSTANCE ItFoCrawlersoup(C_Item)                                          
{	                                                                
	name 				=	"Minecrawlersuppe";
	                                                            
	mainflag 			=	ITEM_KAT_FOOD;  
	flags 				=	ITEM_MULTI;
                                                                    
	value 				=	Value_CrawlerSuppe;  
	weight				=	RP_CarryWeight_Crawler;
	nutrition			= 	RP_Nutrition_Crawler;
                                                                    
	visual 				=	"ItFo_Crawlersoup_01.3ds";    
	material 			=	MAT_LEATHER;
	on_state[0]			=	UseCrawlersoup;   
	scemeName			=	"RICE";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_CrawlerSuppe;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Crawler;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Crawler/2;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_CrawlerSuppe;                                                    
};

	FUNC VOID UseCrawlersoup()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_CrawlerSuppe);
		RP_Needs_ConsumeDrink(self, item.nutrition / 2);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};


/*******************************************************************************************
**	Trinkbares				                                        					  **
*******************************************************************************************/

/******************************************************************************************/
INSTANCE ItFo_Potion_Water_01(C_Item)
{
	name 			=	"Wasser";

	mainflag 		=	ITEM_KAT_FOOD;
	flags 			=	ITEM_MULTI;

	value 			=	Value_Wasser;
	weight				=	RP_CarryWeight_Water;
	nutrition			= 	RP_Nutrition_Water;

	visual 			=	"ItFo_Potion_Water_01.3ds";
	material 		=	MAT_GLAS;
	on_state[0]		=	UseWaterPotion;
	scemeName		=	"POTION";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Wasser;
	TEXT[2]				= RP_Needs_TXT_ThirstBonus;			COUNT[2]	= RP_Nutrition_Water;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Wasser;
};

	func VOID UseWaterPotion()
	{	
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Wasser);
		RP_Needs_ConsumeDrink(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};


// FIXME: zs_Drunk f�r SC oder nicht? 
INSTANCE ItFoBeer(C_Item)
{
	name 				=	"Bier";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;
	
	value 				=	Value_Bier;	
	weight				=	RP_CarryWeight_Beer;
	nutrition			= 	RP_Nutrition_Beer;

	visual 				=	"ItFo_Beer_01.3ds";
	material 			=	MAT_GLAS;
	on_state[0]			=	UseBeer;
	scemeName			=	"POTION";
	
	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Bier;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Beer/3;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Beer;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Bier;
};

	FUNC VOID UseBeer()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Bier);
		RP_Needs_ConsumeDrink(self, item.nutrition);
		RP_Needs_ConsumeFood(self, item.nutrition/3);
		RP_CarryWeight_Drop(self, item, 1);	//damit auch das gewicht verringert wird!
	};

/******************************************************************************************/

INSTANCE ItFoWine(C_Item)
{	
	name 				=	"Wein";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Wein;
	weight				=	RP_CarryWeight_Wine;
	nutrition			= 	RP_Nutrition_Wine;

	visual	 			=	"ItFo_Wine_01.3ds";
	material 			=	MAT_LEATHER;
	on_state[0]			=	UseWine;
	scemeName			=	"POTION";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Wein;
	TEXT[2]				= RP_Needs_TXT_ThirstBonus;			COUNT[2]	= RP_Nutrition_Wine;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Wein;
};

	FUNC VOID UseWine()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Wein);
		RP_Needs_ConsumeDrink(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};

/******************************************************************************************/

INSTANCE ItFoBooze(C_Item)
{	
	name 				=	"Reisschnaps";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Reisschnaps;
	weight				=	RP_CarryWeight_Schnaps;
	nutrition			= 	RP_Nutrition_Schnaps;

	visual 				=	"ItFo_Booze_01.3ds";
	material 			=	MAT_GLAS;
	on_state[0]			=	UseBooze;
	scemeName			=	"POTION";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Reisschnaps;
	TEXT[2]				= RP_Needs_TXT_ThirstBonus;			COUNT[2]	= RP_Nutrition_Schnaps;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Reisschnaps;
};

	FUNC VOID UseBooze()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Reisschnaps);
		RP_Needs_ConsumeDrink(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
	};



//**************************************************************************************/
//						PFLANZEN  PFLANZEN PFLANZEN
/******************************************************************************************/ 
INSTANCE ItFo_Plants_Berrys_01(C_Item)
{	
	name 				=	"Waldbeeren";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Waldbeeren;
	weight				=	RP_CarryWeight_Berry01;
	nutrition			= 	RP_Nutrition_Berry01;

	visual 				=	"ItFo_Plants_Berrys_01.3ds";
	material 			=	MAT_WOOD;
	on_state [0]		=   UseBerrys;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Waldbeeren;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Berry01;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Berry01/3;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Waldbeeren;
};

		func void UseBerrys ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Waldbeeren);
			RP_Needs_ConsumeDrink(self, item.nutrition/3);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Beeren");
		};

/******************************************************************************************/
INSTANCE ItFo_Plants_Flameberry_01(C_Item)
{	
	name 				=	"Flammendorn";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Flammendorn;
	weight				=	RP_CarryWeight_Berry02;
	nutrition			= 	RP_Nutrition_Berry02;

	visual 				=	"ItFo_Plants_Flameberry_01.3ds";
	material 			=	MAT_WOOD;
	on_state [0]		=   Useflame;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Flammendorn;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Berry02;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Berry02/3;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Flammendorn;
};

		func void Useflame ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Flammendorn);
			RP_Needs_ConsumeDrink(self, item.nutrition/3);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Flammendorn");
		};
		
/******************************************************************************************/
INSTANCE ItFo_Plants_Seraphis_01(C_Item)
{	
	name 				=	"Seraphis";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Seraphis;
	weight				=	RP_CarryWeight_Seraphis;
	nutrition			= 	RP_Nutrition_Seraphis;

	visual 				=	"ItFo_Plants_Seraphis_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= Useseraphis;	
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Seraphis;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Seraphis;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Seraphis/3;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Seraphis;
};

		func void Useseraphis ()
		{			
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Seraphis);
			RP_Needs_ConsumeDrink(self, item.nutrition/3);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Seraphis");
		};
		
/******************************************************************************************/
// Steigerung von Seraphis

INSTANCE ItFo_Plants_Velayis_01(C_Item)
{	
	name 				=	"Velayis";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Velayis;
	weight				=	RP_CarryWeight_Valayis;
	nutrition			= 	RP_Nutrition_Valayis;

	visual 				=	"ItFo_Plants_Seraphis_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= UseVelayis;	
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Velayis;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Valayis;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_Valayis/3;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Velayis;
};

		func void UseVelayis ()
		{			
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Velayis);
			RP_Needs_ConsumeDrink(self, item.nutrition/3);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Velayis");
		};

/******************************************************************************************/
INSTANCE ItFo_Plants_mountainmoos_01(C_Item)
{	
	name 				=	"Bergmoos";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Bergmoos;
	weight				=	RP_CarryWeight_Moss01;
	nutrition			= 	RP_Nutrition_Moss01;

	visual 				=	"ItFo_Plants_mountainmoos_01.3ds";
	material 			=	MAT_WOOD;
	on_state [0]		=   Usemoos;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Bergmoos;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Moss01;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Bergmoos;
};

		func void Usemoos ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Bergmoos);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Moos");
		};
		
/******************************************************************************************/
INSTANCE ItFo_Plants_mountainmoos_02(C_Item)
{	
	name 				=	"Grabmoos";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Grabmoos;
	weight				=	RP_CarryWeight_Moss02;
	nutrition			= 	RP_Nutrition_Moss02;

	visual 				=	"ItFo_Plants_mountainmoos_01.3ds";
	material 			=	MAT_WOOD;
	on_state [0]		=   Usemoos2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Grabmoos;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Moss02;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Grabmoos;
};

		func void Usemoos2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Grabmoos);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse GrabMoos");
		};

/******************************************************************************************/
INSTANCE ItFo_Plants_Nightshadow_01(C_Item)
{	
	name 				=	"Nachtschatten";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Nachtschatten;
	weight				=	RP_CarryWeight_Nightshade;
	nutrition			= 	RP_Nutrition_Nightshade;

	visual 				=	"ItFo_Plants_Nightshadow_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]         = Usenight;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Nachtschatten;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Nightshade;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Nachtschatten;
};

		func void Usenight ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Nachtschatten);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Nachtschatten");
		};
		
/******************************************************************************************/
INSTANCE ItFo_Plants_Nightshadow_02(C_Item)
{	
	name 				=	"Mondschatten";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Mondschatten;
	weight				=	RP_CarryWeight_Moonshade;
	nutrition			= 	RP_Nutrition_Moonshade;

	visual 				=	"ItFo_Plants_Nightshadow_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]         = Usemoon;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Mondschatten;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Moonshade;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Mondschatten;
};

		func void Usemoon ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Mondschatten);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Mondschatten");
		};
		
/******************************************************************************************/
INSTANCE ItFo_Plants_OrcHerb_01(C_Item)
{	
	name 				=	"Orkblatt";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Orkblatt;
	weight				=	RP_CarryWeight_Orcleaf;
	nutrition			= 	RP_Nutrition_Orcleaf;

	visual 				=	"ItFo_Plants_OrcHerb_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= 	Useorc;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Orkblatt;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Orcleaf;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Orkblatt;
};

		func void Useorc ()
		{	
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Orkblatt);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse orkblatt");
		};
		
/******************************************************************************************/
INSTANCE ItFo_Plants_OrcHerb_02(C_Item)
{	
	name 				=	"Eichenblatt";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Eichenblatt;
	weight				=	RP_CarryWeight_Oakleaf;
	nutrition			= 	RP_Nutrition_Oakleaf;

	visual 				=	"ItFo_Plants_OrcHerb_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= Useorc2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Eichenblatt;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Oakleaf;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Eichenblatt;
};

		func void Useorc2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Eichenblatt);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Eichenblatt");
		};
		
/*****************************************************************************************/
//								PILZE
/*****************************************************************************************/
INSTANCE ItFo_Plants_mushroom_01(C_Item)
{	
	name 				=	"H�llenpilz";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_H�llenpilz;
	weight				=	RP_CarryWeight_Shroom01;
	nutrition			= 	RP_Nutrition_Shroom01;

	visual 				=	"ItFo_Plants_mushroom_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= Usemush;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_H�llenpilz;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Shroom01;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_H�llenpilz;
};

		func void Usemush ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_H�llenpilz);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse H�llenpilz");
		};

/*****************************************************************************************/
INSTANCE ItFo_Plants_mushroom_02(C_Item)
{	
	name 				=	"Sklavenbrot";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Sklavenbrot;
	weight				=	RP_CarryWeight_Shroom02;
	nutrition			= 	RP_Nutrition_Shroom02;
	
	visual 				=	"ItFo_Plants_mushroom_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= 	Usemush2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Sklavenbrot;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Shroom02;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Sklavenbrot;
};

		func void Usemush2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Sklavenbrot);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Sklavenbrot");
		};


/*****************************************************************************************/
//								HEILKR�UTER
/*****************************************************************************************/
INSTANCE ItFo_Plants_Herb_01(C_Item)
{	
	name 				=	"Heilkr�uter";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Heilkr�uter1;
	weight				=	RP_CarryWeight_Herb01;
	nutrition			= 	RP_Nutrition_Herb01;

	visual 				=	"ItFo_Plants_Herb_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=	UsePlants1;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Heilkr�uter1;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Herb01;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Heilkr�uter1;
};

	func void UsePlants1 ()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Heilkr�uter1);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
		//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Heilkr�uter");
	};

/******************************************************************************************/
INSTANCE ItFo_Plants_Herb_02(C_Item)
{	
	name 				=	"Heilkr�uter";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Heilkr�uter2;
	weight				=	RP_CarryWeight_Herb02;
	nutrition			= 	RP_Nutrition_Herb02;

	visual 				=	"ItFo_Plants_Herb_02.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=	UsePlants2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Heilkr�uter2;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Herb02;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Heilkr�uter2;
};

	func void UsePlants2 ()
	{
		Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Heilkr�uter2);
		RP_Needs_ConsumeFood(self, item.nutrition);
		RP_CarryWeight_Drop(self, item, 1);
		//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Heilpflanzen");
	};

/******************************************************************************************/
INSTANCE ItFo_Plants_Herb_03(C_Item)
{	
	name 				=	"Heilkr�uter";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Heilkr�uter3;
	weight				=	RP_CarryWeight_Herb03;
	nutrition			= 	RP_Nutrition_Herb03;

	visual 				=	"ItFo_Plants_Herb_03.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=	UsePlants3;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Heilkr�uter3;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Herb03;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Heilkr�uter3;
};

	func void UsePlants3 ()
	{			
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Heilkr�uter3);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Heilpflanzen");
	};

/*****************************************************************************************/
//							MANAREG. PFLANZEN
/*****************************************************************************************/
instance ItFo_Plants_Bloodwood_01 (C_ITEM)
{	
	name 					=	"Blutbuchen Samen";

	mainflag 				=	ITEM_KAT_FOOD;
	flags 					=	ITEM_MULTI | ITEM_RESPAWN;	

	value 					=	Value_Blutbuche;
	weight				=	RP_CarryWeight_Seed01;
	nutrition			= 	RP_Nutrition_Seed01;

	visual 					=	"ItFo_Plants_Bloodwood_01.3ds";
	material 				=	MAT_WOOD;
	on_state[0]				=  Useblood;
	scemeName				=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Blutbuche;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Seed01;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Blutbuche;                          	
};
                                       
		func void Useblood ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Blutbuche);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Blutbuchensamen");
		};   
		                                  
/******************************************************************************************/
instance ItFo_Plants_Towerwood_01 (C_ITEM)
{	
	name 					=	"Turmeichen Samen";

	mainflag 				=	ITEM_KAT_FOOD;
	flags 					=	ITEM_MULTI | ITEM_RESPAWN;	

	value 					=	Value_Turmeiche;
	weight					=	RP_CarryWeight_Seed02;
	nutrition				= 	RP_Nutrition_Seed02;

	visual 					=	"ItFo_Plants_Bloodwood_01.3ds";
	material 				=	MAT_WOOD;
	on_state[0]				=  Useblood2;
	scemeName				=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Turmeiche;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Seed02;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Turmeiche;                    	
};                                        

		func void Useblood2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Turmeiche);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Turmeichensamen");
		};
		                 
/******************************************************************************************/
INSTANCE ItFo_Plants_RavenHerb_01(C_Item)
{	
	name 				=	"Rabenkraut";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Rabenkraut;
	weight				=	RP_CarryWeight_Ravenroot;
	nutrition			= 	RP_Nutrition_Ravenroot;

	visual 				=	"ItFo_Plants_RavenHerb_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			= Useraven;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Rabenkraut;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Ravenroot;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Rabenkraut;
};

		func void Useraven ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Rabenkraut);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Rabenkraut");
		}; 
		  
/******************************************************************************************/
INSTANCE ItFo_Plants_RavenHerb_02(C_Item)
{	
	name 				=	"Dunkelkraut";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Dunkelkraut;
	weight				=	RP_CarryWeight_Darkroot;
	nutrition			= 	RP_Nutrition_Darkroot;

	visual 				=	"ItFo_Plants_RavenHerb_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=   Useraven2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Dunkelkraut;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Darkroot;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Dunkelkraut;
};

		func void Useraven2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Dunkelkraut);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Dunkelkraut");
		}; 
		 
/******************************************************************************************/ 
INSTANCE ItFo_Plants_Stoneroot_01(C_Item)
{	
	name 				=	"Steinwurzel";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Steimwurzel;
	weight				=	RP_CarryWeight_Root01;
	nutrition			= 	RP_Nutrition_Root01;

	visual 				=	"ItFo_Plants_Stoneroot_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=   Useroot;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Steinwurzel;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Root01;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Steimwurzel;
};

		func void Useroot ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Steinwurzel);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Steinwurzel");
		}; 
		
/******************************************************************************************/  
INSTANCE ItFo_Plants_Stoneroot_02(C_Item)
{	
	name 				=	"Drachenwurzel";

	mainflag 			=	ITEM_KAT_FOOD;
	flags 				=	ITEM_MULTI | ITEM_RESPAWN;	

	value 				=	Value_Drachenwurzel;
	weight				=	RP_CarryWeight_Root02;
	nutrition			= 	RP_Nutrition_Root02;

	visual 				=	"ItFo_Plants_Stoneroot_01.3ds";
	material 			=	MAT_WOOD;
	on_state[0]			=   Useroot2;
	scemeName			=	"FOOD";

	description			= name;
	TEXT[1]				= NAME_Bonus_Mana;				COUNT[1]	= Mana_Drachenwurzel;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_Root02;
	
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Drachenwurzel;
};

		func void Useroot2 ()
		{
			Npc_ChangeAttribute	(self,	ATR_MANA,	Mana_Drachenwurzel);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Drachenwurzel");
		}; 
		  
/******************************************************************************************/
//								GIFTPFLANZEN
/******************************************************************************************/
INSTANCE ItFo_Plants_Trollberrys_01(C_Item)
{	
	name 					=	"Trollkirsche";

	mainflag 				=	ITEM_KAT_FOOD;
	flags 					=	ITEM_MULTI | ITEM_RESPAWN;	

	value 					=	Value_Trollkirsche;
	weight					=	RP_CarryWeight_TrollBer;
	nutrition				= 	RP_Nutrition_TrollBer;

	visual 					=	"ItFo_Plants_Trollberrys_01.3ds";
	material 				=	MAT_WOOD;
	on_state [0]			=   UseTrollberrys;
	scemeName				=	"FOOD";

	description			= name;
//	TEXT[1]				= NAME_Bonus_HP;				COUNT[1]	= HP_Trollkirsche;
	TEXT[2]				= RP_Needs_TXT_HungerBonus;			COUNT[2]	= RP_Nutrition_TrollBer;
	TEXT[3]				= RP_Needs_TXT_ThirstBonus;			COUNT[3]	= RP_Nutrition_TrollBer/3;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Trollkirsche;
};

		func void UseTrollberrys ()
		{
			Npc_ChangeAttribute	(self,	ATR_HITPOINTS,	HP_Trollkirsche);
			RP_Needs_ConsumeFood(self, item.nutrition);
			RP_Needs_ConsumeDrink(self, item.nutrition/3);
			RP_CarryWeight_Drop(self, item, 1);
			//PrintDebugNpc 		(PD_ITEM_MOBSI, "Ich esse Trollkirschen");
		};