/***********************************
*	Konsolenbefehle zum Debugging
************************************/

func void _RP_CC_SetNeed(var int need, var string param)
{
    var int value;

    if (Hlp_StrCmp(param, " FULL")) // param Wert nimmt Leerzeichen vor dem eigentlich param mit
    {
        value = RP_Needs_HungerMax; // alle max Werte sollte eigentlich gleich sein

    }
    else if (Hlp_StrCmp(param, " HALF"))
    {
        value = RP_Needs_HungerMax / 2;
    }
    else if (Hlp_StrCmp(param, " QUARTER"))
    {
        value = RP_Needs_HungerMax / 4;
    }
    else
    {
        value = 0;
    };

	PrintDebug(ConcatStrings("[RP] Needs - set need ", IntToString(need)));
	PrintDebug(ConcatStrings("... to value ", IntToString(value)));

    if (need == RP_NEED_HUNGER) 
    {
		PrintDebug("[RP] Needs - set hunger via CC");
        RP_Needs_Hunger = value;
    }
    else if (need == RP_NEED_THIRST)
    {
		PrintDebug("[RP] Needs - set thirst via CC");
        RP_Needs_Thirst = value;
    }
    else if (need == RP_NEED_SLEEP)
    {
		PrintDebug("[RP] Needs - set sleep via CC");
        RP_Needs_Sleep = value;
    };
};

func string RP_CC_SetHunger(var string param)
{
   	_RP_CC_SetNeed(RP_NEED_HUNGER, param);
	return ConcatStrings("Set hunger to", param);
};

func string RP_CC_SetThirst(var string param)
{
   	_RP_CC_SetNeed(RP_NEED_THIRST, param);    
	return ConcatStrings("Set thirst to", param);
};

func string RP_CC_SetSleep(var string param)
{
	_RP_CC_SetNeed(RP_NEED_SLEEP, param);   
	return ConcatStrings("Set sleep to", param);
};

func string RP_CC_ShowNeeds(var string param)
{
	var string prnt;
	prnt = IntToString(RP_Needs_Hunger); 
	prnt = ConcatStrings("Hunger: ", prnt);
	/*RP_Needs_HungerTextHndl = */Print_Ext(150, 1000, prnt, "font_old_20_white.tga", RGBA(255, 0, 0, 255), 3*1000);
	prnt = IntToString(RP_Needs_Thirst); 
	prnt = ConcatStrings("Thirst: ", prnt);
	/*RP_Needs_ThirstTextHndl = */Print_Ext(150, 1500, prnt, "font_old_20_white.tga", RGBA(0, 255, 0, 255), 3*1000);
	prnt = IntToString(RP_Needs_Sleep); 
	prnt = ConcatStrings("Sleep: ", prnt);
	/*RP_Needs_SleepTextHndl = */	Print_Ext(150, 2000, prnt, "font_old_20_white.tga", RGBA(0, 0, 255, 255), 3*1000);
	
	return "showing needs for 3 seconds";
};

func string RP_CC_ShowEncumberment(var string param)
{
	var string prnt; var string hlp;
	prnt = IntToString(RP_CarryWeight); 
	hlp  = IntToString(RP_CarryWeightMax);
	prnt = ConcatStrings("enc: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 2200, prnt, "font_old_20_white.tga", RGBA(0, 255, 150, 255), 3*1000);
	
	return "showing encumberment for 3 seconds";
};

func string RP_CC_ShowSkills(var string param)
{
	var string prnt; var string hlp;
	var cRP_Skill skl; 
	
	skl = get(RP_Skill_1h);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("1H: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 500, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_2h);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("2H: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 1000, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Bow);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Bow: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 1500, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Crossbow);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("CBow: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 2000, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Magic);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Magic: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 2500, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Cooking);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Cook: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 3000, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Smithing);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Smith: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 3500, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Alchemy);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Alche: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 4000, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Pickpocket);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Pickpo: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 4500, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	skl = get(RP_Skill_Lockpicking);
	prnt = IntToString(skl.exp); 
	prnt = ConcatStrings("Picklo: ", prnt);
	prnt = ConcatStrings(prnt, "/");
	hlp = IntToString(skl.exp_next); 
	prnt = ConcatStrings(prnt, hlp);
	Print_Ext(150, 5000, prnt, "font_old_20_white.tga", RGBA(150, 255, 0, 255), 3*1000);
	
	return "showing skills for 3 seconds";
};

func string RP_CC_NextDay(var string param)
{
	Wld_AddTime(0, 24, 0); // einen Tag adden scheint nicht zu klappen, also das erste Argument ist nutzlos?
	
	return "Ein neuer Tag...";
};

func string RP_CC_AddTimeOverMidnight(var string param)
{
	var int hour; hour = Wld_GetHour();

	var int targetTime; targetTime = 24 - hour;

	Wld_AddTime(0, targetTime + 1, 0);
	
	return "Good night";
};

func string RP_CC_ShowSleepMenu(var string param)
{
	RP_ShowSleepMenu();
	
	return "showing sleep menu";
};

func void RP_TESTHOOKF()
{
	PrintScreen	("TEST", -1,-1,"font_old_20_white.tga",3);
};

func void RP_HK_Debug_01()
{
	if (MEM_KeyState(KEY_Y) == KEY_PRESSED)
	{
		var c_item wpn; wpn = Npc_GetEquippedMeleeWeapon(hero);
		var int wpnPtr; wpnPtr = MEM_InstToPtr(wpn);
		var int heroPtr; heroPtr = MEM_InstToPtr(hero);
		PrintDebug(ConcatStrings(ConcatStrings(ConcatStrings("[RP] Debug - trying to unequip wpn ", IntToString(wpnPtr)), " "), IntToString(heroPtr)));
		CALL_PtrParam (_@(wpnPtr));
		CALL__thiscall(hero, oCNpc__UnequipItem); // does nothing?
	};
};

func void RP_HK_Debug_02()
{
	if (MEM_KeyState(KEY_C) == KEY_PRESSED)
	{
		Wld_SetTime(8, 0);
	};
};

// Konsolenbefehle zum Testen
func void RP_InitDebug()
{
	// Init AFSP Debug Kram
	CC_ShowAI_Init ();
	CC_UnLock_Init ();
	CC_SetRoutine_Init();
	CC_SetSleepingMode_Init();
	CC_GotoNPC_Init ();
	CC_GotoZEN_Init ();
	CC_HoldTime_Init ();

	CC_Register(RP_CC_ShowNeeds, "show needs", "display need values on screen");
	CC_Register(RP_CC_ShowEncumberment, "show encumberment", "display current weight values on screen");
	CC_Register(RP_CC_ShowSleepMenu, "show sleepmenu", "displays new sleep menu");
	CC_Register(RP_CC_AddTimeOverMidnight, "minutesToMidnight", "adds time to reach midnight");
	CC_Register(RP_CC_NextDay, "nextDay", "increases day timer by one");
	CC_Register(RP_CC_ShowSkills, "show skills", "displays skill values on screen");
	CC_Register(RP_CC_SetHunger, "rp_setHunger", "set hunger to full|half|quarter|empty");
	CC_Register(RP_CC_SetThirst, "rp_setThirst", "set thirst to full|half|quarter|empty");
	CC_Register(RP_CC_SetSleep, "rp_setSleep", "set sleep to full|half|quarter|empty");

	//FF_ApplyOnceExtGT(RP_HK_Debug_01, 0, -1);
	//FF_ApplyOnceExtGT(RP_HK_Debug_02, 0, -1);
};