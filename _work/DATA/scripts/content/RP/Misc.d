/****************************************
* N�tzliche Funktionen ohne Zuordnung	*
*****************************************/
// Menu Item bearbeiten - von Sektenspinner
/*##############################################################
  Idea:
    This code is useful if you want to display information
    in the character menu that you cannot or do not want to
    link to an Npc-Talent or Attribute.
    
    Traditionally, the Engine fills in values itself, for example
    initialising the menu Item MENU_ITEM_LEVEL with hero.level,
    or retrieving the string in MENU_ITEM_PLAYERGUILD according to
    hero.guild and the table TXT_GUILDS (see Text.d).
    
    Because the way in which the engine retrieves these values
    is hardcoded, you have only limited controll on what is
    displayed in the character menu.
    
    The following code is aimed to give you more control,
    allowing you to implement additional talents (more
    than NPC_TALENT_MAX) or statistical information
    (like number of monsters killed) that are not represented
    in attributes or talents.
    You also have more controll on how the information is
    displayed, for example as a value (100), a fraction (30/100)
    with a unit (32 Coins), as a percentage (42%) or in
    any other way you fancy.
    
    Usage:
        1.) Call Install_Character_Menu_Hook() in INIT_GLOBAL
          after having called LeGo_Init.
        2.) For each piece of information you want to display,
          create a menu item in Scripts\System\Menu\Menu_Status.d
          You need to specify the position of the menu item there.
          Use e.g. MENU_ITEM_STATUS_HEADING as a reference.
          Lets say your new menu item is called MENU_ITEM_GOLD.
        3.) At the top of Menu_Status.d, update MENU_STATUS
          such that your new menu item is listed as well.
        4.) In the function Update_Character_Menu (see below)
          you need to "calculate" the value that should be
          displayed within the menu item (calculating might
          be as easy as specifying a variable).
          Use the function Update_Menu_Item.
          This may look like this:
            var int gold;
            gold = Npc_HasItems(hero, ItMi_Gold);
            var string s;
            s = ConcatStrings(IntToString(gold), " Coins");
            Update_Menu_Item("MENU_ITEM_GOLD", s);
        5.) If you need additional labels or headings,
          create additional menu items in Menu_Status.d for it.
          For example a label "Gold:".
          You can use MENU_ITEM_STATUS_HEADING as a reference.
          Don't forget to list all new items in MENU_STATUS.
        5.) Do not call Update_Character_Menu yourself.
          This is automatically done for you each time
          the character menu is opened.
        6.) Do not try to manage menu items that the engine
          manages itself. For example you will not be able
          to change the value in MENU_ITEM_LEARN, because
          the engine will always display the number of learn points there, negating your changes.
          If, for example, you want to display the player strength,
          create a new menu item or rename the old menu item
          instead of just using MENU_ITEM_ATTRIBUTE_1.
          
    The function Update_Character_Menu currently contains
    code for replacing the heading "CHARAKTERPROFIL" in the
    status menu with a string displaying number of gold coins
    in the hero's inventory.
    This is supposed to serve as an example.
    After following only step 1 above this should already work.
##############################################################*/

// [INTERAL]
    func void Update_Menu_Item(var string name, var string val) {
        var int itPtr;
        itPtr = MEM_GetMenuItemByString(name);

        if (!itPtr) {
            MEM_Error(ConcatStrings("[RP] Calendar - Update_Menu_Item: Invalid Menu Item: ", name));
            return;
        };
        
        //0x004D3C80 void __thiscall zCMenuItem::SetText(val = val, line = 0, drawNow = true)
        //const int SetText = 5061760;

        CALL_IntParam(true);
        CALL_IntParam(0);
        CALL_zStringPtrParam(val);
        CALL__thiscall(itPtr, zCMenuItem__SetText);
    };
    
    //Call this is INIT_GLOBAL after LeGo_Init.
	// -> in Calendar.d
    func void RP_Hook_StatusScreen() {
        //if(!IsHooked(oCNpc__OpenScreen_Status)) {
        //    HookEngineF(oCNpc__OpenScreen_Status, 6, RP_Update_StatusScreen);
        //};
    };
// [/INTERNAL]

// -----------------------------------------------------------------------------------------------------


									//item name		misc (z.B. gewicht)		wert von desc
func string RP_BuildItemNameString(var string name, var string desc, var int value)
{
	var string str; str = "";
	var string space; space = " (";
	var string brace; brace = ")";
	var string val; val = IntToString(value);
	
	
	str = ConcatStrings(name, space);
	str = ConcatStrings(str, desc);
	str = ConcatStrings(str, val);
	str = ConcatStrings(str, brace);
	
	return str;
};

func int RP_C_HasUnwantedBS() // TODO add npc param?
{
	if(C_BodyStateContains(PC_Hero ,BS_MOBINTERACT_INTERRUPT)
		|| C_BodyStateContains(PC_Hero, BS_MOBINTERACT)
		|| C_BodyStateContains(PC_Hero, BS_MOBINTERACT_INTERRUPT)
		|| C_BodyStateContains(PC_Hero, BS_UNCONSCIOUS)
		|| C_BodyStateContains(PC_Hero, BS_DEAD)
		|| C_BodyStateContains(PC_Hero, BS_INVENTORY))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	};
};

func int RP_C_PlayerCanSit()
{
	return !C_BodyStateContains(hero ,BS_MOBINTERACT_INTERRUPT)
		&& !C_BodyStateContains(hero, BS_MOBINTERACT)
		&& !C_BodyStateContains(hero, BS_MOBINTERACT_INTERRUPT)
		&& !C_BodyStateContains(hero, BS_UNCONSCIOUS)
		&& !C_BodyStateContains(hero, BS_DEAD)
		&& !C_BodyStateContains(hero, BS_INVENTORY)
		&& !C_BodyStateContains(hero, BS_SWIM)
		&& !C_BodyStateContains(hero, BS_CLIMB)
		&& !hero.aivar[AIV_INVINCIBLE]; // TODO besserer Check f�r Dialogmodus?
};

//es sind nach aktuellem stand immer nur 3 buffs max bekannt (needs)
func void _RP_ShowBuff() 
{
	if(Hlp_IsValidHandle(bufflist_views[0])) { View_Open(bufflist_views[0]); };
	if(Hlp_IsValidHandle(bufflist_views[1])) { View_Open(bufflist_views[1]); };
	if(Hlp_IsValidHandle(bufflist_views[2])) { View_Open(bufflist_views[2]); };
};

func void _RP_HideBuff() 
{
	if(Hlp_IsValidHandle(bufflist_views[0])) { View_Close(bufflist_views[0]); };
	if(Hlp_IsValidHandle(bufflist_views[1])) { View_Close(bufflist_views[1]); };
	if(Hlp_IsValidHandle(bufflist_views[2])) { View_Close(bufflist_views[2]); };
};

//Flip-Flop Funktion, wechselt nach jedem Aufruf
//wird beim Men�modus aufgerufen
func int RP_ShowHideBars()
{
	var int showBars;
	
	if(showBars)
	{
		if(RP_Needs_Initialized)
		{		
			Bar_Hide(RP_Needs_HungerBarHndl);
			Bar_Hide(RP_Needs_ThirstBarHndl);
			Bar_Hide(RP_Needs_SleepBarHndl);

			_RP_HideBuff();
		};

		showBars = false;
	}
	else
	{
		if(RP_Needs_Initialized)
		{	
			Bar_Show(RP_Needs_HungerBarHndl);
			Bar_Show(RP_Needs_ThirstBarHndl);
			Bar_Show(RP_Needs_SleepBarHndl);

			_RP_ShowBuff();
		};
		
		showBars = true;
	};
	
	return showBars;
};

func int RP_Is1HWpn(var c_item wpn)
{
	return (wpn.flags & ITEM_SWD) || (wpn.flags & ITEM_AXE);
};

func int RP_Is2HWpn(var c_item wpn)
{
	return (wpn.flags & ITEM_2HD_SWD) || (wpn.flags & ITEM_2HD_AXE);
};

func int RP_IsRangedWeapon(var c_item wpn)
{
	return (wpn.flags & ITEM_BOW) || (wpn.flags & ITEM_CROSSBOW);
};

// "Versch�nerungen" von Engine-bezogenen Sachen

const int sizeofPtr = 4; //Bytes, auch int

// Alias f�r ECX was in den allermeisten(?) F�llen der this-pointer von gehookten Funktionen ist
func int thisptr()
{
	return ECX;
};

// Zeiger zum ersten Argument der meisten(?) gehookten Function-Calls, solange der Argumenttyp 4 Byte gro� ist
func int getArg0()
{
	return MEM_ReadInt(ESP + sizeofPtr);
};

// Zeiger zum zweiten Argument der meisten(?) gehookten Function-Calls, solange der Argumenttyp 4 Byte gro� ist
func int getArg1()
{
	return MEM_ReadInt(ESP + sizeofPtr + sizeofPtr);
};

func int DIA_Condition_True()
{
	return TRUE;
};

func void DIA_Exit()
{
	AI_StopProcessInfos(self);
};

