/***********************************
*	Needs System	Vars/Constants	*
************************************/

// identifier
const int RP_NEED_HUNGER = 1;
const int RP_NEED_THIRST = 2;
const int RP_NEED_SLEEP = 3;

var int RP_Needs_HungerRate;	var int RP_Needs_HungerTickMS;
var int RP_Needs_ThirstRate;	var int RP_Needs_ThirstTickMS;
var int RP_Needs_SleepRate;		var int RP_Needs_SleepTickMS;
var int RP_BSListenerTick;

var int RP_Needs_Hunger;	const int RP_Needs_HungerMax = 5000;
var int RP_Needs_Thirst;	const int RP_Needs_ThirstMax = 5000;
var int RP_Needs_Sleep;		const int RP_Needs_SleepMax = 5000;

var int RP_Needs_FirstInit;

//Handles for Bars and Text
var int RP_Needs_HungerTextHndl;	var int RP_Needs_HungerBarHndl;
var int RP_Needs_ThirstTextHndl;	var int RP_Needs_ThirstBarHndl;
var int RP_Needs_SleepTextHndl;		var int RP_Needs_SleepBarHndl;

const int RP_Needs_Bar_X = 98; // guesstimated health bar dist from screen x
const int RP_Needs_Bar_Y_Offset_Sleep = 40; // right above health bar
const int RP_Needs_Bar_Y_Offset_Thirst = RP_Needs_Bar_Y_Offset_Sleep + 20; // roughly the size of a bar
const int RP_Needs_Bar_Y_Offset_Hunger = RP_Needs_Bar_Y_Offset_Thirst + 20;


//Variables for Buff/Debuff data
var int RP_Needs_Debuff_BaseHealth;
var int RP_Needs_Debuff_BaseMana;
var int RP_Needs_Debuff_BaseHealth2;
var int RP_Needs_Debuff_BaseMana2;

var int RP_Needs_Debuff_HungerHndl;

//BS Checker constants
const int RP_BS_IDLE 	= 0;			const int RP_Needs_HungerRate_Idle	= 4;	//stehen, sitzen, sonst.
const int RP_BS_WALK 	= 1;			const int RP_Needs_HungerRate_Walk 	= 4;	//gehen
const int RP_BS_CARDIO 	= 2;			const int RP_Needs_HungerRate_Cardio 	= 5;	//rennen, schwimmen, tauchen
const int RP_BS_WORK 	= 3;			const int RP_Needs_HungerRate_Work 	= 6;	//schmieden, hacken
const int RP_BS_PAUSE 	= 4;			const int RP_Needs_HungerRate_Pause 	= 4;	//rumsitzen, ???

const int RP_Needs_ThirstRate_Idle 		= 4;	const int RP_Needs_SleepRate_Idle 	= 2;	
const int RP_Needs_ThirstRate_Walk 		= 5;	const int RP_Needs_SleepRate_Walk 	= 3;	
const int RP_Needs_ThirstRate_Cardio 	= 8;	const int RP_Needs_SleepRate_Cardio = 5;	
const int RP_Needs_ThirstRate_Work 		= 8;	const int RP_Needs_SleepRate_Work 	= 6;	
const int RP_Needs_ThirstRate_Pause 		= 4;	const int RP_Needs_SleepRate_Pause 	= 1;	

//combat fatigue values per hit
const int RP_Needs_CombatAttack_Thirst	=	4;	const int RP_Needs_CombatHurt_Thirst	=	1;
const int RP_Needs_CombatAttack_Hunger	=	2;	const int RP_Needs_CombatHurt_Hunger	=	1;
const int RP_Needs_CombatAttack_Sleep	=	2;	const int RP_Needs_CombatHurt_Sleep		=	4;

// ******************************************
// 		RP_Nutrition-Konstanten f�r Needs	*
// ******************************************
// -- TODO: Diese Werte sollten �berarbeitet werden, hierzu ist mehr Testing/Lore-Info n�tig
//      - Beispiel: Als Sch�rfer soll man 3 Portionen Reis pro Tag kriegen, damit muss man �berleben k�nnen!
//      - Als Sch�rfer soll man auch 50 Erz pro Monat(!) kriegen, das sollte L�cken f�llen k�nnen
const int 	RP_Nutrition_Meatbug	=	300;	const int	RP_Nutrition_Apple		=	175;
const int 	RP_Nutrition_Wineberry	=	125;	const int	RP_Nutrition_Bread		=	600;
const int 	RP_Nutrition_Meat		=	500;	const int	RP_Nutrition_MeatRaw	=	300;
const int 	RP_Nutrition_Ham		=	600;	const int	RP_Nutrition_Cheese		=	500;
const int 	RP_Nutrition_Rice		=	500;	const int	RP_Nutrition_RootSoup	=	400;
const int 	RP_Nutrition_Ragout		=	650;	const int	RP_Nutrition_Crawler	=	500;
const int 	RP_Nutrition_Water		=	800;	const int	RP_Nutrition_Beer		=	500;
const int 	RP_Nutrition_Wine		=	450;	const int	RP_Nutrition_Schnaps	=	450;
const int 	RP_Nutrition_Berry01	=	180;	const int	RP_Nutrition_Berry02	=	300;
const int 	RP_Nutrition_Seraphis	=	225;	const int	RP_Nutrition_Valayis	=	240;
const int 	RP_Nutrition_Moss01		=	 75;	const int	RP_Nutrition_Moss02		=	 80;
const int 	RP_Nutrition_Nightshade	=	 70;	const int	RP_Nutrition_Moonshade	=	 80;
const int 	RP_Nutrition_Orcleaf	=	 85;	const int	RP_Nutrition_Oakleaf	=	 85;
const int 	RP_Nutrition_Shroom01	=	140;	const int	RP_Nutrition_Shroom02	=	180;
const int 	RP_Nutrition_Herb01		=	 50;	const int	RP_Nutrition_Herb02		=	 75;
const int 	RP_Nutrition_Herb03		=	700;	const int	RP_Nutrition_Seed01		=	 50;
const int 	RP_Nutrition_Seed02		=	 55;	const int	RP_Nutrition_Ravenroot	=	 65;
const int 	RP_Nutrition_Darkroot	=	 70;	const int	RP_Nutrition_Root01		=	 40;
const int 	RP_Nutrition_Root02		=	 45;	const int	RP_Nutrition_TrollBer	=	110;

/*
*	Some Strings
*/

const string RP_Needs_TXT_HungerBonus = "Hunger:";
const string RP_Needs_TXT_ThirstBonus = "Durst:";
const string RP_Needs_TXT_SleepBonus = "M�digkeit:";
