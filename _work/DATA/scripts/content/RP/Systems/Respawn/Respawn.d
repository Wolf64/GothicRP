/************************************
*			Respawn System			*
************************************/
var int RP_RespawnInitialized;

const int RP_RESPAWN_DELAY 		= 3;
CONST INT NPC_FLAG_RESPAWN		= 1 << 2;	//für c_npc flags feld;
CONST INT ITEM_RESPAWN			= 1 << 13;	//für item flags feld;

class RP_RespawnNPC 
{
    var int inst; //Die Monsterinstanz
    var string name; //Erstmal nur zum debuggen
    var string wp; //Der Wegpunkt für NPCs
    var int dayOfDeath; // Der Tag des Todes/
	var string worldName;	//damit items auch in der richtigen welt landen
};

class RP_RespawnItem 
{
    var int inst; //Die Monsterinstanz
	var int spawnPosition[16]; //weltkoordinaten für items
    var int dayOfRemoval; // Der Tag des Item-Pickups (wenn GetDay = dayOfRemoval +RP_RESPAWN_DELAY dann respawn)
	var string worldName;	//damit items auch in der richtigen welt landen
};

//um Variable erzeugen zu können
instance RP_RespawnNPC@(RP_RespawnNPC);
instance RP_RespawnItem@(RP_RespawnItem);

func void RP_AddRespawnNPC(var c_npc slf) 
{
    PrintDebug(ConcatStrings("[RP] Respawn - register for respawn: ", slf.name));

    var int hndl; hndl = new(RP_RespawnNPC@);     
    var RP_RespawnNPC rsp; rsp = get(hndl);
	
    rsp.inst = Hlp_GetInstanceID(slf);
    rsp.name = slf.name;
    rsp.wp = slf.spawnPoint;
    rsp.dayOfDeath = Wld_GetDay();
	rsp.WorldName = MEM_World.worldName;
};

func void RP_AddRespawnItem(var oCItem itm) 
{
    var int hndl; hndl = new(RP_RespawnItem@);
    var RP_RespawnItem rsp; rsp = get(hndl);
	
    rsp.inst = Hlp_GetInstanceID(itm);
    MEM_CopyWords(_@(itm._zCVob_trafoObjToWorld), _@(rsp.spawnPosition), 16);
    rsp.dayOfRemoval = Wld_GetDay(); 
    rsp.WorldName = MEM_World.worldName;
};

func int RP_CanNPCRespawn(var c_npc slf) 
{ 
    return (slf.flags & NPC_FLAG_RESPAWN); // Überprüfe ob flag gesetzt oder nicht
};

func int RP_CanItemRespawn(var oCItem itm) 
{ 
    return (itm.flags & ITEM_RESPAWN); // Überprüfe ob flag gesetzt oder nicht
};

/*	In die ZS DEAD EINTRAGEN! //
    if (RP_CanNPCRespawn(self)) 
	{
        RP_AddRespawn(self);
    };
*/

//sollte in der func aufgerufen werden, die bei jedem tageswechsel aufgerufen wird
//->B_RefreshArmor()
func void RP_CheckRespawns() 
{
	/*  
		Mit ForEachHndl() führe ich eine angegebene Funktion (2. Parameter) für alle Objekte/Handles 
		einer angegebenen Ursprungsinstanz (1. Parameter) aus. So kann ich ganz einfach
		alle RespawnObjects überprüfen :) 
	*/
	//Print_Ext(150, 1000, "Checking for possible respawns", "font_old_20_white.tga", RGBA(255, 0, 0, 255), 1*1000);
	ForEachHndl(RP_RespawnNPC@, _RP_CheckNPCRespawns);
	ForEachHndl(RP_RespawnItem@, _RP_CheckItemRespawns);
};

func void _RP_Respawn_InsertItemInstance (var int inst, var int fX, var int fY, var int fZ) {
    var zCWaynet wayNet; wayNet = MEM_PtrToInst(MEM_World.wayNet);
    var zCWaypoint wp; wp = MEM_PtrToInst(MEM_ReadInt(wayNet.wplist_next+4));
    var int x; x = wp.pos[0];
    var int y; y = wp.pos[1];
    var int z; z = wp.pos[2];
    wp.pos[0] = fX;
    wp.pos[1] = fY;
    wp.pos[2] = fZ;
    Wld_InsertItem(inst, wp.name);
    wp.pos[0] = x;
    wp.pos[1] = y;
    wp.pos[2] = z;
};

func int _RP_CheckNPCRespawns(var int hndl) // In diesem Parameter steht, für welches Handle die Funktion gerade ausgeführt wird
{ 
    var RP_RespawnNPC rsp; rsp = get(hndl);
    //Jetzt haben wir unser Objekt!
        
    if (Wld_GetDay() >= (rsp.dayOfDeath + RP_RESPAWN_DELAY) && (Hlp_StrCmp (rsp.WorldName, MEM_World.worldName))) // Der Tag des Respawns ist gekommen! \o/
	{ 
		PrintDebug(ConcatStrings("[RP] Respawn - inserting ", rsp.name));
        Wld_InsertNpc(rsp.inst, rsp.wp); // Daher fügen wir einfach den NPC an seinem WP ein :)
        // Allerdings müssen wir nun unser Objekt auch entfernen, sonst würde es ja beim nächsten Mal wieder eingefügt!
        delete(hndl);
	};
	
    return rContinue; // Wir wollen schließlich weiterhin alle Handles durchlaufen - bis wir alle haben
};

//DIESE FUNKTION CRASHT SOBALD EIN ITEM RESPAWNEN SOLL
func int _RP_CheckItemRespawns(var int hndl) // In diesem Parameter steht, für welches Handle die Funktion gerade ausgeführt wird
{ 
    var RP_RespawnItem rsp; rsp = get(hndl);
        
		if ((Wld_GetDay() >= (rsp.dayOfRemoval + RP_RESPAWN_DELAY)) && (Hlp_StrCmp (rsp.WorldName, MEM_World.worldName))) 
		{ 
			//Print_Ext(150, 1000, "Respawning item!", "font_old_20_white.tga", RGBA(0, 255, 0, 255), 1*1000);
            _RP_Respawn_InsertItemInstance(rsp.inst, rsp.spawnPosition[3], rsp.spawnPosition[7], rsp.spawnPosition[11]);
            var zCPar_Symbol a;    a = _^(MEM_ReadIntArray (currSymbolTableAddress, rsp.inst)); 
            var oCItem itm;    itm = _^(a.offset); 
            var int itmPtr;    itmPtr = MEM_InstToPtr (itm); 
            MEM_CopyWords(_@(rsp.spawnPosition),_@(itm._zCVob_trafoObjToWorld), 16);
            VobPositionUpdated(itmPtr);
			
            delete(hndl);
        };
		
    return rContinue;	// Wir wollen schlie�lich weiterhin alle Handles durchlaufen - bis wir alle haben
};