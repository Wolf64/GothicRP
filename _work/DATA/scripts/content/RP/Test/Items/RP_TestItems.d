INSTANCE ItMw_RP_Test_Sword_01 (C_Item)
{	
	name 				=	"Test Schwert";

	mainflag 			=	ITEM_KAT_NF;
											// Throw macht auch anderweitig Probleme f�r equippte Items, weglassen oder Flag dynamisch setzen?
	flags 				=	ITEM_SWD | ITEM_THROW; // TODO Werfen zieht Gewicht nicht aus Inventar ab
	material 			=	MAT_METAL;

	value 				=	44;//22;
	weight				=	RP_CarryWeight_ShortSword;

    hp_max = 100;
    hp = hp_max;	// TODO hp wird anscheinend nicht durch Gothic persistiert

	damageTotal			= 	12;
	damagetype 			=	DAM_EDGE;		
	range    			=  	100;		

	cond_atr[2]   		= 	ATR_STRENGTH;
	cond_value[2]  		= 	6;
	visual 				=	"ItMw_1H_Sword_Short_01.3DS";

	description			= RP_BuildItemNameString(name, RP_TXT_ITEMCONDITION, hp);
	TEXT[0]				= NAME_Damage;					COUNT[0]	= damageTotal;
	TEXT[1] 			= NAME_Str_needed;				COUNT[1]	= cond_value[2];
	TEXT[2] 			= NAME_OneHanded;

	TEXT[4]				=	RP_TXT_WeightPoints;				COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= value;
};

INSTANCE ItMw_RP_Test_Sword_02 (C_Item)
{	
	name 				=	"Test Schwert Notro";

	mainflag 			=	ITEM_KAT_NF;
	flags 				=	ITEM_SWD;
	material 			=	MAT_METAL;

	value 				=	44;//22;
	weight				=	RP_CarryWeight_ShortSword;

    hp_max = 100;
    hp = hp_max;

	damageTotal			= 	12;
	damagetype 			=	DAM_EDGE;		
	range    			=  	100;		

	cond_atr[2]   		= 	ATR_STRENGTH;
	cond_value[2]  		= 	6;
	visual 				=	"ItMw_1H_Sword_Short_01.3DS";

	description			= RP_BuildItemNameString(name, RP_TXT_ITEMCONDITION, hp);
	TEXT[0]				= NAME_Damage;					COUNT[0]	= damageTotal;
	TEXT[1] 			= NAME_Str_needed;				COUNT[1]	= cond_value[2];
	TEXT[2] 			= NAME_OneHanded;

	TEXT[4]				=	RP_TXT_WeightPoints;				COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= value;
};

INSTANCE RP_TEST_ARMOR(C_Item)
{
	name 					=	"Test R�stung";

	mainflag 				=	ITEM_KAT_ARMOR;
	flags 					=	0;

	protection [PROT_EDGE]	=	30;
	protection [PROT_BLUNT] = 	30;
	protection [PROT_POINT] = 	5;
	protection [PROT_FIRE] 	= 	15;
	protection [PROT_MAGIC] = 	0;

	value 					=	VALUE_STT_ARMOR_M;
	weight					=	RP_CarryWeight_Armor_STT_M;

    hp_max = 100;
    hp = hp_max;

	wear 					=	WEAR_TORSO;

	visual 					=	"sttm.3ds";
	visual_change 			=	"Hum_STTM_ARMOR.asc";
	visual_skin 			=	0;
	material 				=	MAT_LEATHER;

	description				=	RP_BuildItemNameString(name, RP_TXT_ITEMCONDITION, hp);
	//TEXT[0]				=	"";
	TEXT[0]					=	NAME_Prot_Edge;			COUNT[0]	= protection	[PROT_EDGE];
	TEXT[1]					=	NAME_Prot_Point;		COUNT[1]	= protection	[PROT_POINT];
	TEXT[2] 				=	NAME_Prot_Fire;			COUNT[2]	= protection	[PROT_FIRE];
	
	TEXT[4]					=	RP_TXT_WeightPoints;				COUNT[4]	= weight;
	TEXT[5]					=	NAME_Value;				COUNT[5]	= value;
};
