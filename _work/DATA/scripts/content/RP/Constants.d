/************************************
*	Konstanten die �berall in RP    *
*   vorkommen k�nnen 	            *
************************************/

/****************
*	Hooks
****************/
const int RP_isHooked = false;
const int RP_Needs_SleepMenuInitiated = false;
const int oCGame__SetShowPlayerStatus = 6523872;  //00638BE0 byte length = 6;
//const int oCGame__Pause	= 6545232;	//0063DF50	//gebraucht für neue Menüs um spiel zu stoppen???
//const int oCItemContainer__Remove = 6722336; // 006693D0 (int, int) - funzt nicht, try this: 00669320
const int oCItemContainer__Remove_Item = 6722512; // das sollte jetzt definitv klappen, danke an F a w k e s!
//const int oCViewDialogTrade__OnAccept = 7514224; // ???
const int oCNpc__DropAllInHand = 6898224;	//00694230
const int oCNpc__RemoveFromHand = 6897760; //00694060
const int oCNpc__EquipItem = 6879552; //0x0068F940

//TESTING
const int oCNpcInventory__Insert = 6735824;  //literally creates an inventory apparently each time an npc is in AI range
const int oCNpc__DoPutInInventory = 6952431; //006A15F0
const int oCNpcInventory__CheckForEquippedItems = 6747968; //0x0066F740
const int oCNpc__OpenScreen_Log = 6922320; //0x0069A050 -- zu fr�h, Men� exisiert noch nicht im Speicher
const int oCNpc__OpenScreen_Status = 6923264; //0x0069A400  -- unused
const int zCMenuItem__SetText = 5061760; //0x004D3C80
const int oCMenu_Log__SetDayTime = 4672896;// 0x00474D80 -- Log Screen hook f�r Datumsanzeige
const int oCMenu_Log__ScreenDone = 4672624;// 0x00474C70
const int oCMenu_Log__InitForDisplay = 4676688;// 0x00475C50
const int oCWorldTimer__SetDay = 7175072; // 0x006D7BA0 public: void __thiscall oCWorldTimer::SetDay
const int oCGame__SetTime = 6531344; // 0x0063A910
const int zCSession__SetTime = 5994432;// 0x005B77C0
const int oCWorldTimer__SetTime = 7175168;// 0x006D7C00

// Hilfsfunktion f�r Dialoge ohne Bedingung
func int RP_CND_TRUE()
{
    return true;
};

//-------------------- Standard Hero Dialogende -------------------------

INSTANCE PC_End (c_Info)
{
	npc				= PC_Hero;
	nr				= 999;
	condition		= RP_CND_TRUE;
	information		= PC_End_Info;
	important		= 0;
	permanent		= 1;
	description		= DIALOG_ENDE; 
};

func VOID PC_End_Info()
{
	AI_StopProcessInfos (self);
	self.aivar[AIV_INVINCIBLE]=FALSE;
};