/***********************************************
*	Liste der neuen Eigenschaften, die pro
*   NPC gespeichert werden können sollen
***********************************************/

//****************************** Reputation *********************************
var int RP_Prop_Reputation;
var int RP_Prop_Reputation_Initialized;
var int RP_Prop_Reputation_TxtHndl;
const String RP_TEXT_REP = "Ansehen: ";

func void Show_Reputation (var c_npc slf)
{	
	// Vorbelegung nur beim ersten Treffen des NPC
    if (!TAL_GetValue(slf, RP_Prop_Reputation_Initialized))
    {
        TAL_SetValue(slf, RP_Prop_Reputation_Initialized, 1);
        TAL_SetValue(slf, RP_Prop_Reputation, 25);
    };

	var int RepVal;
	RepVal = TAL_GetValue(slf, RP_Prop_Reputation);
	
	var String RepText;
	RepText = IntToString(RepVal);
	RepText = ConcatStrings(RP_TEXT_REP, RepText);

	if (RepVal < 25)
	{
		RP_Prop_Reputation_TxtHndl = Print_Ext(150, 7800, RepText, "FONT_OLD_20_WHITE.TGA", RGBA(255, 0, 0, 255), -1);
	}
	else
	{
		RP_Prop_Reputation_TxtHndl = Print_Ext(150, 7800, RepText, "FONT_OLD_20_WHITE.TGA", RGBA(0, 153, 0, 255), -1);
	};
};


func void _RP_UpdateReputation ()
{
	Print_DeleteText(RP_Prop_Reputation_TxtHndl);
	Show_Reputation(self); // self sollte hier jeweils ein gueltiger Sprecher innerhalb eines Dialoges sein!
};

func void RP_HideReputation()
{
	Print_DeleteText(RP_Prop_Reputation_TxtHndl);
};

func void RP_AddReputation(var C_NPC slf, var int value)
{
    var int RepVal;
	RepVal = TAL_GetValue(slf, RP_Prop_Reputation);
    RepVal += value;
    TAL_SetValue(slf, RP_Prop_Reputation, RepVal);

    if (Npc_IsInState(slf,ZS_Talk)) // nur in ZS_Talk da hier auf 'self' zugegriffen wird
	{
		_RP_UpdateReputation();
	};
};

func void RP_InitReputation()
{
	if (!RP_Prop_Reputation)
	{
		RP_Prop_Reputation = TAL_CreateTalent();
		RP_Prop_Reputation_Initialized = TAL_CreateTalent(); // Check um Wert vorbelegen zu koennen
	};
};