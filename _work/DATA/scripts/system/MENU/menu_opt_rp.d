// *********************************************************************
// RP mod options menu
// *********************************************************************

INSTANCE MENU_OPT_RP(C_MENU_DEF) 
{
	backpic			= MENU_BACK_PIC;
	
	items[0]		= "MENUITEM_RP_HEADLINE";
				
	items[1]		= "MENUITEM_RP_DIFFICULTY";
	items[2]		= "MENUITEM_RP_DIFFICULTY_CHOICE";
	
	items[3]		= "MENUITEM_RP_NEEDS_SYSTEM";
	items[4]		= "MENUITEM_RP_NEEDS_SYSTEM_CHOICE";

	items[5]		= "MENUITEM_RP_CARRYWEIGHT";
	items[6]		= "MENUITEM_RP_CARRYWEIGHT_CHOICE";

	items[7]		= "MENUITEM_RP_KEY_SIT";
				
	items[8]		= "MENUITEM_RP_BACK";

	items[23]		= "MENUITEM_RP_KEY_SIT_DEF";
	
	flags = flags | MENU_SHOW_INFO;
};


INSTANCE MENUITEM_RP_HEADLINE(C_MENU_ITEM_DEF) 
{
	text[0]		=	"MOD EINSTELLUNGEN";
	type		=	MENU_ITEM_TEXT;
	// Position und Dimension	
	posx		=	0;		posy		=	MENU_TITLE_Y;
	dimx		=	8100;
	
	flags		= flags & ~IT_SELECTABLE;
	flags		= flags | IT_TXT_CENTER;	
};


//
// Lookaround
//

INSTANCE MENUITEM_RP_DIFFICULTY(C_MENU_ITEM_DEF)
{
	backpic		=	MENU_ITEM_BACK_PIC;
	text[0]		=	"Schwierigkeit";
	text[1]		= 	"Der generelle Schwierigkeitsgrad. Wirkt sich auf die meisten Aspekte des Spiels aus."; // Kommentar
	// Position und Dimension	
	posx		=	1000;	posy		=	MENU_START_Y + MENU_DY*0;
	dimx		=	4000;	dimy		=	750;
	// Aktionen
	onSelAction[0]	= SEL_ACTION_UNDEF;
	// Weitere Eigenschaften
	flags			= flags | IT_EFFECTS_NEXT;
};

instance MENUITEM_RP_DIFFICULTY_CHOICE(C_MENU_ITEM_DEF)
{
	backPic		=	MENU_CHOICE_BACK_PIC;
	type		=	MENU_ITEM_CHOICEBOX;		
	text[0]		= 	"leicht|mittel|schwer";
	fontName	=   MENU_FONT_SMALL;
	// Position und Dimension	
	posx		= 5000;		posy		=	MENU_START_Y + MENU_DY*0 + MENU_CHOICE_YPLUS;
	dimx = MENU_SLIDER_DX;	dimy 		= 	MENU_CHOICE_DY;
	// Aktionen
	onChgSetOption													= "difficulty";
	onChgSetOptionSection 											= "RP";
	// Weitere Eigenschaften	
	flags		= flags & ~IT_SELECTABLE;	
	flags		= flags  | IT_TXT_CENTER;
};


//
// Fenster faden
//

INSTANCE MENUITEM_RP_NEEDS_SYSTEM(C_MENU_ITEM_DEF)
{
	backpic		=	MENU_ITEM_BACK_PIC;
	text[0]		=	"Bed�rfnisse";
	text[1]		= 	"Der Held hat Hunger, Durst und braucht Schlaf."; // Kommentar
	// Position und Dimension	
	posx		=	1000;	posy		=	MENU_START_Y + MENU_DY*1;
	dimx		=	4000;	dimy		=	750;
	// Aktionen
	onSelAction[0]	= SEL_ACTION_UNDEF;
	// Weitere Eigenschaften
	flags			= flags | IT_EFFECTS_NEXT;
};

instance MENUITEM_RP_NEEDS_SYSTEM_CHOICE(C_MENU_ITEM_DEF)
{
	backPic		=	MENU_CHOICE_BACK_PIC;
	type		=	MENU_ITEM_CHOICEBOX;	
	text[0]		= 	"aus|an";
	fontName	=   MENU_FONT_SMALL;
	// Position und Dimension	
	posx		= 5000;		posy		= MENU_START_Y + MENU_DY*1 + MENU_CHOICE_YPLUS;
	dimx 		= 2000;		dimy 		= MENU_CHOICE_DY;
	// Aktionen
	onChgSetOption													= "needsSystem";
	onChgSetOptionSection 											= "RP";
	// Weitere Eigenschaften	
	flags		= flags & ~IT_SELECTABLE;	
	flags		= flags  | IT_TXT_CENTER;
};



//
// Lookaround
//

INSTANCE MENUITEM_RP_CARRYWEIGHT(C_MENU_ITEM_DEF)
{
	backpic		=	MENU_ITEM_BACK_PIC;
	text[0]		=	"Inventarbegrenzung";
	text[1]		= 	"Gegest�nde haben gewicht und der Held hat ein Tragelimit."; // Kommentar
	// Position und Dimension	
	posx		=	1000;	posy		=	MENU_START_Y + MENU_DY*2;
	dimx		=	4000;	dimy		=	750;
	// Aktionen
	onSelAction[0]	= SEL_ACTION_UNDEF;
	// Weitere Eigenschaften
	flags			= flags | IT_EFFECTS_NEXT;
};

instance MENUITEM_RP_CARRYWEIGHT_CHOICE(C_MENU_ITEM_DEF)
{
	backPic		=	MENU_CHOICE_BACK_PIC;
	type		=	MENU_ITEM_CHOICEBOX;		
	text[0]		= 	"aus|an";
	fontName	=   MENU_FONT_SMALL;
	// Position und Dimension	
	posx		= 5000;		posy		=	MENU_START_Y + MENU_DY*2 + MENU_CHOICE_YPLUS;
	dimx = MENU_SLIDER_DX;	dimy 		= 	MENU_CHOICE_DY;
	// Aktionen
	onChgSetOption													= "carryWeight";
	onChgSetOptionSection 											= "RP";
	// Weitere Eigenschaften	
	flags		= flags & ~IT_SELECTABLE;
	flags		= flags  | IT_TXT_CENTER;
};

INSTANCE MENUITEM_RP_KEY_SIT(C_MENU_ITEM_DEF)
{
	text[0]			=	"Hinsetzen";
	text[1]			=   "Taste ENTF zum L�schen und RETURN zum definieren"; // Kommentar
	posx			=	ctrl_sp1_1;		posy	=	ctrl_y_start + ctrl_y_step * 12 + CTRL_GROUP2;
	
	onSelAction[0]	=	SEL_ACTION_EXECCOMMANDS;
	onSelAction_S[0]= 	"RUN MENUITEM_RP_KEY_SIT_DEF";  
	fontName 	= 	MENU_FONT_SMALL;
	flags = flags;
};

INSTANCE MENUITEM_RP_BACK(C_MENU_ITEM_DEF)
{
	backpic		=	MENU_ITEM_BACK_PIC;
	text[0]		=	"Zur�ck";
	// Position und Dimension	
	posx		=	1000;		posy		=	MENU_BACK_Y;
	dimx		=	6192;		dimy		=	MENU_DY;
	// Aktionen
	onSelAction[0]	= 	SEL_ACTION_BACK;	
	flags = flags | IT_TXT_CENTER;
};

// hotkeys

INSTANCE MENUITEM_RP_KEY_SIT_DEF(C_MENU_ITEM_DEF)
{
	type		= 	MENU_ITEM_INPUT;
	text[1] 	=   "Gew�nschte Taste bet�tigen.";
	
	posx		=	ctrl_sp1_2;	posy		=	ctrl_y_start + ctrl_y_step * 12 + CTRL_GROUP2;
	dimx		=	ctrl_dimx;
	dimy		=	300;
	fontName 	= 	MENU_FONT_SMALL;	
	backPic		=	MENU_KBDINPUT_BACK_PIC;
	
	onChgSetOption 			= "keySit";
	onChgSetOptionSection 	= "RP";
	flags		=	flags & ~IT_SELECTABLE;
};