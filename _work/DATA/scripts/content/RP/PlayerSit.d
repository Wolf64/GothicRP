var int RP_PlayerSit_State;
const int RP_PLAYERSIT_STATE_NONE = 0;
const int RP_PLAYERSIT_STATE_SITTING = 1;
const int RP_PLAYERSIT_STATE_SITTING_DOWN = 2;
const int RP_PLAYERSIT_STATE_GETTING_UP = 3;

func void RP_TogglePlayerSitting()
{
    if (RP_PlayerSit_State == RP_PLAYERSIT_STATE_SITTING_DOWN)
    {
        RP_PlayerSit_State = RP_PLAYERSIT_STATE_SITTING;
    }
    else if (RP_PlayerSit_State == RP_PLAYERSIT_STATE_GETTING_UP)
    {
        RP_PlayerSit_State = RP_PLAYERSIT_STATE_NONE;
    };
    PrintDebug(ConcatStrings("[RP] ZS_PlayerSit - toggle sitting to ", IntToString(RP_PlayerSit_State)));
};

func void RP_PlayerSit_Sit()
{
    AI_PlayAniBS (hero,"T_STAND_2_SIT",BS_SIT); // TODO BS geht nur �ber Ani-Dauer, nicht dar�ber hinweg!
    RP_PlayerSit_State = RP_PLAYERSIT_STATE_SITTING_DOWN;
    AI_Function (hero, RP_TogglePlayerSitting);
    AI_Function_S(hero, PrintDebug, "[RP] ZS_PlayerSit - is sitting");
};

func void RP_PlayerSit_GetUp()
{
    AI_PlayAni(hero,"T_SIT_2_STAND");	
    RP_PlayerSit_State = RP_PLAYERSIT_STATE_GETTING_UP;
    AI_Function (hero, RP_TogglePlayerSitting);
    AI_Function_S(hero, PrintDebug, "[RP] ZS_PlayerSit - end");
};

// Tastendr�cke abgefragt in Game_KeyEvent()
func int RP_Hotkey_Sit()
{
	if (RP_C_PlayerCanSit() && RP_PlayerSit_State == RP_PLAYERSIT_STATE_NONE)
	{
        RP_PlayerSit_Sit();
	}
    else
    {
	    RP_Hotkey_GetUp();
    };
};

func void RP_Hotkey_GetUp()
{
    if (RP_PlayerSit_State == RP_PLAYERSIT_STATE_SITTING)
    {
	    RP_PlayerSit_GetUp();
    };
};

