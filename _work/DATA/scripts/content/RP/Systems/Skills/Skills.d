/*******************************
*		Skills System
*******************************/
var int RP_Skills_Initialized;

func void RP_GiveAttributePoint(var int ATR)
{
	// --------- Umwandeln von var in const
	//var int Typ_Atr;
	//if 		(ATR == ATR_STRENGTH)	{	Typ_Atr = hero.attribute[ATR_STRENGTH];	}
	//else if (ATR == ATR_DEXTERITY)	{	Typ_Atr = hero.attribute[ATR_DEXTERITY];	}
	//else if (ATR == ATR_MANA_MAX)	{	Typ_Atr = hero.attribute[ATR_MANA_MAX];	};
	
	// --------- Steigern	
			
	if (ATR == ATR_STRENGTH)
	{
		hero.attribute[ATR_STRENGTH] = hero.attribute[ATR_STRENGTH] + RP_SKILL_LVL_ATRBONUS;
	}
	else if (ATR == ATR_DEXTERITY)
	{
		hero.attribute[ATR_DEXTERITY] = hero.attribute[ATR_DEXTERITY] + RP_SKILL_LVL_ATRBONUS;
	}
	else if (ATR == ATR_MANA_MAX)
	{
		hero.attribute[ATR_MANA_MAX] = hero.attribute[ATR_MANA_MAX] + (RP_SKILL_LVL_ATRBONUS + 1);
	};
};

func void RP_RaiseSkillLevel(var int skillHndl)
{
	Print_Ext(150, 4500, "Skill LvlUp!", "font_old_20_white.tga", RGBA(0, 255, 0, 255), 2*1000);
	
	//wenn es keinen g�ltigen Handle gibt passiert hier garnichts
	if(Hlp_IsValidHandle(skillHndl))
	{
		//den Skill holen
		var cRP_Skill skl; skl = get(skillHndl);
		
		//Level wird immer unabh�ngig vom Typ erh�ht
		//und nur wenn das Maximum nicht erreicht wurde
		if (skl.level < RP_SKILL_LVL_MAX)
		{
			skl.level += 1;
			
			//check skill type
			if(skl.type == RP_SKILL_TYPE_FIGHT)
			{
				skl.exp_next += skl.exp_next/2;
				skl.exp = 0;
				
				//vergebe Attributspunkt je nach skill level
				if (skl.level % RP_SKILL_LVL_MODULO == 0)
				{
					RP_GiveAttributePoint(skl.attribute);
				};
			}
			else if (skl.type == RP_SKILL_TYPE_MAGIC)
			{
				skl.exp_next += skl.exp_next/2;
				skl.exp = 0;
				
				//f�r jedes neue Level!
				RP_GiveAttributePoint(skl.attribute);
			}
			else if (skl.type == RP_SKILL_TYPE_CRAFTING)
			{
				skl.exp_next += 1;
				skl.exp = 0;
			
				if (skl.level % RP_SKILL_LVL_MODULO == 0)
				{
					RP_GiveAttributePoint(skl.attribute);
				};
			}
			else if (skl.type == RP_SKILL_TYPE_THIEF)
			{
			
			};
		};
		
		//vergebe letzen Attributspunkt beim Maximieren
		if (skl.level == RP_SKILL_LVL_MAX && !skl.mastered)
		{
			skl.mastered = true;
			RP_GiveAttributePoint(skl.attribute);
		};
	};
};

func void RP_AddSkillPoints(var int skillHndl, var int amount)
{
	//den Skill holen
	var cRP_Skill skl; skl = get(skillHndl);
	
	skl.exp += amount;
	
	if (skl.exp >= skl.exp_next)
	{
		RP_RaiseSkillLevel(skillHndl);
	};
};

//liefert Handle zum passenden Skill für die Waffe zurück
func int RP_Skills_GetWpnSkill(var c_item wpn)
{
	if(RP_Is1HWpn(wpn))
	{
		return RP_Skill_1h;
	}
	else if (RP_Is2HWpn(wpn))
	{
		return RP_Skill_2h;
	}
	else if (wpn.flags & ITEM_BOW)
	{
		return RP_Skill_Bow;
	}
	else if (wpn.flags & ITEM_CROSSBOW)
	{
		return RP_Skill_Crossbow;
	};
	
	return 0;
};

func void RP_InitSkills()
{
	// TODO Talents benutzen?
	if(!Hlp_IsValidHandle(RP_Skill_1h)) { RP_Skill_1h = new(RP_SKILL_MELEE@); };
	if(!Hlp_IsValidHandle(RP_Skill_2h)) { RP_Skill_2h = new(RP_SKILL_MELEE@); };
	if(!Hlp_IsValidHandle(RP_Skill_Bow)) { RP_Skill_Bow = new(RP_SKILL_RANGED@); };
	if(!Hlp_IsValidHandle(RP_Skill_Crossbow)) { RP_Skill_Crossbow = new(RP_SKILL_RANGED@); };
	
	if(!Hlp_IsValidHandle(RP_Skill_Magic)) { RP_Skill_Magic = new(RP_SKILL_MAGIC@); };
	
	if(!Hlp_IsValidHandle(RP_Skill_Cooking)) { RP_Skill_Cooking = new(RP_SKILL_CRAFTING@); };
	if(!Hlp_IsValidHandle(RP_Skill_Alchemy)) { RP_Skill_Alchemy = new(RP_SKILL_CRAFTING@); };
	if(!Hlp_IsValidHandle(RP_Skill_Smithing)) { RP_Skill_Smithing = new(RP_SKILL_CRAFTING@); };
	
	if(!Hlp_IsValidHandle(RP_Skill_Pickpocket)) { RP_Skill_Pickpocket = new(RP_SKILL_THIEF@); };
	if(!Hlp_IsValidHandle(RP_Skill_Lockpicking)) { RP_Skill_Lockpicking = new(RP_SKILL_THIEF@); };
	
	RP_Skills_Initialized = true;
};