
//****************************
// 	Konstanten f�r 
//  Mobsi Interaktion
//****************************

var int RP_Mob_Current;

const int RP_MOB_NONE = 0;
const int RP_MOB_BED = 1;
const int RP_MOB_PAN = 2;
const int RP_MOB_CAULDRON = 3;
const int RP_MOB_ORE = 4;
const int RP_MOB_LAB = 5;
const int RP_MOB_REPAIR = 6;
const int RP_MOB_SIT = 7;

const int RP_MOB_SMITHFIRE = 12;
const int RP_MOB_SMITHANVIL = 13;
const int RP_MOB_SMITHCOOL = 14;
const int RP_MOB_SMITHSHARP = 15;