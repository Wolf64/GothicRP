func void ZS_Dead ()
{	
	PrintDebugNpc	(PD_ZS_FRAME, "ZS_Dead" );		
	PrintGlobals	(PD_ZS_CHECK);

	C_ZSInit();
	
	//RP Respawn
    if (RP_CanNPCRespawn(self)) 
	{
		//Print_Ext(150, 1000, "Dead NPC can respawn", "font_old_20_white.tga", RGBA(0, 255, 0, 255), 1*1000);
        RP_AddRespawnNPC(self);
    }
	else
	{
		//Print_Ext(150, 1000, "Dead NPC CANNOT respawn", "font_old_20_white.tga", RGBA(255, 0, 0, 255), 1*1000);
	};

	self.aivar[AIV_PLUNDERED] = FALSE;
	
	//-------- Erfahrungspunkte f�r den Spieler ? --------
	//SN: VORSICHT, auch in B_MagicHurtNpc() vorhanden!
	if	Npc_IsPlayer   (other)
	||	(C_NpcIsHuman  (other) && other.aivar[AIV_PARTYMEMBER])
	||	(C_NpcIsMonster(other) && other.aivar[AIV_MM_PARTYMEMBER])
	{
		B_DeathXP();	// vergibt XP an SC
	};
	
	if	C_NpcIsMonster(self)
	{
		B_GiveDeathInv (); 	// f�r Monster
	};
	B_CheckDeadMissionNPCs ();
	B_Respawn (self); 	
};
