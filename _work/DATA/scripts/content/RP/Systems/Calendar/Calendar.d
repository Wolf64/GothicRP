/***********************************************************************
* 	Realisierung von Wochentagen und Monaten f�r erweitere Routinen
*  und/oder Quest-/Gameplaym�glichkeiten
***********************************************************************/

var string RP_CurrentWeekDay;
var string RP_CurrentMonth;
var int RP_CurrentDayOfMonth; // 0-based
var int RP_CurrentDay;
const int RP_GameStartDay = (RP_MAX_DAYS_OF_MONTH * 2) + 2; // 3. M�rz

func void RP_Update_LogScreen() {
    var string date; date = RP_CurrentWeekDay;
    date = ConcatStrings(date, ", ");
    date = ConcatStrings(date, IntToString(RP_CurrentDayOfMonth + 1));
    date = ConcatStrings(date, ". ");
    date = ConcatStrings(date, RP_CurrentMonth);
    Update_Menu_Item("MENU_ITEM_DATE", date);
};

func void RP_Calendar_Init()
{
    // Textanzeige im Tagebuch
    if(!IsHooked(oCMenu_Log__SetDayTime)) { HookEngineF(oCMenu_Log__SetDayTime, 7, RP_Update_LogScreen); };
    if(!IsHooked(oCWorldTimer__SetTime)) { HookEngine(oCWorldTimer__SetTime, 6, "RP_Calendar_Update"); };
    // if (!IsHookD(MEM_GetFuncID(Wld_AddTime))) { HookDaedalusFuncS("Wld_SubTime", "RP_Calendar_Update_Hook"); };

    // Wie handlen wir Jahreszeiten?
    RP_CurrentDay = Wld_GetDay();
    RP_CurrentDayOfMonth = (RP_CurrentDay + RP_GameStartDay) % RP_MAX_DAYS_OF_MONTH;
    RP_CurrentWeekDay = MEM_ReadStatStringArr(RP_WEEKDAYS, RP_CurrentDayOfMonth % RP_MAX_WEEKDAYS);
    RP_CurrentMonth = MEM_ReadStatStringArr(RP_MONTHS, ((RP_CurrentDay + RP_GameStartDay) / RP_MAX_DAYS_OF_MONTH) % RP_MAX_MONTHS);
};

// Sollte bei Tageswechsel aufgerufen werden um Konsistent zu bleiben.
// - B_RefreshArmor wird einmal zw. 23:59 und 0:00 von der Engine aufgerufen
// - Sleepmenu 
// - andere Zeit-Funktionen?? -> Wld_AddTime gehooked
func void RP_Calendar_Update()
{
    // Sicherheitscheck; haben wir wirklich einen Tageswechsel?
    if (Wld_GetDay() > RP_CurrentDay)
    {
        RP_CurrentDay = Wld_GetDay();
        RP_CurrentDayOfMonth = (RP_CurrentDay + RP_GameStartDay) % RP_MAX_DAYS_OF_MONTH;
        RP_CurrentWeekDay = MEM_ReadStatStringArr(RP_WEEKDAYS, RP_CurrentDayOfMonth % RP_MAX_WEEKDAYS);
        RP_CurrentMonth = MEM_ReadStatStringArr(RP_MONTHS, ((RP_CurrentDay + RP_GameStartDay) / RP_MAX_DAYS_OF_MONTH) % RP_MAX_MONTHS);
    };
};
