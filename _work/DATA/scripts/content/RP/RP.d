func void RP_InitAll()
{
	// Kern-Module
	RP_InitHooks();
	RP_InitSkills();
	RP_InitReputation();
	RP_SleepMenu_Init();
	RP_Calendar_Init();

	// Optionales (einstellbar in Optionen)
	if (Hlp_StrCmp(MEM_GetGothOpt("RP", "needsSystem"), "1"))
	{
		RP_InitNeeds();
	};
	if (Hlp_StrCmp(MEM_GetGothOpt("RP", "carryWeight"), "1"))
	{
		RP_InitCarryWeight();
	};
	
	// Hotkeys
	RP_InitHotkeys();
	FF_ApplyOnceExtGT(RP_Hotkey_Sit, 0, -1); // hinsetzen
};
