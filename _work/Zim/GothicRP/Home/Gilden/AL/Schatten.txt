Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2018-09-20T21:09:43+02:00

====== Schatten ======
Angelegt Donnerstag 20 September 2018

Voraussetzungen:
* Buddler
* Kann entweder Schleichen/Schlösser knacken/Taschendiebstahl
* Geschick 30

Jobs:
* [[Jobs:Dieb|Dieb]]
* Erpresser
* Händler
* [[Jobs:Jäger|Jäger]]

Equipment:
* Leichte Schattenkluft
* Schattenkluft
* Schwerter
* Kurz-/Langbögen

Skills:
* Schleichen
* Taschendiebstahl
* Schlösserknacken
* Bogenschießen
* Einhandwaffen
