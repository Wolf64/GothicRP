/****************************************
*   System zum persistieren von         *
*   allerei Item-Daten.     		    *
*                                       *
*   z.Z. nur f�r das Spieler-Inventar!  *
****************************************/

const int RP_MAX_ITEMDATA = 100;
var int RP_ItemData_Container[RP_MAX_ITEMDATA]; // Wir k�nnen Daten f�r bis zu 100 Items speichern 
var int RP_ItemData_Map;
var int RP_ItemData_Container_NextIndex;

var int RP_ItemData_Slot;

class RP_ItemData 
{
    var int inst; //Die Item-Instanz
    var int hp; // Haltbarkeit
};

//um Variable erzeugen zu k�nnen
instance RP_ItemData@(RP_ItemData);

func void RP_ItemData_Debug() {
	PrintDebug(ConcatStrings("[RP] ItemData - checking handle ", IntToString(RP_ItemData_Slot)));
	if (Hlp_IsValidHandle(RP_ItemData_Slot)) 
	{
		PrintDebug("[RP] ItemData - handle is valid!");
		var RP_ItemData obj; obj = get(RP_ItemData_Slot);
		PrintDebug(ConcatStrings("[RP] ItemData - item instance ID: ", IntToString(obj.inst)));
	}
	else 
	{
		MEM_Error("[RP] ItemData - handle not valid!");
	};
};

func void RP_ItemData_Add(var C_Item itm) 
{
	PrintDebug("[RP] ItemData - picked up new item, checking...");
	if (RP_ItemData_Slot != 0) 
	{
		PrintDebug(ConcatStrings("[RP] ItemData - data slot occupied: ", IntToString(RP_ItemData_Slot)));
		RP_ItemData_Debug();
		return;
	};

	PrintDebug(ConcatStrings("[RP] ItemData - creating new data object for instance ", IntToString(Hlp_GetInstanceID(itm))));
	var int hndl; hndl = new(RP_ItemData@);
    var RP_ItemData itemData; itemData = get(hndl);
	itemData.inst = Hlp_GetInstanceID(itm);
	itemData.hp = itm.hp;

	RP_ItemData_Slot = hndl;
	
    PrintDebug(ConcatStrings("[RP] ItemData - handle stored as ", IntToString(hndl)));
    RP_ItemData_Debug();
};

func void RP_ItemData_Remove(var int hndl) {
	if (RP_ItemData_Container_NextIndex == 0) { // Das Array ist leer.
		return;
	};

	delete(hndl); // Hier erledigen wir den PM-Teil unseres Destruktors, alles weitere macht PM dann selber. Unter anderem wird versucht, Respawn_Object_Delete() aufzurufen, allerdings gibt es diese Funktion nicht (sie ist optional)
	
	var int i; i = 0; // Das mag verwirren, aber ich baue blo� eine Schleife in Daedalus. Mit dem neuen Ikarus-Release geht das auch wesentlich sch�ner. 
	var int pos; pos = MEM_StackPos.position; // Stellt euch einfach vor, das hier w�re eine While(1)-Schleife. Zur �bersicht habe ich einger�ckt.

		var int h; h = MEM_ReadStatArr(RP_ItemData_Container, i); // h = RP_ItemData_Container[i];
		if (h == hndl) { // Wir haben unsere Referenz gefunden
			MEM_WriteStatArr(RP_ItemData_Container, i, MEM_ReadStatArr(RP_ItemData_Container, RP_ItemData_Container_NextIndex-1)); // Alte Referenz mit der letzten Referenz �berschreiben
			MEM_WriteStatArr(RP_ItemData_Container, RP_ItemData_Container_NextIndex-1, 0); // Letzte Referenz gleich 0 setzen
			RP_ItemData_Container_NextIndex -= 1; // Unseren Z�hler dekrementieren
			return; // Mehr wollen wir nicht machen.
		};

	i += 1;	
	if (i >= RP_ItemData_Container_NextIndex) { // Wenn i gr��er oder gleich dem nextRespawnIndex ist, haben wir das Array komplett durchlaufen.
	/* if (i >= nextRespawnIndex) {
			break;
		}; */
		return;
	};
	MEM_StackPos.position = pos;
	
};
