/*******************************
*	Skills System	const/Var
*******************************/

class cRP_Skill
{
	var int level;		//bestimmt ab wann der SC verschieden Stufen lernen kann
	var int exp;		//aktuelle Skill-EXP
	var int exp_next;	//n�chster Skill-Level ab wie vielen EXP?
	var int attribute;	//bestimmt das Attribut das erh�ht wird
	var int type;		//bestimmt die Art der Skillprogression
	var int mastered;	//bestimmt die Vergabe des letzten Attributspunktes
};

prototype RP_SKILL_PROTO@(cRP_Skill)
{
	level 	= 0;
	exp		= 0;
	mastered	= false;
};

instance RP_SKILL_MELEE@(RP_SKILL_PROTO@)
{
	exp_next 	= 100;
	type		= RP_SKILL_TYPE_FIGHT;
	attribute 	= ATR_STRENGTH;
};

instance RP_SKILL_RANGED@(RP_SKILL_PROTO@)
{
	exp_next 	= 100;
	type		= RP_SKILL_TYPE_FIGHT;
	attribute 	= ATR_DEXTERITY;
};

instance RP_SKILL_MAGIC@(RP_SKILL_PROTO@)
{
	exp_next 	= 100;
	type		= RP_SKILL_TYPE_MAGIC;
	attribute	= ATR_MANA_MAX;
};

instance RP_SKILL_THIEF@(RP_SKILL_PROTO@)
{
	exp_next	= 1;
	type		= RP_SKILL_TYPE_THIEF;
	attribute 	= ATR_DEXTERITY;
};

prototype RP_SKILL_CRAFTING@(cRP_Skill)
{
	level 	= 0;
	exp		= 0;
	exp_next	= 1;
	type		= RP_SKILL_TYPE_CRAFTING;
	mastered	= false;
};

instance RP_SKILL_COOKING@(RP_SKILL_CRAFTING@)
{
	attribute 	= ATR_DEXTERITY;
};

instance RP_SKILL_SMITHING@(RP_SKILL_CRAFTING@)
{
	attribute 	= ATR_STRENGTH;
};
instance RP_SKILL_ALCHEMY@(RP_SKILL_CRAFTING@)
{
	attribute 	= ATR_MANA_MAX;
};

//Handles f�r Skill-Instanzen, werden in RP_Init gesetzt
var int RP_Skill_1h;
var int RP_Skill_2h;
var int RP_Skill_Bow;
var int RP_Skill_Crossbow;
var int RP_Skill_Pickpocket;
var int RP_Skill_Lockpicking;
var int RP_Skill_Cooking;
var int RP_Skill_Alchemy;
var int RP_Skill_Smithing;
var int RP_Skill_Magic;
var int RP_Skill_Speech;

//RP_Skill.type =
const int RP_SKILL_TYPE_FIGHT 		= 0;
const int RP_SKILL_TYPE_MAGIC		= 1;
const int RP_SKILL_TYPE_CRAFTING	= 2;
const int RP_SKILL_TYPE_THIEF		= 3;

const int RP_SKILL_LVL_MAX			= 100;
const int RP_SKILL_LVL_ATRBONUS		= 1;	//soviele Attributspunkte werden aufgerechnet

const int RP_SKILL_LVL_MODULO		= 3;	//alle x skill level werden Attributspunkte aufgerechnet

/*
var int RP_Skills_1H_Lvl;			var int RP_Skills_1H_Points; 			var int RP_Skills_1H_Next;
var int RP_Skills_2H_Lvl;			var int RP_Skills_2H_Points; 			var int RP_Skills_2H_Next;
var int RP_Skills_Bow_Lvl;			var int RP_Skills_Bow_Points; 			var int RP_Skills_Bow_Next;
var int RP_Skills_Crossbow_Lvl;		var int RP_Skills_Crossbow_Points; 		var int RP_Skills_Crossbow_Next;
var int RP_Skills_Pickpocket_Lvl;	var int RP_Skills_Pickpocket_Points; 	var int RP_Skills_Pickpocket_Next;
var int RP_Skills_Lockpicking_Lvl;	var int RP_Skills_Lockpicking_Points; 	var int RP_Skills_Lockpicking_Next;
var int RP_Skills_Cooking_Lvl;		var int RP_Skills_Cooking_Points; 		var int RP_Skills_Cooking_Next;
var int RP_Skills_Alchemy_Lvl;		var int RP_Skills_Alchemy_Points; 		var int RP_Skills_Alchemy_Next;
var int RP_Skills_Smithing_Lvl;		var int RP_Skills_Smithing_Points; 		var int RP_Skills_Smithing_Next;
var int RP_Skills_Magic_Lvl;		var int RP_Skills_Magic_Points; 		var int RP_Skills_Magic_Next;
var int RP_Skills_Speech_Lvl;		var int RP_Skills_Speech_Points; 		var int RP_Skills_Speech_Next;
*/