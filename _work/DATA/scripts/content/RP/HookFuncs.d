/********************
*	Hook Functions	*
********************/

func void RP_InitHooks()
{
	//Print_Ext(150, 6000, "Initiating hooks...", "font_old_20_white.tga", RGBA(255, 0, 0, 255), 10*1000);
		
	//F�r Carry Weight System und Item Respawn
	if (!IsHooked(oCNpc__DoTakeVob)) { HookEngine(oCNpc__DoTakeVob, 6, "RP_HF_DoTakeVob"); };
		
	//F�r Carry Weight System 
	if (!IsHooked(oCNPC__DoDropVob)) { HookEngine (oCNPC__DoDropVob, 6, "RP_HF_DoDropVob"); };
	if (!IsHooked(oCItemContainer__TransferItem)) { HookEngine (oCItemContainer__TransferItem, 5, "RP_HF_TransferItem");  };
	if (!IsHooked(oCItemContainer__Remove_Item)) { HookEngine (oCItemContainer__Remove_Item, 6, "RP_HF_Remove_Item"); };
	if (!IsHooked(oCViewDialogTrade__OnExit)) { HookEngine (oCViewDialogTrade__OnExit, 5, "RP_CarryWeight_RecalcInventory"); };
	if (!IsHooked(oCNpc__DropAllInHand)) { HookEngine (oCNpc__DropAllInHand, 5, "RP_CarryWeight_RecalcInventory"); };
		
	//Um Bars ein/auszublenden
	if (!IsHooked(oCGame__SetShowPlayerStatus)) { HookEngine(oCGame__SetShowPlayerStatus, 6, "RP_ShowHideBars"); };
	if (!IsHooked(oCNpc__OpenInventory)) { HookEngine(oCNpc__OpenInventory, 6, "RP_ShowEncumberment"); };
	if (!IsHooked(oCNpc__CloseInventory)) { HookEngine(oCNpc__CloseInventory, 6, "RP_HideEncumberment"); };

	//TESTING
	//HookEngine(TESTADDR, 5, "RP_TESTHOOKF");
	//if (!IsHooked(oCGame__SetTime)) { HookEngine(oCGame__SetTime, 6, "RP_Calendar_Update"); };
};

func void RP_HF_DoTakeVob()
{
	//	----- an den NPC und das item kommen -----
	var c_npc slf; slf = MEM_PtrToInst(thisptr());				// der NPC, der das Item einsammelt
	var C_ITEM itm; itm = MEM_PtrToInst(getArg0());		// Pointer auf das Item
	var oCItem itmobj; itmobj = _^(getArg0());	//Engine-Klassenobjet f�r amount
	
	//f�r Carry Weight System
	if (!Hlp_IsValidItem (itm)) { return; };
	if (!Hlp_IsValidNpc (slf)) { return; };
	
	RP_CarryWeight_PickUp(slf, itm, itmobj.amount);
	
	//f�r Respawn System
	if(RP_CanItemRespawn(itmobj))
	{
		//Print_Ext(150, 1000, "Item can respawn", "font_old_20_white.tga", RGBA(0, 255, 0, 255), 1*1000);
		RP_AddRespawnItem(itmobj);
	}
	else
	{
		//Print_Ext(150, 1000, "Item CANNOT respawn", "font_old_20_white.tga", RGBA(255, 0, 0, 255), 1*1000);
	};

	// Item f�r Persistierung registrieren
	RP_ItemData_Add(itm);

	Print(ConcatStrings("InstanceID: ", IntToString(Hlp_GetInstanceId(itm))));
};


var int _RP_HF_CurrentBrokenWeaponPtr;
func void _RP_HF_UnequipWeapon()
{
	var int heroPtr; heroPtr = MEM_InstToPtr(hero);
	PrintDebug(ConcatStrings("[RP] HF - Test equip calling engine func unequip on itemptr ", IntToString(_RP_HF_CurrentBrokenWeaponPtr)));
	PrintDebug(ConcatStrings("[RP] HF - Test equip calling engine func unequip on heroptr ", IntToString(heroPtr)));
	CALL_PtrParam (_@(_RP_HF_CurrentBrokenWeaponPtr));
	CALL__thiscall(heroPtr, oCNpc__UnequipItem); // does nothing?
};

func void RP_HF_TestEquip()
{
	//	----- an den NPC und das item kommen -----
	var c_npc slf; slf = MEM_PtrToInst(thisptr());				// der NPC, der das Item einsammelt
	_RP_HF_CurrentBrokenWeaponPtr = MEM_ReadInt(getArg0()); // Pointer auf das Item
	var C_ITEM itm; itm = MEM_PtrToInst(_RP_HF_CurrentBrokenWeaponPtr);		
	
	PrintDebug(ConcatStrings("[RP] HF - Test equip itemptr ", IntToString(_RP_HF_CurrentBrokenWeaponPtr)));
	PrintDebug(ConcatStrings("[RP] HF - Test equip heroptr ", IntToString(thisptr())));

	if (itm.hp == 0)
	{
		if (itm.mainflag == ITEM_KAT_NF)
		{
		PrintDebug("[RP] HF - Test equipped item is a weapon");
		PrintDebug("[RP] HF - Test equip - Item is broken!");
		//G_CanNotUse(1, ATR_STRENGTH, 100);
		//AI_UnequipWeapons(slf); -- unequipped auch Fernwaffen, nervig!
		FF_ApplyExt(_RP_HF_UnequipWeapon, 0, 2);
		}
		else if (itm.mainflag == ITEM_KAT_ARMOR)
		{
			PrintDebug("[RP] HF - Test equipped item is an armor");
			PrintDebug("[RP] HF - Test equip - Item is broken!");
			AI_UnequipArmor(slf); // easy f�r R�stungen!
		};
	};
};

//NPC -> Hero || Hero -> Container
func void RP_HF_TransferItem()
{
	//mit Hilfe von F a w k e s
	var oCStealContainer Item_Container;
    var C_NPC npc;
    var oCItem itm;
    var int amount;
    var C_ITEM itmInstance;
    var oCNPC her;
    
    //ECX          = 8245076 -> oCNpcContainer
    //ESP + 8    = 1 -> quantity
    //007DCF54  .rdata    Debug data           ??_7oCNpcContainer@@6B@
    //oCNpcContainer = 8245076 !!

    Item_Container = _^(thisptr());
    
    //Get inventory owner
    npc = _^(Item_Container.inventory2_owner);
    
    //Get item
    itm = _^(List_GetS(Item_Container.inventory2_oCItemContainer_contents, Item_Container.inventory2_oCItemContainer_selectedItem + 2));
	itmInstance = MEM_PtrToInst(List_GetS(Item_Container.inventory2_oCItemContainer_contents, Item_Container.inventory2_oCItemContainer_selectedItem + 2));
    
    //Get item quantity
    amount = getArg1();
	
	if(itm.amount < amount)
	{
		amount = itm.amount;
	};

    //Do what you have to do here ;)    
    //var string str; str = "Taken from: ";
	//var string hlp; hlp = IntToString(amount);
	//str = ConcatStrings(str, npc.name);
	//str = ConcatStrings(str, " x");
	//str = ConcatStrings(str, hlp);
	
	//Print_Ext(350, 300, str, "font_old_20_white.tga", RGBA(255, 0, 0, 255), 3*1000);
	//PrintScreen	("Took item from container.", -1,-1,"font_old_20_white.tga",3);
	
	//	Hier muss man zwei fälle beachten:
	//	1. Der Hero lootet eine Leiche (jeder NPC)
	//	2. Der Hero legt etwas in eine Truhe
	//		"npc" ist hier jeweils aus welchem Inventar
	//		Items entnommen werden, im 2. Falle also hero
	//
	//	ACHTUNG! Da "oCItemContainer__Remove_Item" schon 
	//	jeglichen Transfer von NPC -> Hero übernimmt, darf 
	//	in diese Funktion nicht auch nochmal RP_CarryWeight_PickUp
	//	aufgerufen werden! Sonst doppelte Gewichtsaufnahme!
	
	RP_CarryWeight_Drop(npc, itmInstance, amount);
};

//Container -> Hero || NPC -> Hero
func void RP_HF_Remove_Item()
{
	//mit Hilfe von F a w k e s
    var oCItem itm;
    var C_ITEM itmInstance;
    
    //Get item
    itm = _^(getArg0());
	itmInstance = MEM_PtrToInst(getArg0());
    
    //var string str; str = "Taken: ";
	//var string hlp; hlp = IntToString(amount);
	//str = ConcatStrings(str, itm.name);
	//str = ConcatStrings(str, " x");
	//str = ConcatStrings(str, hlp);
	
	//Print_Ext(350, 300, str, "font_old_20_white.tga", RGBA(255, 0, 0, 255), 3*1000);
	//PrintScreen	("Took item from container.", -1,-1,"font_old_20_white.tga",3);
	
	//amount is just one since oCItemContainer__Remove_Item
	//is called for every item in stack (so 5 times for 5 beer in a stack)
	RP_CarryWeight_PickUp(hero, itmInstance, 1);
};

//FIXME: wird nicht aufgerufen wenn der SC in den ZS_Unconscious fällt!
//evlt dort nochmal nach gezogener waffe checken und 
//dementsprechend RP_CarryWeight_Drop darauf aufrufen!
func void RP_HF_DoDropVob()
{
		//	----- an den NPC und das item kommen -----
	var c_npc slf; slf = MEM_PtrToInst(thisptr());				// der NPC, der das Item einsammelt
	var C_ITEM itm; itm = MEM_PtrToInst(getArg0());		// Pointer auf das Item
	var oCItem itmobj; itmobj = _^(getArg0());	//Engine-Klassenobjet für amount

	//	----- stimmt das item? -----
	if (!Hlp_IsValidItem (itm)) { return; };
	if (!Hlp_IsValidNpc (slf)) { return; };
	
	RP_CarryWeight_Drop(slf, itm, itmobj.amount);
};