/****************************************
* Carry Weight System Vars/Constants	*
*****************************************/

//Bar placement
const int RP_CarryWeight_Bar_X = 98; // guesstimated mana bar dist from screen x
const int RP_CarryWeight_Bar_Y_Offset = 40; // right above mana bar

//Food items
const int 	RP_CarryWeight_Meatbug		=	 2;	const int	RP_CarryWeight_Apple		=	 2;
const int 	RP_CarryWeight_Wineberry	=	 1;	const int	RP_CarryWeight_Bread		=	 7;
const int 	RP_CarryWeight_Meat			=	 5;	const int	RP_CarryWeight_MeatRaw		=	 4;
const int 	RP_CarryWeight_Ham			=	 7;	const int	RP_CarryWeight_Cheese		=	 3;
const int 	RP_CarryWeight_Rice			=	 4;	const int	RP_CarryWeight_RootSoup		=	 5;
const int 	RP_CarryWeight_Ragout		=	 6;	const int	RP_CarryWeight_Crawler		=	 5;
const int 	RP_CarryWeight_Water		=	 6;	const int	RP_CarryWeight_Beer			=	 7;
const int 	RP_CarryWeight_Wine			=	 8;	const int	RP_CarryWeight_Schnaps		=	 7;
const int 	RP_CarryWeight_Berry01		=	 1;	const int	RP_CarryWeight_Berry02		=	 1;
const int 	RP_CarryWeight_Seraphis		=	 2;	const int	RP_CarryWeight_Valayis		=	 2;
const int 	RP_CarryWeight_Moss01		=	 1;	const int	RP_CarryWeight_Moss02		=	 1;
const int 	RP_CarryWeight_Nightshade	=	 1;	const int	RP_CarryWeight_Moonshade	=	 1;
const int 	RP_CarryWeight_Orcleaf		=	 2;	const int	RP_CarryWeight_Oakleaf		=	 2;
const int 	RP_CarryWeight_Shroom01		=	 2;	const int	RP_CarryWeight_Shroom02		=	 2;
const int 	RP_CarryWeight_Herb01		=	 1;	const int	RP_CarryWeight_Herb02		=	 1;
const int 	RP_CarryWeight_Herb03		=	 2;	const int	RP_CarryWeight_Seed01		=	 1;
const int 	RP_CarryWeight_Seed02		=	 1;	const int	RP_CarryWeight_Ravenroot	=	 1;
const int 	RP_CarryWeight_Darkroot		=	 1;	const int	RP_CarryWeight_Root01		=	 1;
const int 	RP_CarryWeight_Root02		=	 1;	const int	RP_CarryWeight_TrollBer		=	 1;

//Tränke mit einfacher Staffelung für Klein/Mittel/Groß
const int 	RP_CarryWeight_Potion01	= 	 5; const int RP_CarryWeight_Potion02			= 	 8;
const int 	RP_CarryWeight_Potion03	= 	 12;

//Waffen 
const int 	RP_CarryWeight_ShortSword	=	16;	const int	RP_CarryWeight_Sword		=	20;
const int 	RP_CarryWeight_LongSword	=	24;	const int	RP_CarryWeight_BroadSword	=	26;
const int 	RP_CarryWeight_2HSword		=	32;	const int	RP_CarryWeight_ShortBow		=	12;
const int 	RP_CarryWeight_LongBow		=	18;	const int	RP_CarryWeight_Crossbow		=	16;
const int 	RP_CarryWeight_CrossbowBig	=	25;	const int 	RP_CarryWeight_Staff03		=	25; 
const int 	RP_CarryWeight_Mace			=	22;	const int	RP_CarryWeight_MaceWar		=	26;
const int 	RP_CarryWeight_Warhammer	=	30;	const int	RP_CarryWeight_Axe01		=	11;
const int 	RP_CarryWeight_Axe02		=	14;	const int	RP_CarryWeight_2HHeavy		=	40;
const int 	RP_CarryWeight_Axe2HLight	=	28;	const int	RP_CarryWeight_Axe2HHeavy	=	34;
const int 	RP_CarryWeight_OrcWeapon	=	42;	const int 	RP_CarryWeight_OrcStaff		=	16;	
const int 	RP_CarryWeight_Club			=	12;	const int	RP_CarryWeight_Poker		=	10;
const int 	RP_CarryWeight_Sickle		=	 8;	const int	RP_CarryWeight_Pickaxe		=	15;
const int 	RP_CarryWeight_Sledgehammer	=	14;	const int	RP_CarryWeight_Keule		=	12;
const int 	RP_CarryWeight_Hatchet		=	 8;	const int	RP_CarryWeight_Nailmace		=	 7;
const int 	RP_CarryWeight_Axe2H		=	36;	const int	RP_CarryWeight_Scythe		=	 9;
const int 	RP_CarryWeight_Staff01		=	13;	const int	RP_CarryWeight_Staff02		=	19;
const int 	RP_CarryWeight_BowWar		=	24; const int 	RP_CarryWeight_Arrow		=	 1;
const int 	RP_CarryWeight_Bolt			=	 1;

//Misc
const int 	RP_CarryWeight_Lockpick		=	 1; const int RP_CarryWeight_Key			=  1;
const int 	RP_CarryWeight_Stomper		= 	 4; const int RP_CarryWeight_Hammer			= 12;
const int 	RP_CarryWeight_Scoop		= 	18; const int RP_CarryWeight_Flask			=  3;
const int 	RP_CarryWeight_SwordRaw		= 	20; const int RP_CarryWeight_SwordBlade		= 16;
const int 	RP_CarryWeight_Torch		= 	 8; const int RP_CarryWeight_Lute			= 23;
const int 	RP_CarryWeight_Wedel		= 	 7; const int RP_CarryWeight_Brush			=  5;
const int 	RP_CarryWeight_Joint		= 	 1; const int RP_CarryWeight_Ore			=  1;
const int 	RP_CarryWeight_SwampHerb	= 	 1; const int RP_CarryWeight_Pipe			=  2;
const int 	RP_CarryWeight_Knife		= 	 3; const int RP_CarryWeight_Coin			=  1;
const int 	RP_CarryWeight_Plate		=	 4; const int RP_CarryWeight_Candle			= 24;
const int 	RP_CarryWeight_Cup01		= 	 6; const int RP_CarryWeight_Cup02			= 10;
const int 	RP_CarryWeight_Silverware	= 	 3; const int RP_CarryWeight_Pan			= 12;
const int 	RP_CarryWeight_Mug			= 	 7; const int RP_CarryWeight_Amphore		=  9;
const int 	RP_CarryWeight_Idol			= 	20; const int RP_CarryWeight_Sulphur		=  2;
const int 	RP_CarryWeight_Quicksilver	= 	 6; const int RP_CarryWeight_Salt			=  2;
const int 	RP_CarryWeight_Oil			= 	 6; const int RP_CarryWeight_MolLube		= 12;
const int 	RP_CarryWeight_Alcohol		= 	 8; 

//Amulette
const int 	RP_CarryWeight_Ring			= 	 1; const int RP_CarryWeight_Amulet			=  2;

//Animaltropy
const int 	RP_CarryWeight_Teeth		=	 2; const int RP_CarryWeight_Claws			=  4;
const int 	RP_CarryWeight_FurWolf		= 	12; const int RP_CarryWeight_FurWarg		= 14;
const int 	RP_CarryWeight_FurShadow	= 	25; const int RP_CarryWeight_FurTrol		= 42;
const int 	RP_CarryWeight_CrwlPlate	= 	21; const int RP_CarryWeight_CrwlMandible	=  6;
const int 	RP_CarryWeight_LurkerClaw	= 	 5; const int RP_CarryWeight_LurkerSkin		=  6;
const int 	RP_CarryWeight_SharkSkin	= 	10; const int RP_CarryWeight_Wings			=  3;
const int 	RP_CarryWeight_Gland		= 	 4; const int RP_CarryWeight_FireTongue		=  1;
const int 	RP_CarryWeight_SharkTeeth	= 	 3; const int RP_CarryWeight_ShadowHorn		=  1;
const int 	RP_CarryWeight_TrollTusk	= 	30; const int RP_CarryWeight_GolemHeart		= 18;

//Rüstung
const int 	RP_CarryWeight_Armor_VLK_L	=	25; const int RP_CarryWeight_Armor_VLK_M	= 30;
const int 	RP_CarryWeight_Armor_STT_M	= 	35; const int RP_CarryWeight_Armor_STT_H	= 45;
const int 	RP_CarryWeight_Armor_GRD_L	= 	45; const int RP_CarryWeight_Armor_GRD_M	= 55;
const int 	RP_CarryWeight_Armor_GRD_H	= 	70; const int RP_CarryWeight_Armor_KDF_L	= 30;
const int 	RP_CarryWeight_Armor_KDF_H	= 	35; const int RP_CarryWeight_Armor_EBR_M	= 75;
const int 	RP_CarryWeight_Armor_EBR_H	= 	80; const int RP_CarryWeight_Armor_SFB_L	= 25;
const int 	RP_CarryWeight_Armor_ORG_L	= 	35; const int RP_CarryWeight_Armor_ORG_M	= 40;
const int 	RP_CarryWeight_Armor_ORG_H	= 	45; const int RP_CarryWeight_Armor_SLD_L	= 25;
const int 	RP_CarryWeight_Armor_SLD_M	= 	50; const int RP_CarryWeight_Armor_SLD_H	= 65;
const int 	RP_CarryWeight_Armor_KDW_L	= 	35; const int RP_CarryWeight_Armor_KDW_H	= 38;
const int 	RP_CarryWeight_Armor_NOV_L	= 	10; const int RP_CarryWeight_Armor_NOV_M	= 25;
const int 	RP_CarryWeight_Armor_NOV_H	= 	30; const int RP_CarryWeight_Armor_TPL_L	= 30;
const int 	RP_CarryWeight_Armor_TPL_M	= 	40; const int RP_CarryWeight_Armor_TPL_H	= 50;
const int 	RP_CarryWeight_Armor_CRW	= 	60; const int RP_CarryWeight_Armor_DMB		= 45;
const int 	RP_CarryWeight_Armor_ORE	= 	95; const int RP_CarryWeight_Armor_GRD_I	= 55;
const int 	RP_CarryWeight_Armor_GUR_M	= 	30;	const int RP_CarryWeight_Armor_GUR_H	= 30; 
const int 	RP_CarryWeight_Armor_LAW	= 	35; const int RP_CarryWeight_Armor_EBR_L	= 30;	//prunkgewand

//Geschriebenes
const int 	RP_CarryWeight_Map			= 	 3; const int RP_CarryWeight_Gear			=  7;
const int 	RP_CarryWeight_Letter		= 	 1; 
//Mission Items
const int 	RP_CarryWeight_Focus		= 	10; const int RP_CarryWeight_Book			= 10;
const int 	RP_CarryWeight_CrwlEgg		= 	12; 

/*
*	Some Strings
*/
const string RP_TXT_WeightPoints = "Gewicht: ";