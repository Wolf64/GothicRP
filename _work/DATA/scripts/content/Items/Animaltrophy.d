// ************************************************************************************************
// Wert von Tiertroph�en
// ************************************************************************************************

// FIXME: welche Troph�e wann freigeschaltet? Wert tunen!
const int	Value_Z�hne				=	10;
const int	Value_Krallen			=	15;

const int	Value_Wolfsfell			=	10;
const int	Value_Orkhundfell		=	15;

const int	Value_Panzerplatte		=	50;
const int	Value_Crawlerzangen		=	10;

const int	Value_Shadowbeastfell	=	100;

const int	Value_Lurkerklaue		=	15;
const int	Value_Lurkerhaut		=	25;

const int	Value_Sumpfhaihaut		=	200;

const int	Value_Trollfell			=	300;

// Bloodfly
const int	Value_Fl�gel			=	15;
const int	Value_Stachel			=	25;

// Durch Ulu-Mulu freigeschaltet
// bei Auftragsvergabe werden je 3 neue Monster gespawnt
const int	Value_Feuerzunge		=	500;
const int	Value_Sumpfhaiz�hne		=	500;
const int	Value_Shadowbeasthorn	=	500;
const int	Value_Trollhauer		=	500;

// Golems beim D�monenbeschw�rer
const int	Value_Steinherz			=	1000;
const int	Value_Feuerherz			=	1000;
const int	Value_Eisherz			=	1000;
const int	Value_Eistueck			=	100;


/*
So in der Tasche
----------------
ItAt_Meatbug_01 	//Meatbug			K�rper (Essen)		(Item-Instanz in Food.d)
ItFoMuttonRaw		//Scavenger			2 Bollen			OK
					//Molerats			2 Bollen			OK
					//Shadowbeast		4 Bollen			OK

ItAt_Bloodfly_01 	//Bloodfly			Fl�gel				OK

Durch J�ger freigeschaltet (noch nicht verteilt)
--------------------------
ItAt_Teeth_01 		//Wolf				2 Z�hne				OK
					//Schwarzer Wolf	2 Z�hne				OK
					//Snapper			10 Z�hne			OK
					//Orcbiter			2 Z�hne				OK
					//Shadowbeast		4 Z�hne				OK
					//Bloodhound		4 Z�hne				OK
//--------------------------------------------------------------
ItAt_Claws_01 		//Waran				4 Klauen			OK
					//Feuerechse		4 Klauen			OK
					//Snapper			3 Klauen			OK
					//Razor				3 Klauen			OK
					//Beisser			2 Klauen			OK
ItAt_Lurker_01 		//Lurker 			2 Klauen			OK
//--------------------------------------------------------------
ItAt_Wolf_01 		//Wolf	 			Wolfsfell			OK
ItAt_Wolf_02 		//Orkhund	 		Orkhundfell			OK
ItAt_Shadow_01 		//Shadowbeast		Shadowbeast-Fell	OK
ItAt_Troll_01 		//Troll				Trollfell			OK
//--------------------------------------------------------------
ItAt_Swampshark_01 	//Sumpfhai			Haut des Sumpfhais	OK
ItAt_Lurker_02 		//Lurker			Haut des Lurkers	OK

Durch Templer / Kalom freigeschaltet (noch nicht verteilt)
------------------------------------
ItAt_Crawler_01 	//Crawler		 	Zangen				OK
ItAt_Bloodfly_02 	//Bloodfly			Stachel				OK

Durch ORG_855_Wolf freigeschaltet
------------------------
ItAt_Crawler_02 	//Crawler			Panzerplatten		OK

Durch Ulu-Mulu freigeschaltet
-----------------------------
ItAt_Waran_01 		//Feuerechse		Flammenzunge		OK
ItAt_Swampshark_02 	//Sumpfhai			Z�hne des Sumpfhais	OK
ItAt_Shadow_02 		//Shadowbeast		Horn				OK
ItAt_Troll_02 		//Troll				Hauer				OK
*/

/******************************************************************************************/
INSTANCE ItAt_Teeth_01 (C_Item)
{
	name 				=	"Z�hne";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Z�hne;
	weight				=	RP_CarryWeight_Teeth;

	visual 				=	"ItAt_Teeth_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Z�hne;
};

/******************************************************************************************/
INSTANCE ItAt_Wolf_01 (C_Item)
{
	name 				=	"Wolfsfell";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Wolfsfell;
	weight				=	RP_CarryWeight_FurWolf;

	visual 				=	"ItAt_Wolf_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Wolfsfell;
};
/******************************************************************************************/
INSTANCE ItAt_Wolf_02 (C_Item)
{
	name 				=	"Orkhundfell";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Orkhundfell;
	weight				=	RP_CarryWeight_FurWarg;

	visual 				=	"ItAt_Wolf_02.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Orkhundfell;
};
/******************************************************************************************/
INSTANCE ItAt_Waran_01 (C_Item)
{
	name 				=	"Flammenzunge";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Feuerzunge;
	weight				=	RP_CarryWeight_FireTongue;

	visual 				=	"ItAt_Waran_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Feuerzunge;
};
/******************************************************************************************/
INSTANCE ItAt_Claws_01 (C_Item)
{
	name 				=	"Krallen";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Krallen;
	weight				=	RP_CarryWeight_Claws;

	visual 				=	"ItAt_Claws_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Krallen;
};
/******************************************************************************************/
INSTANCE ItAt_Crawler_02 (C_Item)
{
	name 				=	"Panzerplatte";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI|ITEM_MISSION;

	value 				=	Value_Panzerplatte;
	weight				=	RP_CarryWeight_CrwlPlate;

	visual 				=	"ItAt_Crawler_02.3DS";
	material 			=	MAT_WOOD;

	description			= "Die Panzerplatte eines Minecrawler Kriegers";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Panzerplatte;
};

/******************************************************************************************/
INSTANCE ItAt_Crawler_01 (C_Item)
{
	name 				=	"Crawlerzangen";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Crawlerzangen;
	weight				=	RP_CarryWeight_CrwlMandible;

	visual 				=	"ItAt_Crawler_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Crawlerzangen;
};

/******************************************************************************************/
INSTANCE ItAt_Shadow_01 (C_Item)
{
	name 				=	"Fell";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Shadowbeastfell;
	weight				=	RP_CarryWeight_FurShadow;

	visual 				=	"ItAt_Shadowbeast_01.3DS";
	material 			=	MAT_LEATHER;

	description			= "Fell eines Schattenl�ufers";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Shadowbeastfell;
};

/******************************************************************************************/
INSTANCE ItAt_Shadow_02 (C_Item)
{
	name 				=	"Horn";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Shadowbeasthorn;
	weight				=	RP_CarryWeight_ShadowHorn;

	visual 				=	"ItAt_Shadowbeast_02.3DS";
	material 			=	MAT_LEATHER;

	description			= "Horn eines Schattenl�ufers";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Shadowbeasthorn;
};

/******************************************************************************************/
INSTANCE ItAt_Lurker_01 (C_Item)
{
	name 				=	"Lurkerklaue";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Lurkerklaue;
	weight				=	RP_CarryWeight_LurkerClaw;

	visual 				=	"ItAt_Lurker_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Lurkerklaue;
};

/******************************************************************************************/
INSTANCE ItAt_Lurker_02 (C_Item)
{
	name 				=	"Lurkerhaut";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Lurkerhaut;
	weight				=	RP_CarryWeight_LurkerSkin;

	visual 				=	"ItAt_Lurker_02.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Lurkerhaut;
};

/******************************************************************************************/
INSTANCE ItAt_Troll_02 (C_Item)
{
	name 				=	"Trollhauer";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Trollhauer;
	weight				=	RP_CarryWeight_TrollTusk;

	visual 				=	"ItAt_Troll_02.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Trollhauer;
};

/******************************************************************************************/
INSTANCE ItAt_Troll_01 (C_Item)
{
	name 				=	"Trollfell";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Trollfell;
	weight				=	RP_CarryWeight_FurTrol;

	visual 				=	"ItAt_Troll_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Trollfell;
};
/******************************************************************************************/
INSTANCE ItAt_Swampshark_01 (C_Item)
{
	name 				=	"Sumpfhaihaut";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Sumpfhaihaut;
	weight				=	RP_CarryWeight_SharkSkin;

	visual 				=	"ItAt_Swampshark_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Sumpfhaihaut;
};
/******************************************************************************************/
INSTANCE ItAt_Swampshark_02 (C_Item)
{
	name 				=	"Sumpfhaiz�hne";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Sumpfhaiz�hne;
	weight				=	RP_CarryWeight_SharkTeeth;

	visual 				=	"ItAt_Swampshark_02.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Sumpfhaiz�hne;
};
/******************************************************************************************/
INSTANCE ItAt_Bloodfly_01 (C_Item)
{
	name 				=	"Fl�gel";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Fl�gel;
	weight				=	RP_CarryWeight_Wings;

	visual 				=	"ItAt_Bloodfly_01.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Fl�gel;
};
/******************************************************************************************/
INSTANCE ItAt_Bloodfly_02 (C_Item)
{
	name 				=	"Blutfliegenstachel";

	mainflag 			=	ITEM_KAT_NONE;
	flags 				=	ITEM_MULTI;

	value 				=	Value_Stachel;
	weight				=	RP_CarryWeight_Gland;

	visual 				=	"ItAt_Bloodfly_02.3DS";
	material 			=	MAT_LEATHER;

	description			= name;
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Stachel;
};



/******************************************************************************************/
INSTANCE ItAt_StoneGolem_01 (C_Item)
{
	name 				=	"Herz eines Steingolems";

	mainflag 			=	ITEM_KAT_RUNE;
	flags 				=	ITEM_MISSION;
	spell				= 	SPL_WINDFIST;

	value 				=	Value_Steinherz;
	weight				=	RP_CarryWeight_GolemHeart;

	visual 				=	"ItAt_StoneGolem_01.3DS";
	material 			=	MAT_STONE;

	description			= name;
	TEXT[0]				= "Dieses Herz stammt aus den �berresten";
	TEXT[1]				= "eines besiegten Steingolems.";
	TEXT[2]				= "Es hat die Kraft, einen Gegner"; 
	TEXT[3]				= "durch die Luft zu schleudern.";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Steinherz;
};

/******************************************************************************************/
INSTANCE ItAt_FireGolem_01 (C_Item)
{
	name 				=	"Herz eines Feuergolems";

	mainflag 			=	ITEM_KAT_RUNE;
	flags 				=	ITEM_MISSION;
	spell				= 	SPL_FIREBALL;

	value 				=	Value_Feuerherz;
	weight				=	RP_CarryWeight_GolemHeart;

	visual 				=	"ItAt_FireGolem_01.3DS";
	material 			=	MAT_STONE;

	description			= name;
	TEXT[0]				= "Dieses Herz stammt aus den �berresten";
	TEXT[1]				= "eines besiegten Feuergolems.";
	TEXT[2]				= "Es hat die Kraft, einen Gegner";
	TEXT[3]				= "in Flammen aufgehen zu lassen.";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Feuerherz;
};

/******************************************************************************************/
INSTANCE ItAt_IceGolem_01 (C_Item)
{
	name 				=	"Herz eines Eisgolems";

	mainflag 			=	ITEM_KAT_RUNE;
	flags 				=	ITEM_MISSION;
	spell				= 	SPL_ICECUBE;

	value 				=	Value_Eisherz;
	weight				=	RP_CarryWeight_GolemHeart;

	visual 				=	"ItAt_IceGolem_01.3DS";
	material 			=	MAT_STONE;

	description			= name;
	TEXT[0]				= "Dieses Herz stammt aus den �berresten";
	TEXT[1]				= "eines besiegten Eisgolems.";
	TEXT[2]				= "Es hat die Kraft, einen Gegner";
	TEXT[3]				= "zu Eis erstarren zu lassen.";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Eisherz;
};

/******************************************************************************************/
INSTANCE ItAt_IceGolem_02 (C_Item)
{
	name 				=	"St�ck eines Eisgolems";

	mainflag 			=	ITEM_KAT_RUNE;
	flags 				=	0;
	spell				= 	SPL_THUNDERBOLT;

	value 				=	Value_Eistueck;
	weight				= 	2;
	cond_atr[2]   		= 	ATR_MANA_MAX;
	cond_value[2]  		= 	500;

	visual 				=	"ItAt_IceGolem_01.3DS";
	material 			=	MAT_STONE;

	description			= name;
	TEXT[0]				= "Dieses St�ck stammt aus den �berresten";
	TEXT[1]				= "eines besiegten Eisgolems.";
	TEXT[4]				= RP_TXT_WeightPoints;			COUNT[4]	= weight;
	TEXT[5]				= NAME_Value;					COUNT[5]	= Value_Eistueck;
};
