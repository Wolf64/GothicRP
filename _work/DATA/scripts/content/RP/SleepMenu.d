/****************
*	Sleep Menu
****************/
var int RP_SleepMenu_Initialized;

var int RP_SleepMenu_Hours;

var int RP_SleepMenu_ViewHndl;		var int RP_SleepMenu_BtnPlusHndl;
var int RP_SleepMenu_BtnMinusHndl;	var int RP_SleepMenu_HoursTextHndl;
var int RP_SleepMenu_BtnCloseHndl;	var int RP_SleepMenu_BtnConfirmHndl;

const string buttonTex 				= "INV_SLOT_EQUIPPED.TGA";
const string buttonHightlightTex	= "INV_SLOT_EQUIPPED_HIGHLIGHTED.TGA";
const string backTex 				= "STATUS_BACK.TGA";

func void RP_SleepMenu_Init()
{
	var int posx;	var int posy;
	Print_GetScreenSize();
	
	posx = PS_VMax/2 + 600;	//etwas rechts von der Midde
	posy = PS_VMax/2;
	RP_SleepMenu_BtnPlusHndl = Button_Create(posx, posy, 256, 300, buttonTex, _RP_SleepMenu_ButtonIncHighlight, _RP_SleepMenu_ButtonIncHighlight, _RP_SleepMenu_Increment);
	Button_SetCaption(RP_SleepMenu_BtnPlusHndl, "+", "font_old_20_white.tga");
	
	posx = PS_VMax/2 - 600;	//etwas links von der Midde
	RP_SleepMenu_BtnMinusHndl = Button_Create(posx, posy, 256, 300, buttonTex, _RP_SleepMenu_ButtonDecHighlight, _RP_SleepMenu_ButtonDecHighlight, _RP_SleepMenu_Decrement);
	Button_SetCaption(RP_SleepMenu_BtnMinusHndl, "-", "font_old_20_white.tga");
	
	posx = PS_VMax/2;
	RP_SleepMenu_BtnConfirmHndl = Button_Create(posx, posy, 256, 300, buttonTex, _RP_SleepMenu_ButtonConfirmHighlight, _RP_SleepMenu_ButtonConfirmHighlight, _RP_SleepMenu_Confirm);
	Button_SetCaption(RP_SleepMenu_BtnConfirmHndl, "OK", "font_old_20_white.tga");
	
	posx = PS_VMax/2 + 400;	//ecke unten rechts
	posy = PS_VMax/2 + 400;
	RP_SleepMenu_BtnCloseHndl = Button_Create(posx, posy, 300, 300, buttonTex, _RP_SleepMenu_ButtonCancHighlight, _RP_SleepMenu_ButtonCancHighlight, _RP_SleepMenu_Close);
	Button_SetCaption(RP_SleepMenu_BtnCloseHndl, "X", "font_old_20_white.tga");
	
	posx = Print_Screen[PS_X]/2;
	posy = Print_Screen[PS_Y]/2;
	RP_SleepMenu_ViewHndl = View_CreateCenterPxl(posx, posy, 512, 256);
	View_SetTexture(RP_SleepMenu_ViewHndl, backTex);

	RP_SleepMenu_Initialized = true;
};

func void RP_ShowSleepMenu()
{
	Cursor_Show();
	Cursor_NoEngine = true;
	
	View_Open(RP_SleepMenu_ViewHndl);
	Button_Show(RP_SleepMenu_BtnPlusHndl);
	Button_Show(RP_SleepMenu_BtnMinusHndl);
	Button_Show(RP_SleepMenu_BtnCloseHndl);
	Button_Show(RP_SleepMenu_BtnConfirmHndl);
	
	var int posx; posx = Print_Screen[PS_X]/2+400;
	var int posy; posy = Print_Screen[PS_Y]/2+200;
	var string hrs; hrs = IntToString(RP_SleepMenu_Hours);
	hrs = ConcatStrings(hrs, "h schlafen?");
	View_DeleteText(RP_SleepMenu_ViewHndl);
	View_AddText(RP_SleepMenu_ViewHndl, posx, posy, hrs, "font_old_20_white.tga");
};

func void _RP_SleepMenu_Increment()
{
	if ((RP_SleepMenu_Hours + 1) > 24) { return; };
	
	RP_SleepMenu_Hours += 1;
	
	var int posx; posx = Print_Screen[PS_X]/2+400;
	var int posy; posy = Print_Screen[PS_Y]/2+200;
	var string hrs; hrs = IntToString(RP_SleepMenu_Hours);
	hrs = ConcatStrings(hrs, "h schlafen?");
	View_DeleteText(RP_SleepMenu_ViewHndl);
	View_AddText(RP_SleepMenu_ViewHndl, posx, posy, hrs, "font_old_20_white.tga");
};

func void _RP_SleepMenu_Decrement()
{
	if ((RP_SleepMenu_Hours - 1) < 0) { return; };
	
	RP_SleepMenu_Hours -= 1;
	
	var int posx; posx = Print_Screen[PS_X]/2+400;
	var int posy; posy = Print_Screen[PS_Y]/2+200;
	var string hrs; hrs = IntToString(RP_SleepMenu_Hours);
	hrs = ConcatStrings(hrs, "h schlafen?");
	View_DeleteText(RP_SleepMenu_ViewHndl);
	View_AddText(RP_SleepMenu_ViewHndl, posx, posy, hrs, "font_old_20_white.tga");
};

func void _RP_SleepMenu_Close()
{
	Button_Hide(RP_SleepMenu_BtnPlusHndl);
	Button_Hide(RP_SleepMenu_BtnMinusHndl);
	Button_Hide(RP_SleepMenu_BtnCloseHndl);
	Button_Hide(RP_SleepMenu_BtnConfirmHndl);
	View_DeleteText(RP_SleepMenu_ViewHndl);
	View_Close(RP_SleepMenu_ViewHndl);
	Cursor_Hide();
	Cursor_NoEngine = false;
};

//angepasst f�r neues Sleep Menu
func void PC_Sleep (var int t)
{
	AI_StopProcessInfos(self);		// [SK] ->muss hier stehen um das update zu gewährleisten
	self.aivar[AIV_INVINCIBLE]=FALSE;
	
	//Sektenspinners Time Scripts machen alles unglaublich einfach!
	Wld_AddTime(0, t, 0);
	
	//RP Respawn (auch in B_RefreshArmor())
	RP_CheckRespawns();
	
	//RP Needs
	if (RP_Needs_Initialized)
	{
		RP_Needs_ConsumeHoursSlept(t);
	};
	
	PrintScreen	("Du hast geschlafen und bist ausgeruht!", -1,-1,"font_old_20_white.tga",3);
	hero.attribute[ATR_HITPOINTS] = hero.attribute[ATR_HITPOINTS_MAX];
	hero.attribute[ATR_MANA] = hero.attribute[ATR_MANA_MAX];	

	//-------- AssessEnterRoom-Wahrnehmung versenden --------
	PrintGlobals		(PD_ITEM_MOBSI);
	Npc_SendPassivePerc	(hero,	PERC_ASSESSENTERROOM, NULL, hero);		//...damit der Spieler dieses Feature nicht zum Hütteplündern ausnutzt!
};

func void _RP_SleepMenu_Confirm()
{
	PC_Sleep(RP_SleepMenu_Hours);
	_RP_SleepMenu_Close();
};


func void _RP_SleepMenu_ButtonIncHighlight()
{
	var int highlight;
	
	if(highlight)
	{
		Button_SetTexture(RP_SleepMenu_BtnPlusHndl, buttonTex);
		highlight = false;
	}
	else
	{
		Button_SetTexture(RP_SleepMenu_BtnPlusHndl, buttonHightlightTex);
		highlight = true;
	};
};

func void _RP_SleepMenu_ButtonDecHighlight()
{
	var int highlight;
	
	if(highlight)
	{
		Button_SetTexture(RP_SleepMenu_BtnMinusHndl, buttonTex);
		highlight = false;
	}
	else
	{
		Button_SetTexture(RP_SleepMenu_BtnMinusHndl, buttonHightlightTex);
		highlight = true;
	};
};

func void _RP_SleepMenu_ButtonCancHighlight()
{
	var int highlight;
	
	if(highlight)
	{
		Button_SetTexture(RP_SleepMenu_BtnCloseHndl, buttonTex);
		highlight = false;
	}
	else
	{
		Button_SetTexture(RP_SleepMenu_BtnCloseHndl, buttonHightlightTex);
		highlight = true;
	};
};

func void _RP_SleepMenu_ButtonConfirmHighlight()
{
	var int highlight;
	
	if(highlight)
	{
		Button_SetTexture(RP_SleepMenu_BtnConfirmHndl, buttonTex);
		highlight = false;
	}
	else
	{
		Button_SetTexture(RP_SleepMenu_BtnConfirmHndl, buttonHightlightTex);
		highlight = true;
	};
};