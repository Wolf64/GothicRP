#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

TMP_DIR=./tmp
SYS_DIR=./System
WORK_DIR=./_work
MILES_DIR=./Miles
DATA_DIR=./Data
MDK_DIR=./_work/DATA/Scripts/content/Story
IKARUS_DIR=./_work/DATA/Scripts/content/Ikarus
LEGO_DIR=./_work/DATA/Scripts/content/LeGo
PLAYERKIT=./system/GothicStarter_mod.exe
SYSTEMPACK=./system/Systempack.ini
AFSP_DIR=./_work/DATA/Scripts/content/AFSP
UNION=./system/ResourceManager.exe

########## helper functions ##########

function check_component() {
  if [ $# -ne 3 ]; then
    echo "Error: Exactly 3 arguments required!"
    return 1
  fi
  if [ ! $3 $1 ]; then
    printf "${RED}- $2 does not seem to be installed.\n"
    return 0
  else
    printf "${GREEN}- $2 found!\n${NC}"
    return 1
  fi
}

############ actual install script ########
function main() {
  printf "\n===== Gothic 1 MDK Setup Script =====\n"

  if [ -d ${TMP_DIR,,} ]; then
    echo "Now where's my broom? Cleaning up previous tmp..."
    rm -rf tmp
  fi

  if [ ! -d ${SYS_DIR,,} ] || [ ! -d ${WORK_DIR,,} ] || [ ! -d ${MILES_DIR,,} ] || [ ! -d ${DATA_DIR,,} ]; then
    printf "${RED}It seems like you're not in the root of your Gothic install dir. The script must be executed from there.\n"
    exit 1
  fi

  printf "${GREEN}Gothic directory recognized! Checking components...\n${NC}"
  check_component ${PLAYERKIT,,} "Playerkit" -f
  local player_kit_installed=$?
  check_component ${SYSTEMPACK,,} "SystemPack" -f
  local system_pack_installed=$?
  check_component ${MDK_DIR,,} "MDK" -d
  local mdk_installed=$?
  check_component ${IKARUS_DIR,,} "Ikarus" -d
  local ikarus_installed=$?
  check_component ${LEGO_DIR,,} "LeGo" -d
  local lego_installed=$?
  check_component ${AFSP_DIR,,} "AFSP" -d
  local afsp_installed=$?
  check_component ${UNION,,} "Union" -f
  local union_installed=$?

  if [ $player_kit_installed = 1 -a $system_pack_installed = 1 -a $mdk_installed = 1 -a $ikarus_installed = 1 -a $afsp_installed = 1 -a $lego_installed = 1 -a $union_installed = 1 ]; then
    echo "Nothing to do here, bye!"
    exit 0
  fi

  printf "${NC}Creating tmp folder... "
  mkdir tmp
  cd tmp
  printf "done!\n"

  echo "Checking dependencies... "

  if ! [ -x "$(command -v wget)" ]; then
    echo "wget not found, downloading..."
    curl -O https://eternallybored.org/misc/wget/1.21.4/64/wget.exe

    # move wget to git bash bin... how to find???
    #while read -r line
    #do
      #if [[ "$line" =~ .*"/Git/usr/bin".* ]]; then
        #git_path=${line%" on"*}
        #printf "installing into '"
        #printf "$git_path"
        #printf "'\n"

        #mv wget.exe "$git_path/wget.exe" #permission issues because C:/Program Files
      #fi
    #done < <(mount)
  fi

  ########################## essentials ################################
  echo $player_kit_installed
  echo $player_kit_installed = 0
  if [ $player_kit_installed = 0 ]; then
    echo "Downloading PlayerKit..."
    ./wget.exe -O playerkit.exe "https://www.worldofgothic.de/download.php?id=61"
    ./playerkit.exe
  fi

  # SystemPack can be skipped if Union should be installed later on
  if [ $system_pack_installed = 0 ]; then
    echo "Downloading SystemPack..."
    ./wget.exe -O systempack.exe "https://www.worldofgothic.de/download.php?id=1523"
    ./systempack.exe
  fi

  if [ $mdk_installed = 0 ]; then
    #echo "Downloading 7z executable..."
    #curl -O https://7-zip.org/a/7zr.exe

    echo "Downloading G1 MDK devkit compilation..."
    #curl -O https://dl6.worldofplayers.de/dl/fc229c9db2a443491ad94a858b48b847/659e9d77/wog/gothic1/editing/gothic_devkit_1.2.7z
    #./wget.exe -O mdk.7z https://www.worldofgothic.de/download.php?id=1582
    #./7zr.exe x -aoa mdk.7z

    git clone https://github.com/PhoenixTales/gothic-devkit.git MDK

    echo "Moving G1 MDK into place..."
    cd "MDK"
    cp -rT -f gothic/ ../../../Gothic # forces override, should probably remove for scripts!
    cd ..
  fi

########################## script stuff ################################

  if [ $ikarus_installed = 0 ]; then
    echo "Cloning Ikarus master..."
    git clone https://github.com/Lehona/Ikarus.git
    cp -rT -f Ikarus ../_work/DATA/Scripts/content/Ikarus
  fi

  if [ $lego_installed = 0 ]; then
    echo "Cloning LeGo master..."
    git clone https://github.com/Lehona/LeGo.git
    cd LeGo
    local currentTag=$(git describe --tags)
    git checkout tags/$currentTag
    cd ..
    cp -rT -f LeGo ../_work/DATA/Scripts/content/LeGo
  fi

  if [ $afsp_installed = 0 ]; then
    echo "Cloning AFSP master..."
    #git clone https://github.com/Bad-Scientists/AF-Script-Packet.git AFSP
    git clone https://github.com/Wolf-64/AFSP.git # using my own fork
    cp -rT -f AFSP ../_work/DATA/Scripts/content/AFSP
  fi


  # we'll place Union's Resource Manager in the Gothic root folder to keep it around and not throw it away later with the rest
  cd ../system

  if [ $union_installed = 0 ]; then
    echo "Downloading Union..."
    curl -L "https://drive.usercontent.google.com/download?id=18si3zHD6cnIPeg1Snhyop8bnPdTTnVFT&confirm=xxx" -o ResourceManager.exe
    ./ResourceManager.exe
  fi

  printf "${GREEN}You're all set up! Happy modding!\n${NC}"
}

main "$@"