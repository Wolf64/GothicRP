/***********************************
*	Carry Weight System
************************************/
var int RP_CarryWeight_Initialized;

//10 weight units should be approx. 10kg
//5 weight would be 0,5kg then
var int 	RP_CarryWeight;
const int 	RP_CarryWeightMax = 300;	//taken from The Long Dark survival game as reference
var int 	RP_WeightBarHndl;

//Um overlays auch nur dann zu entfernen, falls wirklich eins
//existiert. Sonst ploppt der SC von Truhen weg und die
//Truhe verbleibt effektiv unnutzbar.
//FIXME: Trotzdem sollte das Overlay erst NACH DEM SCHLIESSEN
//	der Truhe angehängt werden, sonst passiert der gleiche Fehler!
var int RP_CarryWeight_SlowedDown;	
var int RP_CarryWeight_CanApplySlowdown;
var int RP_CarryWeight_CanRemoveSlowdown;

func void RP_ShowEncumberment()
{
	if(RP_CarryWeight_Initialized)
	{
		Bar_Show(RP_WeightBarHndl);
	};
};

func void RP_HideEncumberment()
{
	if(RP_CarryWeight_Initialized)
	{
		Bar_Hide(RP_WeightBarHndl);
	};
};

/*
*	Erhalt von Items durch Dialoge in B_Functions.d
*					(B_GiveInvItems)
*/

func void RP_FF_CarryWight_WaitForOverlay()
{
	if(RP_CarryWeight_CanApplySlowdown == TRUE && !RP_C_HasUnwantedBS())
	{
		RP_CarryWeight_CanApplySlowdown = FALSE;
		RP_CarryWeight_SlowedDown = TRUE;
		Mdl_ApplyOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
		FF_Remove(RP_FF_CarryWight_WaitForOverlay);
	};

	if(RP_CarryWeight_CanRemoveSlowdown == TRUE && !RP_C_HasUnwantedBS())
	{
		RP_CarryWeight_CanRemoveSlowdown = FALSE;
		RP_CarryWeight_SlowedDown = FALSE;
		Mdl_RemoveOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
		FF_Remove(RP_FF_CarryWight_WaitForOverlay);
	};
};

func void RP_CarryWeight_ApplySlowdown()
{
	if (RP_CarryWeight_SlowedDown == FALSE && !RP_C_HasUnwantedBS()) //TRUHEN!!
	{
		RP_CarryWeight_SlowedDown = TRUE;
		Mdl_ApplyOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
		RP_CarryWeight_CanApplySlowdown = FALSE; //... da ja bereits applied ist
	}
	else
	{
		RP_CarryWeight_CanApplySlowdown = TRUE; //sobald ein passender state erreicht wird, kann applied werden
		FF_ApplyOnceExtGT(RP_FF_CarryWight_WaitForOverlay, 0, -1);
	};

	B_SayOverlay(PC_Hero, NULL,"$INVFULL");
};

func void RP_CarryWeight_RemoveSlowdown()
{
	if (RP_CarryWeight_SlowedDown == TRUE && !RP_C_HasUnwantedBS()) //TRUHEN!!
	{
		Mdl_RemoveOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
		RP_CarryWeight_SlowedDown = TRUE;
		RP_CarryWeight_CanRemoveSlowdown = FALSE; //... da ja bereits removed ist
	}
	else
	{
		RP_CarryWeight_CanRemoveSlowdown = TRUE; //sobald ein passender state erreicht wird, kann removed werden
		FF_ApplyOnceExtGT(RP_FF_CarryWight_WaitForOverlay, 0, -1);
	};
};

/***********************************
*	Pickup und Drop Funktionen		*
************************************/
func void RP_CarryWeight_PickUp(var c_npc slf, var C_ITEM itm, var int amt)
{
	//nur für den hero aktionen ausführen!
	if(Hlp_GetInstanceID(slf) == Hlp_GetInstanceID(PC_Hero))
	{
		//Die Anzahl von gestackten Items wird mit "amt" berücksichtigt
		RP_CarryWeight += itm.weight*amt;
		
		if (RP_CarryWeight > RP_CarryWeightMax)
		{
			//PrintScreen	("APPLY SLOWDOWN!", -1,-1,"font_old_20_white.tga",3);
			RP_CarryWeight_ApplySlowdown();
		};
		
		//Bar_SetMax(RP_WeightBarHndl, RP_CarryWeightMax);
		Bar_SetValue(RP_WeightBarHndl, RP_CarryWeight);
	};
};

func void RP_CarryWeight_Drop(var c_npc slf, var C_ITEM itm, var int amt)
{
	//nur für den hero aktionen ausführen!
	if(Hlp_GetInstanceID(slf) == Hlp_GetInstanceID(PC_Hero))
	{
		//Die Anzahl von gestackten Items wird mit "amt" berücksichtigt
		RP_CarryWeight -= itm.weight*amt;
		
		if (RP_CarryWeight < RP_CarryWeightMax && RP_CarryWeight_SlowedDown == true)
		{
			//PrintScreen	("DISPEL SLOWDOWN!", -1,-1,"font_old_20_white.tga",3);
			RP_CarryWeight_RemoveSlowdown();
		};
		
		//Safety first
		if(RP_CarryWeight < 0)
		{
			RP_CarryWeight = 0;
		};
		
		Bar_SetValue(RP_WeightBarHndl, RP_CarryWeight);
	};
};

/***********************************
*	INTERN		Hook Funktionen		*
************************************/
/*	Ist nach RP_HookFuncs.d gewandert
func void _RP_CarryWeight_PickUp(var c_npc slf, var c_item itm, var oCItem itmobj)
{
	//	----- an den NPC und das item kommen -----
	//wird in RP_HF_DoTakeVob entnommen!
	//	----- stimmt das item? -----
	if (!Hlp_IsValidItem (itm)) { return; };
	if (!Hlp_IsValidNpc (slf)) { return; };
	
	RP_CarryWeight_PickUp(slf, itm, itmobj.amount);
};
*/

//OBSOLETE
func void _RP_CarryWeight_DropContainer()
{
	PrintScreen	("Dropped item to container.", -1,-1,"font_old_20_white.tga",3);
};

//by F a w k e s
FUNC void RP_CarryWeight_AddInvCategoryWeight (var C_NPC slf, var int inv_category)
{
    var int p;
    var int itm_slot; itm_slot = 0;
    var int amount;
	
    //Loop
    p = MEM_StackPos.position;

    //Is there any item in itm_slot ?
	amount = NPC_GetInvItemBySlot (slf, inv_category, itm_slot);
    if (amount > 0)
    {
        //item is filled by above NPC_GetInvItemBySlot
		RP_CarryWeight_PickUp(hero, item, amount);
		
		//var string str; str = "Check: ";
		//var string hlp; hlp = IntToString(amount);
		//str = ConcatStrings(str, item.name);
		//str = ConcatStrings(str, " x");
		//str = ConcatStrings(str, hlp);
		
		//Print_Ext(350, 300, str, "font_old_20_white.tga", RGBA(255, 0, 0, 255), 3*1000);
        
        itm_slot = itm_slot + 1;
        MEM_StackPos.position = p;
    };
};

FUNC void RP_CarryWeight_RecalcInventory ()
{
	//PrintScreen	("Recalculating Inventory...", -1,-1,"font_old_20_white.tga",3);
	
	RP_CarryWeight = 0;
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_WEAPON);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_ARMOR);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_RUNE);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_MAGIC);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_FOOD);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_POTION);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_DOC);
    RP_CarryWeight_AddInvCategoryWeight (hero, INV_MISC);
};

//////////////////////////////////////////////////////////////////////////
//	B_GiveInvItems
//	==============
//	Übergibt ein Item von 'giver' an 'taker' und gibt eine Meldung
//	auf dem Bildschirm aus.
//////////////////////////////////////////////////////////////////////////
func void B_GiveInvItems(var C_NPC giver, var C_NPC taker, var int itemInstance, var int amount)
{
	PrintDebugNpc	(PD_ZS_DETAIL,	"B_GiveInvItems");

	//RP - Carry Weight aufaddieren
	Npc_GetInvItem(giver,itemInstance);		//befüllt globale item Variable

	//-------- Gegenstand übertragen --------
	Npc_RemoveInvItems	(giver,	itemInstance,	amount);
	CreateInvItems		(taker,	itemInstance,	amount);

	//-------- Meldung ausgeben --------
	var string msg;

	if	Npc_IsPlayer(giver)
	{
		RP_CarryWeight_Drop(giver, item, amount);
		if (itemInstance == ItMiNugget)
		{
			msg = ConcatStrings(IntToString(amount), _STR_MESSAGE_ORE_GIVEN);
			PrintScreen	(msg, -1,_YPOS_MESSAGE_GIVEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_GIVEN);
		}
		else
		{
		    if amount == 1
		    {
			    msg = ConcatStrings(IntToString(amount), _STR_MESSAGE_ITEM_GIVEN);
			    PrintScreen	(msg, -1,_YPOS_MESSAGE_GIVEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_GIVEN);
			}   
			else
		    {
			    msg = ConcatStrings(IntToString(amount),_STR_MESSAGE_ITEMS_GIVEN);
			    PrintScreen	(msg, -1,_YPOS_MESSAGE_GIVEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_GIVEN);
			};   
		};
	}
	else if Npc_IsPlayer(taker)
	{
		RP_CarryWeight_PickUp(taker, item, amount);
		if (itemInstance == ItMiNugget)
		{
			msg = ConcatStrings(IntToString(amount), _STR_MESSAGE_ORE_TAKEN);
			PrintScreen	(msg, -1,_YPOS_MESSAGE_TAKEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_TAKEN);
		}
		else
		{
		    if amount == 1
		    {
			    msg = ConcatStrings(IntToString(amount), _STR_MESSAGE_ITEM_TAKEN);
			    PrintScreen	(msg, -1,_YPOS_MESSAGE_TAKEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_TAKEN);
			}   
			else
		    {
			    msg = ConcatStrings(IntToString(amount),_STR_MESSAGE_ITEMS_TAKEN);
			    PrintScreen	(msg, -1,_YPOS_MESSAGE_TAKEN,"FONT_OLD_10_WHITE.TGA",_TIME_MESSAGE_TAKEN);
			}; 
		};
	};	
};

func void RP_InitCarryWeight()
{
	if(!Hlp_IsValidHandle(RP_WeightBarHndl)) { RP_WeightBarHndl = Bar_Create(RP_CarryWeight_Bar); };
	
    Bar_SetMax(RP_WeightBarHndl, RP_CarryWeightMax);
    Bar_SetValue(RP_WeightBarHndl, RP_CarryWeight);
	Bar_Hide(RP_WeightBarHndl); // nur im Inventar anzeigen?
	
	RP_CarryWeight_Initialized = true;
};