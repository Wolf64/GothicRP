/***********************************
*	CarryWeigt System Instanzen		*
************************************/

//Bar Instanz, wird knapp �ber der ManaBar dargestellt
instance RP_CarryWeight_Bar(GothicBar)
{
	x = Print_Screen[PS_X] - RP_CarryWeight_Bar_X;
	y = Print_Screen[PS_Y] - RP_CarryWeight_Bar_Y_Offset;
	barTex = "Bar_Weight.tga";
	//backTex = "";
};

/************************************
*		Debuff Stufe 5  			*
************************************/
instance RP_Needs_DebuffHunger_L5(lCBuff)
{
    name = "Hunger Stufe 1";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_HungerTickMS+500;
	tickMS = 1000;
        
    OnApply = SAVE_GetFuncID(RP_CarryWeight_Debuff_L5_Apply);
	OnRemoved = SAVE_GetFuncID(RP_CarryWeight_Debuff_L5_Remove);
    buffTex = "HUNGER_L1.TGA";
};

func void RP_CarryWeight_Debuff_L5_Apply()
{
    Mdl_ApplyOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
};

func void RP_CarryWeight_Debuff_L5_Remove()
{
    Mdl_ApplyOverlayMds(PC_Hero, "HUMANS_OVERCUMBERED.MDS");
};
