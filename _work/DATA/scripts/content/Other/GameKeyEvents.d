/*
 * gameKeyEvents.d
 * Source: https://forum.worldofplayers.de/forum/threads/?p=26055992
 *
 * Handle key events from the player the way Gothic is doing it
 *
 * - Requires Ikarus, LeGo (HookEngine)
 * - Compatible with Gothic 1 and Gothic 2
 *
 * Instructions
 * - Initialize from Init_Global with
 *     Game_KeyEventInit();
 * - Add your key event detection in Game_KeyEvent
 */


/*
 * Customizable function to handle key events (pressed is FALSE: key is held, pressed is TRUE: key press onset)
 * This function has to return TRUE if the given key was handled, and FALSE otherwise
 */
func int Game_KeyEvent(var int key, var int pressed) {
    // TODO pack key and pressed values into single int and use Events for key press listeners?
    if ((key == MEM_GetKey("keySit"))
        || MEM_GetSecondaryKey("keySit") && (pressed)) {
        RP_Hotkey_Sit();
        return TRUE;
    }
    else if ((key == MEM_GetKey("keyUp"))
        || MEM_GetSecondaryKey("keyUp") && (pressed)) {
        RP_Hotkey_GetUp();
        return TRUE;
    };
    return FALSE;
};


/*
 * This function is called during the running game when a key is pressed/held that is not already handled by Gothic
 */
func void Game_KeyEvent_() {
    if (Game_KeyEvent(ESI, MEM_ReadByte(MEMINT_KeyToggle_Offset + ESI))) {
        MEM_WriteByte(MEMINT_KeyToggle_Offset + ESI, 0); // Change toggle state only if key event was handled
    };
};


/*
 * Initialization function for custom key events
 */
func void Game_KeyEventInit() {
    const int oCGame__HandleEvent_dfltCase_G1 = 6684404; //0x65FEF4
    const int oCGame__HandleEvent_dfltCase_G2 = 7328820; //0x6FD434
    HookEngineF(+MEMINT_SwitchG1G2(oCGame__HandleEvent_dfltCase_G1,
                                   oCGame__HandleEvent_dfltCase_G2), 6, Game_KeyEvent_);
};
