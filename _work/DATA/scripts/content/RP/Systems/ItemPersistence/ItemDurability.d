//////////////////////////////////////////////////////////////////////////
//	Haltbarkeit f�r Waffen und Ruestungen
//	==============
//	Nutzt die bisher ungenutzen Felder hp und hp_max zum Festlegen
//  der Abnutzung von Gegenstaenden. 
//
//  Da sowohl Waffen als auch Ruestungen nicht gestackt werden, kann
//  es keine Probleme mit verschiedenen Werten pro Item-Instanz geben
//////////////////////////////////////////////////////////////////////////

const string RP_TXT_ITEMCONDITION = "Zustand: ";

func void RP_ModDurability (var c_npc slf, var c_item itm, var int damageReceived)
{
	itm.hp -= damageReceived / 10;
	itm.description = RP_BuildItemNameString(itm.name, RP_TXT_ITEMCONDITION, itm.hp);
	PrintDebug(ConcatStrings("[RP] ItemDurability - reduced item hp to ", IntToString(itm.hp)));

	if (itm.hp <= 0)
	{
		itm.hp = 0;

		if (itm.mainflag == ITEM_KAT_ARMOR)
		{
			AI_UnequipArmor(slf);
			Snd_Play ("BREAKSOUND_ARMOR");
			//DEBUG	
			PrintScreen ("ARMOR BREAK", -1, 35, _STR_FONT_ONSCREEN, 2);
		}
		else if (itm.mainflag == ITEM_KAT_NF || itm.mainflag == ITEM_KAT_FF) // Waffen
		{
			AI_RemoveWeapon(slf);
			AI_UnequipWeapons (slf);
			
			if ((itm.flags & ITEM_BOW)
			|| (itm.flags & ITEM_CROSSBOW))
			{
				AI_EquipBestMeleeWeapon(slf);
			}
			else
			{
				AI_EquipBestRangedWeapon(slf);
			};
			
			PrintScreen ("WEAPON BREAK", -1, 35, _STR_FONT_ONSCREEN, 2);
			Snd_Play ("BREAKSOUND_WEAPON");
		};
	};
};

func void RP_ModCurrentWeaponDurability (var c_npc slf, var int damageReceived)
{
	// Aktive items hier holen, da wenn der Funktion uebergeben, diese nicht mehr gueltig sind
	// Liegt evtl. daran, dass die Instanzen aus der OnDamage nur einen Frame gueltig sind
	var c_item wpn;
	wpn = Npc_GetReadiedWeapon(slf);
	
	RP_ModDurability(slf, wpn, damageReceived);
};

func void RP_ModCurrentArmorDurability (var c_npc slf, var int damageReceived)
{
	// Aktive items hier holen, da wenn der Funktion uebergeben, diese nicht mehr gueltig sind
	// Liegt evtl. daran, dass die Instanzen aus der OnDamage nur einen Frame gueltig sind
	var c_item armor;
	armor = Npc_GetEquippedArmor(slf);
	
	RP_ModDurability(slf, armor, damageReceived);
};
