Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2024-01-07T15:10:23+01:00

====== Alchemist ======
Angelegt Sonntag 07 Januar 2024

**Alchemist**
Gilden:				[[Gilden:AL:Feuermagier|Feuermagier]], [[Gilden:NL:Wassermagier|Wassermagier]], [[Gilden:SL:Novize|Novize]]/[[Gilden:SL:Guru|Guru]]
Voraussetzungen:		[[Systeme:Skills:Alchemie|Alchemie]] >= 25
Tätigkeiten/Quests:		Tränke brauen, Zutaten suchen, Forschung

== Ablauf ==
=== [TODO] Sumpflager: ===
- Einstiegsmöglichkeiten:
	1. Fragen bei Cor Kalom oder Caine
- Arbeitszeiten:
	* Schichtarbeit nach Absprache?
	* Frühschicht 8 - 16 Uhr
	* Spätschicht 16 - 24 Uhr
- Einfinden im Alchemielabor
- versch. Tränke brauen
- Nachfrage zufällig bestimmt? Konkrete Anfragen versch. Leute? Später Crawler-Sekret?
- Abgabe bei Kalom oder deponieren in Kiste
- Belohnung von Kalom (Erz?)

=== [TODO] Altes Lager: ===
- Einstiegsmöglichkeiten:
	1. Melden bei Damarok?
- Arbeitszeiten:
	* Frühschicht 8 - 16 Uhr
	* Spätschicht 16 - 24 Uhr
- Einfinden im Alchemielabor
- versch. Tränke brauen
- Nachfrage zufällig bestimmt? Konkrete Anfragen versch. Leute? 
- Abgabe bei Torrez?
- Belohnung von?

=== [TODO] Neues Lager: ===
- Einstiegsmöglichkeiten:
	1. Nachfrage bei Riordian
- Arbeitszeiten:
	* Schichtarbeit nach Absprache?
	* Frühschicht 8 - 16 Uhr
	* Spätschicht 16 - 24 Uhr
- Einfinden im Alchemielabor
- versch. Tränke brauen
- Nachfrage zufällig bestimmt? Konkrete Anfragen versch. Leute?
- Abgabe bei Chronos?
- Belohnung von ???

== [TODO] Spielmechanik ==
Ziel: 
- Mindestsatz an bestimmten Tränken (Art zufällig)
- Konkrete Aufträge von verschiedenen Personen/Gruppen (z.B. x Heiltränke für Templer)
- Erforschung neuer Tränke? Verbesserte Rezepturen?

Möglichkeiten der Aktivität an sich:
- Einsammeln von Kraut-/Reis-Items in der Welt (mit Respawn)
- Generisches "Sammeln mit Animation" an vorgesehenen Stellen (ähnlich Erz hacken)
- Zeitvorlauf in gewissen abständen (ähnlich Sleepmenu)
- Einfach sammeln lassen mit Timer, Erz wird dem Inventar sporadisch hinzugefügt

Belohnungen:
- Erz (pro Trank oder Pauschale? Abhängig von Auftrag oder genrellem Tagesziel?)
- XP
- Skill-Bonus
