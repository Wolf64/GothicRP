//****************************
// 		Erzklumpen Sch�rfen
//****************************

func void RP_WORKORE_S1 ()
{
	var C_NPC her; 	her = Hlp_GetNpc(PC_Hero); 
	var C_NPC rock; rock = Hlp_GetNpc(PC_Rockefeller);
	
	//***ALT** if	(Hlp_GetInstanceID (self)== Hlp_GetInstanceID (Hero)) // MH: ge�ndert, damit kontrollierte NSCs nicht schlafen k�nnen!
	if ( (Hlp_GetInstanceID(self)==Hlp_GetInstanceID(her))||(Hlp_GetInstanceID(self)==Hlp_GetInstanceID(rock)) ) 
	{	
		self.aivar[AIV_INVINCIBLE]=TRUE;
        RP_Mob_Current = RP_MOB_ORE;
		Ai_ProcessInfos (her);
		
		//RP_ShowSleepMenu();
	};
};

INSTANCE PC_WorkOre_PassWork (C_INFO)
{
	npc				= PC_Hero;
	condition		= PC_WorkOre_PassWork_Condition;
	information		= PC_WorkOre_PassWork_Info;
	important		= 0;
	permanent		= 1;
	description		= "(Zeitvorlauf)"; 
};

func int PC_WorkOre_PassWork_Condition()
{
    return RP_Mob_Current == RP_MOB_ORE;
};

func void PC_WorkOre_PassWork_Info ()
{
    Wld_AddTime(0, 3, 0);
    AI_StandUp(hero);
    self.aivar[AIV_INVINCIBLE]=FALSE;
	AI_StopProcessInfos (self);
};