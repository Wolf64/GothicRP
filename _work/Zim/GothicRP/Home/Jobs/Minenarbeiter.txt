Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2024-01-07T15:09:55+01:00

====== Minenarbeiter ======
Angelegt Sonntag 07 Januar 2024

**Minenarbeiter**
Gilden:				[[Gilden:AL:Buddler|Buddler]], [[Gilden:NL:Schürfer|Schürfer]]
Voraussetzungen:		Spitzhacke
Tätigkeiten/Quests:		In den Minen schürfen

== [TODO] Ablauf ==
=== Altes Lager: ===
- Einstiegsmöglichkeiten:
	1. Freiwillig melden (bei Thorus?)
	2. Schutzgeld verweigern (Fletcher, Bloodwyn, Jackal), verzögerter Effekt (nach 1 Tag?)
	3. Kampf mit Lagerangehörigen (außer Arena)
- Arbeitszeiten:
	* Mo-Sa reguläre Arbeitszeiten (s.u.)
	* 2x2 Tage Arbeit und Pause, Übernachtung in Minen-Camp (Bedingungen hierfür?)
	* 6 Tage Durcharbeiten in der Mine als Bestrafungsmöglichkeit (e.g. um Erz beschissen, zu lange kein Schutzgeld...)
	* 8 - 18 Uhr, Arbeitsweg ca. 45 Min (Aufstehen um 7, Arbeitsbeginn 8, bei 19 Uhr zurück im Lager)
	* Schichtarbeit?
- Anmeldung und Einweisung bei Drake in der Alten Mine (nur beim ersten Mal?)
- Zuweisung Schürfstelle (Zufällig?)
- Suchen von freier Erzader (Möglich Buddler zu fragen ihren Posten zu wechseln)
- [[#[TODO]-spielmechanik|Hacken]] (Minigame? Wie Zeit überbrücken?)
- Beliebige Pausen (zwecks Essen/Trinken)
- Arbeitsschluss 18 Uhr
- Abgabe gesammeltes Erz bei Viper, Einstreichen das Anteils (Erz)
- Übernachtung in Lager vor der Mine oder Rückweg ins Alte Lager
- Abholung von Anteil (Nahrung) bei Fisk/Dexter?

=== Neues Lager: ===
- Einstiegsmöglichkeiten:
	1. Freiwillig melden bei Chronos/Okyl/Swiney
- Arbeitszeiten:
	* Mo-Sa reguläre Arbeitszeiten (s.u.)
	* Übernachtung im Kessel
	* 8 - 18 Uhr, kein Arbeitsweg durch Hütten im Kessel
	* Schichtarbeit?
- Anmeldung und Einweisung bei Swiney im Kessel (nur beim ersten Mal?)
- Zuweisung Schürfstelle (Zufällig?), beginnend nur im Kessel, später auch in der Mine
- Suchen von freier Erzader (Möglich Schürfer zu fragen ihren Posten zu wechseln)
- [[#[TODO]-spielmechanik|Hacken]] (Minigame? Wie Zeit überbrücken?)
- Beliebige Pausen (zwecks Essen/Trinken)
- Arbeitsschluss 18 Uhr
- Abgabe gesammeltes Erz bei Swiney
- Übernachtung im Kessel
- Abholung von Verpflegung (3x Reis) bei Swiney

== [TODO] Spielmechanik ==
Ziel: 
- Mindestsatz an Erz?
- Erreichte Menge bestimmt Belohnung oder Bestrafung
- Abhängig von Skill, Pausen oder anderen Mitteln (z.B. Tricksereien)

Möglichkeiten der Aktivität an sich:
- Überspringen der Arbeitszeit (in wie fern Hunger/Durst berechnen?)
- Zeitvorlauf in gewissen abständen (ähnlich Sleepmenu)
- Einfach hacken lassen mit Timer, Erz wird dem Inventar sporadisch hinzugefügt

Belohnungen:
- Erz (basierend auf geschürfter Menge)
- XP
- Skillpunkte [[Systeme:Skills:Schürfen|Schürfen]]
- Gunst im Lager?

