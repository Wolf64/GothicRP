/***********************************
*	Needs System
************************************/
var int RP_Needs_Initialized;

var int RP_BS;

var int RP_Needs_OverlayApplied; // Humans_Tired bei sleep debuff (L2/L3)

func void RP_Needs_HungerTick()
{
	if ((RP_Needs_Hunger - RP_Needs_HungerRate) < 0) 
	{ 
		RP_Needs_Hunger = 0;
		return; 
	};
	
	RP_Needs_Hunger = RP_Needs_Hunger - RP_Needs_HungerRate;

	if(RP_Needs_Hunger == 0)
	{
		AI_StandUp(hero);
		hero.attribute[ATR_HITPOINTS] = 0;
	};
	if (RP_Needs_Hunger < 1000)
	{
		//Use PC_Hero instance instead of the global "hero" var!
		//E.g. Control spell can put the controlled NPC in "hero".
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffHunger_L3);
	}
	else if (RP_Needs_Hunger < 2000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffHunger_L2);
	}
	else if (RP_Needs_Hunger < 4000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffHunger_L1);
	};

	/*
	var string prnt;
	prnt = IntToString(RP_Needs_Hunger); 
	prnt = ConcatStrings("Hunger: ", prnt);
	
	Print_DeleteText(RP_Needs_HungerTextHndl);
	RP_Needs_HungerTextHndl = Print_Ext(150, 1000, prnt, "font_old_20_white.tga", RGBA(255, 0, 0, 255), -1);
	*/
	
    Bar_SetValue(RP_Needs_HungerBarHndl, RP_Needs_Hunger);
};

func void RP_Needs_ThirstTick()
{
	if ((RP_Needs_Thirst - RP_Needs_ThirstRate) < 0) 
	{ 
		RP_Needs_Thirst = 0;
		return; 
	};
	
	RP_Needs_Thirst = RP_Needs_Thirst - RP_Needs_ThirstRate;
	
	if(RP_Needs_Thirst == 0)
	{
		AI_StandUp(hero);
		hero.attribute[ATR_HITPOINTS] = 0;
	}
	else if (RP_Needs_Thirst < 1000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffThirst_L3);
	}
	else if (RP_Needs_Thirst < 2000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffThirst_L2);
	}
	else if (RP_Needs_Thirst < 4000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffThirst_L1);
	};
	
	/*
	var string prnt;
	prnt = IntToString(RP_Needs_Thirst); 
	prnt = ConcatStrings("Thirst: ", prnt);
	
	Print_DeleteText(RP_Needs_ThirstTextHndl);
	RP_Needs_ThirstTextHndl = Print_Ext(150, 1500, prnt, "font_old_20_white.tga", RGBA(0, 255, 0, 255), -1);
	*/
	
    Bar_SetValue(RP_Needs_ThirstBarHndl, RP_Needs_Thirst);
};

func void RP_Needs_SleepTick()
{
	if(Hlp_GetInstanceID(hero) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	if ((RP_Needs_Sleep - RP_Needs_SleepRate) < 0) 
	{ 
		RP_Needs_Sleep = 0;
		return; 
	};

	//etwas Regeneration während einer Schlafattacke um es nicht zu nervig zu machen
	if (NPC_IsInState(hero, ZS_MagicSleep))
	{
		RP_Needs_Sleep += RP_Needs_SleepRate/2;
	}
	else
	{
		RP_Needs_Sleep -=  RP_Needs_SleepRate;
	};
	
	
	if (RP_Needs_Sleep < 1000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffSleep_L3);
		if (!RP_Needs_OverlayApplied)
		{
			Mdl_ApplyOverlayMds(hero, "Humans_Tired.mds"); 
			RP_Needs_OverlayApplied = true;
		};
		
		//zufällige Müdigkeitsanfälle
		if (r_Max(100) < 5 && !NPC_IsInState(hero, ZS_MagicSleep))
		{
			AI_StartState	(hero,ZS_MagicSleep,1,"");
		};
	}
	else if (RP_Needs_Sleep < 2000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffSleep_L2);
		if (!RP_Needs_OverlayApplied)
		{
			Mdl_ApplyOverlayMds(hero, "Humans_Tired.mds");
			RP_Needs_OverlayApplied = true;
		};
	}
	else if (RP_Needs_Sleep < 4000)
	{
		Buff_ApplyOrRefresh(PC_Hero, RP_Needs_DebuffSleep_L1);
	};

	if (RP_Needs_Sleep > 2000
		&& RP_Needs_OverlayApplied)
	{
		Mdl_RemoveOverlayMds(hero, "Humans_Tired.mds"); 
		RP_Needs_OverlayApplied = false;
	};
	
	/*
	var string prnt;
	prnt = IntToString(RP_Needs_Sleep); 
	prnt = ConcatStrings("Sleep: ", prnt);
	
	Print_DeleteText(RP_Needs_SleepTextHndl);
	RP_Needs_SleepTextHndl = Print_Ext(150, 2000, prnt, "font_old_20_white.tga", RGBA(0, 0, 255, 255), -1);
	*/
	
    Bar_SetValue(RP_Needs_SleepBarHndl, RP_Needs_Sleep);
};

func void RP_Needs_ConsumeFood(var c_npc slf, var int amt)
{
	//Darf nur für den PC passieren, sonst hat man nie wieder
	//Hungerprobleme sobald man von NPCs umringt ist
	if(Hlp_GetInstanceID(slf) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	if ((RP_Needs_Hunger + amt) > RP_Needs_HungerMax)
	{
		RP_Needs_Hunger = RP_Needs_HungerMax;
	}
	else
	{
		RP_Needs_Hunger += amt;
	};

	if(!Hlp_IsValidHandle(RP_Needs_HungerBarHndl)) 
	{
		RP_Needs_HungerBarHndl = Bar_Create(RP_Needs_HungerBar);
    };
	
    //Bar_SetMax(RP_Needs_HungerBarHndl, RP_Needs_HungerMax);
    Bar_SetValue(RP_Needs_HungerBarHndl, RP_Needs_Hunger);
};

func void RP_Needs_ConsumeDrink(var c_npc slf, var int amt)
{
	//Darf nur für den PC passieren, sonst hat man nie wieder
	//Durst sobald man von NPCs umringt ist
	if(Hlp_GetInstanceID(slf) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	if ((RP_Needs_Thirst + amt) > RP_Needs_ThirstMax)
	{
		RP_Needs_Thirst = RP_Needs_ThirstMax;
	}
	else
	{
		RP_Needs_Thirst += amt;
	};
	
	if(!Hlp_IsValidHandle(RP_Needs_ThirstBarHndl)) 
	{
		RP_Needs_ThirstBarHndl = Bar_Create(RP_Needs_ThirstBar);
    };
	
    //Bar_SetMax(RP_Needs_ThirstBarHndl, RP_Needs_ThirstMax);
    Bar_SetValue(RP_Needs_ThirstBarHndl, RP_Needs_Thirst);	
};

func void RP_Needs_ConsumeHoursSlept(var int t)
{
	//Wird eh nur vom Spieler abgerufen, also kein Check n�tig
	//if(Hlp_GetInstanceID(slf) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	//Wert um welcher RP_Needs_Sleep erh�ht werden soll
	var int amt; amt = t*60*RP_Needs_SleepRate*3;
	
	if ((RP_Needs_Sleep + amt) > RP_Needs_SleepMax)
	{
		RP_Needs_Sleep = RP_Needs_SleepMax;
	}
	else
	{
		RP_Needs_Sleep += amt;
	};
	
	if(!Hlp_IsValidHandle(RP_Needs_SleepBarHndl)) 
	{
		RP_Needs_SleepBarHndl = Bar_Create(RP_Needs_ThirstBar);
    };
	
    //Bar_SetMax(RP_Needs_SleepBarHndl, RP_Needs_SleepMax);
    Bar_SetValue(RP_Needs_SleepBarHndl, RP_Needs_Sleep);	
	
	//auch f�r Hunger
	amt = t*60*RP_Needs_HungerRate/2;
	
	if (!(RP_Needs_Hunger - amt) < 100)	//damit man beim schlafen nicht direkt stirbt
	{
		RP_Needs_Hunger -= amt;
	};
	
	if(!Hlp_IsValidHandle(RP_Needs_HungerBarHndl)) 
	{
		RP_Needs_HungerBarHndl = Bar_Create(RP_Needs_HungerBar);
    };
	
    //Bar_SetMax(RP_Needs_SleepBarHndl, RP_Needs_SleepMax);
    Bar_SetValue(RP_Needs_HungerBarHndl, RP_Needs_Hunger);	
	
	//und Durst
	amt = t*60*RP_Needs_ThirstRate/2;
	
	if (!(RP_Needs_Thirst - amt) < 100)	//damit man beim schlafen nicht direkt stirbt
	{
		RP_Needs_Thirst -= amt;
	};
	
	if(!Hlp_IsValidHandle(RP_Needs_ThirstBarHndl)) 
	{
		RP_Needs_ThirstBarHndl = Bar_Create(RP_Needs_ThirstBar);
    };
	
    //Bar_SetMax(RP_Needs_SleepBarHndl, RP_Needs_SleepMax);
    Bar_SetValue(RP_Needs_ThirstBarHndl, RP_Needs_Thirst);	
};

func void _RP_Needs_UpdateTickRates(var int state)
{
	if (state == RP_BS_WALK)
	{
		RP_Needs_HungerRate = RP_Needs_HungerRate_Walk;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Walk;	
		RP_Needs_SleepRate = RP_Needs_SleepRate_Walk;
		PrintDebug("[RP] Needs - BS Walk, tickrates set to 4|5|3");
	}
	else if (state == RP_BS_CARDIO)
	{
		RP_Needs_HungerRate = RP_Needs_HungerRate_Cardio;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Cardio;	
		RP_Needs_SleepRate = RP_Needs_SleepRate_Cardio;
		PrintDebug("[RP] Needs - BS Cardio, tickrates set to 5|8|5");
	}
	else if (state == RP_BS_WORK)
	{
		RP_Needs_HungerRate = RP_Needs_HungerRate_Work;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Work;	
		RP_Needs_SleepRate = RP_Needs_SleepRate_Work;
		PrintDebug("[RP] Needs - BS Work, tickrates set to 6|8|6");
	}
	else if (state == RP_BS_PAUSE)
	{
		RP_Needs_HungerRate = RP_Needs_HungerRate_Pause;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Pause;	
		RP_Needs_SleepRate = RP_Needs_SleepRate_Pause;
		PrintDebug("[RP] Needs - BS Pause, tickrates set to 4|5|1");
	}
	else if (state == RP_BS_IDLE)
	{
		RP_Needs_HungerRate = RP_Needs_HungerRate_Idle;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Idle;	
		RP_Needs_SleepRate = RP_Needs_SleepRate_Idle;
		PrintDebug("[RP] Needs - BS Idle, tickrates set to 4|5|2");
	};
};

//Funktion pr�ft den aktuellen BodyState und ruft
//die passende Funktion daf�r auf.
// K�mpfe werden hier nicht behandelt, es wird
// k�nstliche Ersch�pfung hinzugef�gt bei jedem Schlag/Treffer 
// -> siehe LeGo\OnDamage
func void RP_Needs_BSChecker()
{
	//TODO: bestimmte BSs in Kategorien zusammenfassen
	//	z.B. Schwimmen und Rennen
	if(Hlp_GetInstanceID(hero) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	if (C_BodyStateContains(hero, BS_WALK))
	{
		_RP_Needs_UpdateTickRates(RP_BS_WALK);
	}
	else if (C_BodyStateContains(hero, BS_CLIMB)
		|| C_BodyStateContains(hero, BS_JUMP)
		|| C_BodyStateContains(hero, BS_DIVE)
		|| C_BodyStateContains(hero, BS_SWIM)
		|| C_BodyStateContains(hero, BS_RUN))
	{
		_RP_Needs_UpdateTickRates(RP_BS_CARDIO);
	}
	else if (C_BodyStateContains(hero, BS_MOBINTERACT) // TODO Betten sind auch BS_MOBINTER! Hier muss unterschieden werden
		|| C_BodyStateContains(hero, BS_MOBINTERACT_INTERRUPT))
	{
		_RP_Needs_UpdateTickRates(RP_BS_WORK);
	}
	else if (C_BodyStateContains(hero, BS_SIT)
		|| C_BodyStateContains(hero, BS_LIE)) // Wann passiert das?
	{
		_RP_Needs_UpdateTickRates(RP_BS_PAUSE);
	}
	else
	{
		_RP_Needs_UpdateTickRates(RP_BS_IDLE);
	};
};

//BodyState Listener calls a check every time
//as soon as BS of hero changes (FF in RP_Init).
func void RP_Needs_BSListener()
{
	//Return if it's not our "real" hero
	if(Hlp_GetInstanceID(hero) != Hlp_GetInstanceID(PC_Hero)) { return; };
	
	if(!(Npc_GetBodyState(hero) == RP_BS)) // mache checks nur wenn sich der BS auch ge�ndert hat
	{
		RP_BS = Npc_GetBodyState(hero);

		// der eigentliche BS-Check
		RP_Needs_BSChecker();
	};
};


func void RP_InitNeeds()
{
	//Raten und variablen das erste Mal setzen
	if(!RP_Needs_FirstInit)
	{
		//Print_Ext(150, 4000, "Needs: First init...", "font_old_20_white.tga", RGBA(0, 0, 255, 255), 5*1000);
		
		//besser alle 4 sec als jede Sekunde, spart evlt. etwas Performance
		RP_Needs_HungerRate = RP_Needs_HungerRate_Idle;	RP_Needs_HungerTickMS = 4*1000;
		RP_Needs_ThirstRate = RP_Needs_ThirstRate_Idle;	RP_Needs_ThirstTickMS = 4*1000+10;	//immer ein paar ms sp�ter, sonst scheinen manche buffs nicht zu feuern
		RP_Needs_SleepRate = RP_Needs_SleepRate_Idle;	RP_Needs_SleepTickMS = 2*1000+20;
		RP_BSListenerTick = 1000;
		
		RP_Needs_Hunger = RP_Needs_HungerMax/2;
		RP_Needs_Thirst = RP_Needs_ThirstMax/2;
		RP_Needs_Sleep = RP_Needs_SleepMax/2;
		
		RP_Needs_FirstInit = true;
	};

	RP_Needs_HungerTick();
	RP_Needs_ThirstTick();
	RP_Needs_SleepTick();
	
	//***********Bars initialisieren******************
	if(!Hlp_IsValidHandle(RP_Needs_HungerBarHndl)) { RP_Needs_HungerBarHndl = Bar_Create(RP_Needs_HungerBar); };
    Bar_SetMax(RP_Needs_HungerBarHndl, RP_Needs_HungerMax);
    Bar_SetValue(RP_Needs_HungerBarHndl, RP_Needs_Hunger);
	
	if(!Hlp_IsValidHandle(RP_Needs_ThirstBarHndl)) { RP_Needs_ThirstBarHndl = Bar_Create(RP_Needs_ThirstBar); };
    Bar_SetMax(RP_Needs_ThirstBarHndl, RP_Needs_ThirstMax);
    Bar_SetValue(RP_Needs_ThirstBarHndl, RP_Needs_Thirst);
	
	if(!Hlp_IsValidHandle(RP_Needs_SleepBarHndl)) { RP_Needs_SleepBarHndl = Bar_Create(RP_Needs_SleepBar); };
    Bar_SetMax(RP_Needs_SleepBarHndl, RP_Needs_SleepMax);
    Bar_SetValue(RP_Needs_SleepBarHndl, RP_Needs_Sleep);
	//*********************************************
	
	//Print_Ext(150, 4500, "Needs: Initiated!", "font_old_20_white.tga", RGBA(0, 255, 0, 255), 5*1000);
	//FrameFunctions initialisieren (nur für Game Time, nicht während pause mode)
	//Dies soll IMMER passieren, damit auch Levelwechsel ber�cksichtig werden!
	FF_ApplyOnceExtGT(RP_Needs_HungerTick, RP_Needs_HungerTickMS, -1);
	FF_ApplyOnceExtGT(RP_Needs_ThirstTick, RP_Needs_ThirstTickMS, -1);
	FF_ApplyOnceExtGT(RP_Needs_SleepTick, RP_Needs_SleepTickMS, -1);
	FF_ApplyOnceExtGT(RP_Needs_BSListener, RP_BSListenerTick, -1);
	
	RP_Needs_Initialized = true;
};
