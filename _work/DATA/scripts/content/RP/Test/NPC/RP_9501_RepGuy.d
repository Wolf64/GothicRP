instance RP_9501_RepGuy (Npc_Default)
{
	//-------- primary data --------
	
	name		=	"Reputationstyp";
	npctype		=	npctype_main;
	guild 		=	GIL_NONE;    
	level 		=	4;
	voice 		=	4;
	id 			=	902; 

	//-------- abilities --------
	attribute[ATR_STRENGTH] =		20;
	attribute[ATR_DEXTERITY] =		10;
	attribute[ATR_MANA_MAX] =		0;
	attribute[ATR_MANA] =			0;
	attribute[ATR_HITPOINTS_MAX] =	88;
	attribute[ATR_HITPOINTS] =		88;

	//-------- visuals --------
	// 				animations
	Mdl_SetVisual		(self,"HUMANS.MDS");
	Mdl_ApplyOverlayMds	(self,"Humans_Tired.mds");
	//			body mesh     ,bdytex,skin,head mesh     ,headtex,teethtex,ruestung	
	Mdl_SetVisualBody (self,"hum_body_Naked0",2,1,"Hum_Head_Pony", 3,  2,-1);
	
	B_Scale (self);
	Mdl_SetModelFatness(self,0);
	fight_tactic	=	FAI_HUMAN_COWARD;
	
	//-------- Talente --------                                    
	
	Npc_SetTalentSkill	(self,NPC_TALENT_1H,1);		
	
	//-------- inventory --------                                    

		CreateInvItems (self, ItFoRice,6);
		CreateInvItem (self, ItMi_Stuff_Plate_01);
		CreateInvItem (self, ItFoBooze);
		EquipItem (self, ItMw_1H_Scythe_01); 		

	//-------------Daily Routine-------------
	daily_routine = Rtn_start_9501;
};

FUNC VOID Rtn_start_9501 ()
{
	TA_StandAround	    (20,15,07,15,"WP_TEST_REP");
	TA_StandAround		(07,15,20,15,"WP_TEST_REP");
};





