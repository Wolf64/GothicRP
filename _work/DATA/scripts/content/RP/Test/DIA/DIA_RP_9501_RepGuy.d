// ************************************************************
// 			  				   EXIT 
// ************************************************************

INSTANCE DIA_RepGuy_EXIT (C_INFO)
{
	npc			= RP_9501_RepGuy;
	nr			= 999;
	condition	= DIA_Condition_True;
	information	= DIA_Exit;
	permanent	= 1;
	description = DIALOG_ENDE;
};                       

INSTANCE Info_RepGuy_Increase(C_INFO)
{
	npc			= RP_9501_RepGuy;
	nr			= 1;
	condition	= DIA_Condition_True;
	information	= Info_RepGuy_Increase_Info;
	permanent	= 1;
	description = "Gut siehste aus.";
};                       

FUNC VOID Info_RepGuy_Increase_Info()
{
	AI_Output(other,self,"Info_RepGuy_Increase_15_00"); //Gut siehste aus.
	AI_Output(self,other,"Info_RepGuy_Increase_04_00"); //Jo danke dir.
    RP_AddReputation(self, 5);
};

INSTANCE Info_RepGuy_Decrease(C_INFO)
{
	npc			= RP_9501_RepGuy;
	nr			= 2;
	condition	= DIA_Condition_True;
	information	= Info_RepGuy_Decrease_Info;
	permanent	= 1;
	description = "Geh dich mal waschen.";
};                       

FUNC VOID Info_RepGuy_Decrease_Info()
{
	AI_Output(other,self,"Info_RepGuy_Decrease_15_00"); //Geh dich mal waschen.
	AI_Output(self,other,"Info_RepGuy_Decrease_04_00"); //Geh du dich mal waschen, du Greilijen.
    RP_AddReputation(self, -5);
};

INSTANCE Info_RepGuy_TESMIS(C_INFO)
{
	npc			= RP_9501_RepGuy;
	nr			= 3;
	condition	= DIA_Condition_True;
	information	= Info_RepGuy_TESMIS_Info;
	permanent	= 1;
	description = "Gib Mission.";
};                       

FUNC VOID Info_RepGuy_TESMIS_Info()
{
	AI_AskText(self, NOFUNC, NOFUNC, "Yep", "Nope");
};

func void NOFUNC() {};