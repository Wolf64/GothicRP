Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2024-01-07T16:30:28+01:00

====== Armbrust ======
Angelegt Sonntag 07 Januar 2024

* **Armbrust**, //bestimmt den Effektiven Skill mit Armbrüsten (z.B. kann ein Anfänger eine schwere Armbrust nur mit Stufe "Unerfahren" halten)//
	 0-24: 	unbedeutend, kann mit keinen Waffen gut umgehen													(alles "Unerfahren")
   25-49:		Anfänger, kann mit normalen Waffen gut umgehen, möglichkeit für Stufe "Erfahren"				(Waffen mit normaler (mittlerer) Länge entspricht Einhandskill, alles andere "Unerfahren")
   50-74:		erfahren, kann mit Langwaffen gut umgehen													(Langwaffen entspricht Einhandskill, alles andere "Unerfahren")
75-100:		Meister, kann mit Zweihandwaffen gut umgehen, möglichkeit für Stufe "Meister"					(Zweihandwaffen entspricht Zweihandskill)
