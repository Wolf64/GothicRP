/***********************************
*	Needs System Instances		*
************************************/

//Bar Instanzen, werden knapp über der HP Bar dargestellt
instance RP_Needs_HungerBar(GothicBar)
{
	x = RP_Needs_Bar_X;
	y = Print_Screen[PS_Y] - RP_Needs_Bar_Y_Offset_Hunger;
	barTex = "Bar_Hunger.tga";
};

instance RP_Needs_ThirstBar(GothicBar)
{
	x = RP_Needs_Bar_X;
	y = Print_Screen[PS_Y] - RP_Needs_Bar_Y_Offset_Thirst;
	barTex = "Bar_Thirst.tga";
};

instance RP_Needs_SleepBar(GothicBar)
{
	x = RP_Needs_Bar_X;
	y = Print_Screen[PS_Y] - RP_Needs_Bar_Y_Offset_Sleep;
	barTex = "Bar_Sleep.tga";
};

//Debuffs f�r Hunger/Durst/M�digkeit in 3 Stufen
/************************************
*			Hunger Stufe 1			*
************************************/
instance RP_Needs_DebuffHunger_L1(lCBuff)
{
    name = "Hunger Stufe 1";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_HungerTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffHunger_L1_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffHunger_Remove);
    buffTex = "HUNGER_L1.TGA";
};

func void RP_Needs_DebuffHunger_L1_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 10% der MAX HP und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = n.attribute[ATR_HITPOINTS_MAX] / 10;
	
	if (hpCurrent <= 0) { hpCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth = hpCurrent;
	
    Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];

	// als visual hint noch das modell anpassen
	PrintDebug("[RP] Needs - applying hunger L1 model fatness");
	Mdl_SetModelFatness(hero, 0.8);
};

//Muss nur einmal erstellt werden und gilt für alle Stufen
//da im Prinzip ja immer nur die globale Variable ausgelesen
//wird, die von den gleichen Debuff-Typen geshared wird.
func void RP_Needs_DebuffHunger_Remove(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
    Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, RP_Needs_Debuff_BaseHealth);
	//n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];
};

/************************************
*			Hunger Stufe 2			*
************************************/
instance RP_Needs_DebuffHunger_L2(lCBuff)
{
    name = "Hunger Stufe 2";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_HungerTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffHunger_L2_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffHunger_Remove);
    buffTex = "HUNGER_L2.TGA";
};

func void RP_Needs_DebuffHunger_L2_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 20% der MAX HP und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = (n.attribute[ATR_HITPOINTS_MAX] / 10) * 2;
	
	if (hpCurrent <= 0) { hpCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth = hpCurrent;
	
    Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];

	// als visual hint noch das modell anpassen
	PrintDebug("[RP] Needs - applying hunger L1 model fatness");
	Mdl_SetModelFatness(hero, 0.6);
};

/************************************
*			Hunger Stufe 3			*
************************************/
instance RP_Needs_DebuffHunger_L3(lCBuff)
{
    name = "Hunger Stufe 3";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_HungerTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffHunger_L3_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffHunger_Remove);
    buffTex = "HUNGER_L3.TGA";
};

func void RP_Needs_DebuffHunger_L3_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 20% der MAX HP und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = (n.attribute[ATR_HITPOINTS_MAX] / 10) * 5;
	
	if (hpCurrent <= 0) { hpCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth = hpCurrent;
	
    Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];

	// als visual hint noch das modell anpassen
	PrintDebug("[RP] Needs - applying hunger L1 model fatness");
	Mdl_SetModelFatness(hero, 0.4);
};

/************************************
*			Durst Stufe 1			*
************************************/
instance RP_Needs_DebuffThirst_L1(lCBuff)
{
	name = "Durst Stufe 1";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_ThirstTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffThirst_L1_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffThirst_Remove);
    buffTex = "THIRST_L1.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffThirst_L1_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 10% des Mana und ziehe sie von den Punkten ab
	var int manaCurrent;
	manaCurrent = n.attribute[ATR_MANA_MAX] / 10;
	
	if(manaCurrent == 0 ) { manaCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseMana = manaCurrent;
	
    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
};

func void RP_Needs_DebuffThirst_Remove(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
    Npc_ChangeAttribute(n, ATR_MANA_MAX, RP_Needs_Debuff_BaseMana);
	//n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
};

/************************************
*			Durst Stufe 2			*
************************************/
instance RP_Needs_DebuffThirst_L2(lCBuff)
{
	name = "Durst Stufe 2";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_ThirstTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffThirst_L2_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffThirst_Remove);
    buffTex = "THIRST_L2.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffThirst_L2_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 20% des Mana und ziehe sie von den Punkten ab
	var int manaCurrent;
	manaCurrent = (n.attribute[ATR_MANA_MAX] / 10) * 2;
	
	if(manaCurrent == 0 ) { manaCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseMana = manaCurrent;
	
    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
};

/************************************
*			Durst Stufe 3			*
************************************/
instance RP_Needs_DebuffThirst_L3(lCBuff)
{
	name = "Durst Stufe 3";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_ThirstTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffThirst_L3_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffThirst_Remove);
    buffTex = "THIRST_L3.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffThirst_L3_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
    //Berechne 50% des Mana und ziehe sie von den Punkten ab
	var int manaCurrent;
	manaCurrent = (n.attribute[ATR_MANA_MAX] / 10) * 5;
	
	if(manaCurrent == 0 ) { manaCurrent = 1; };

	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseMana = manaCurrent;
	
    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
};

/************************************
*			M�digkeit Stufe 1		*
************************************/
instance RP_Needs_DebuffSleep_L1(lCBuff)
{
	name = "M�digkeit Stufe 1";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_SleepTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffSleep_L1_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffSleep_Remove);
    buffTex = "SLEEP_L1.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffSleep_L1_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
	//Berechne 10% der MAX HP und Mana und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = n.attribute[ATR_HITPOINTS_MAX] / 10;
	if (hpCurrent <= 0) { hpCurrent = 1; };
	
	var int manaCurrent;
	manaCurrent = n.attribute[ATR_MANA_MAX] / 10;
	if(manaCurrent == 0 ) { manaCurrent = 1; };
	
	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth2 = hpCurrent;
	RP_Needs_Debuff_BaseMana2 = manaCurrent;

    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];
};

func void RP_Needs_DebuffSleep_Remove(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
    Npc_ChangeAttribute(n, ATR_MANA_MAX, RP_Needs_Debuff_BaseMana2);
	Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, RP_Needs_Debuff_BaseHealth2);
	//n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];			//wenn der SC durch Hunger/Durst stirbt
	//n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];	//wird er hierdurch praktisch wiederbelebt!

	// visual hint durch geh-Ani
	PrintDebug("[RP] Needs - remove sleep overlayMDS");
	Mdl_RemoveOverlayMDS(n, "Humans_Tired.mds");
};

/************************************
*			Muedigkeit Stufe 2		*
************************************/
instance RP_Needs_DebuffSleep_L2(lCBuff)
{
	name = "M�digkeit Stufe 2";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_SleepTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffSleep_L2_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffSleep_Remove);
    buffTex = "SLEEP_L2.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffSleep_L2_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
	//Berechne 20% der MAX HP und Mana und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = (n.attribute[ATR_HITPOINTS_MAX] / 10) * 2;
	if (hpCurrent <= 0) { hpCurrent = 1; };
	
	var int manaCurrent;
	manaCurrent = (n.attribute[ATR_MANA_MAX] / 10) * 2;
	if(manaCurrent == 0 ) { manaCurrent = 1; };
	
	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth2 = hpCurrent;
	RP_Needs_Debuff_BaseMana2 = manaCurrent;

    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];

	// als visual hint die Geh-Ani anpassen
	PrintDebug("[RP] Needs - apply L2 tired overlayMDS");
	Mdl_ApplyOverlayMds(hero, "Humans_Tired.mds");
};

/************************************
*			M�digkeit Stufe 3		*
************************************/
instance RP_Needs_DebuffSleep_L3(lCBuff)
{
	name = "M�digkeit Stufe 2";
    bufftype = BUFF_BAD;
        
    durationMS = RP_Needs_SleepTickMS+500;
	tickMS = 500;
        
    OnApply = SAVE_GetFuncID(RP_Needs_DebuffSleep_L3_Apply);
	OnRemoved = SAVE_GetFuncID(RP_Needs_DebuffSleep_Remove);
    buffTex = "SLEEP_L3.TGA";	//TODO: Textur erstellen
};

func void RP_Needs_DebuffSleep_L3_Apply(var int bh) 
{
    var int ptr; ptr = Buff_GetNpc(bh);
    if (!ptr) { return; }; // Kann passieren, falls z.B. die Welt gewechselt wurde

    var c_npc n; n = _^(ptr);
	
	//Berechne 20% der MAX HP und Mana und ziehe sie von den Punkten ab
	var int hpCurrent;
	hpCurrent = (n.attribute[ATR_HITPOINTS_MAX] / 10) * 5;
	if (hpCurrent <= 0) { hpCurrent = 1; };
	
	var int manaCurrent;
	manaCurrent = (n.attribute[ATR_MANA_MAX] / 10) * 5;
	if(manaCurrent == 0 ) { manaCurrent = 1; };
	
	//Betrag der abgezogen wird zwischenspeichern für Remove!
	RP_Needs_Debuff_BaseHealth2 = hpCurrent;
	RP_Needs_Debuff_BaseMana2 = manaCurrent;

    Npc_ChangeAttribute(n, ATR_MANA_MAX, manaCurrent*-1);
	Npc_ChangeAttribute(n, ATR_HITPOINTS_MAX, hpCurrent*-1);
	n.attribute[ATR_MANA] = n.attribute[ATR_MANA_MAX];
	n.attribute[ATR_HITPOINTS] = n.attribute[ATR_HITPOINTS_MAX];

	// als visual hint die Geh-Ani anpassen
	// Mdl_ApplyOverlayMds(hero, "Humans_Tired.mds"); // funzt nicht innerhalb von Buffs?
};
